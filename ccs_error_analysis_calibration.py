import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
from scipy import stats


def pred_error_percent(pred_values, true_values):
    y = np.multiply(np.divide(np.subtract(pred_values, true_values), true_values), 100)
    return y

def get_diff_cal_out_csv_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('combined_WH_WV_.csv')]
    # files = [x for x in files if not x.endswith('WH_WV_.csv')]
    # files = [x for x in files if x.startswith(fileid)]
    return files


def plot_bar_chart(ydata, yerr_data, y_avg, y_std, xlabel, dirpath, file_id):
    num = len(ydata)
    ind = np.arange(num)
    width = 0.7
    fig, ax = plt.subplots()
    rects = ax.bar(ind, ydata, width, color='blue', yerr=yerr_data, error_kw=dict(lw=1, capsize=1, capthick=1))
    plt.fill_between(ind, [y_avg+y_std]*len(ind), [y_avg-y_std]*len(ind), color='red', alpha=0.5)
    plt.axhline(y_avg, ls='--', color='red')
    ax.set_ylabel('% CCS Error')
    # plt.ylim((-15, 24))
    plt.xticks(ind, tuple(xlabel), rotation='vertical')
    plt.subplots_adjust(bottom=0.22)
    plt.tick_params(labelsize=5.5)

    plt.savefig(os.path.join(dirpath, 'ccs_error_' + file_id + '_.png'), dpi=500)
    plt.close()


if __name__=='__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\All_ions\All_ions_included"

    outlines = ''
    header = '#caltyple,avg_CCS_err,std_CCS_err,min_CCS_err,max_CCS_err,min_CCSerr_wh,min_CCSerr_wv,max_CCSerr_wh,max_CCSerr_wv,min_CCSerr_id,max_CCSerr_id\n'
    outlines += header


    files = get_diff_cal_out_csv_files(dirpath)

    file_id_list = []
    ccs_avg_arr_list = []

    for file in files:

        file_identity = '_'.join(str(file).split('_')[:4])

        file_id_list.append(file_identity)


        df = pd.read_csv(os.path.join(dirpath, file))
        df = df[(df['wh'] != 40) | (df['wv'] != 300)]
        # df = df[(df['wh'] != 35) | (df['wv'] != 300)]
        # df = df[(df['wh'] != 30) | (df['wv'] != 300)]
        # df = df[(df['wh'] != 40) | (df['wv'] != 400)]
        # df = df[(df['wh'] != 40) | (df['wv'] != 1000)]
        df = df[(df['wv'] == 300) | (df['wv'] == 400)]
        id_ = df['id'].values
        oligo = df['oligomer'].values
        mass = df['mass'].values
        charge = df['charge'].values
        ccs_error = df['ccs_error_percent'].values

        key_id_list = []
        for ind, (identity, oligo_, charge_) in enumerate(zip(id_, oligo, charge)):
            id_tag = '_'.join([str(identity), str(oligo_), str(charge_)])
            key_id_list.append(id_tag)
        unique_tag = np.unique(key_id_list)

        ccs_err_ = []
        mass_ = []
        charge_ = []


        for index, un_tag in enumerate(unique_tag):
            ccs_err = []
            mass_arr = []
            charge_arr = []
            for num, key_id in enumerate(key_id_list):
                if key_id == un_tag:
                    ccs_err.append(ccs_error[num])
                    mass_arr.append(mass[num])
                    charge_arr.append(charge[num])
            mass_.append(mass_arr)
            charge_.append(charge_arr)
            ccs_err_.append(ccs_err)

        mass_ = np.average(mass_, axis=1)
        charge_ = np.average(charge_, axis=1)
        sort_ind = np.lexsort((charge_, mass_))

        mass_sorted = np.array(mass_)[sort_ind]

        ccs_err_min_arr = np.min(ccs_err_, axis=1)[sort_ind]
        ccs_err_max_arr = np.max(ccs_err_, axis=1)[sort_ind]
        ccs_err_avg_arr = np.average(ccs_err_, axis=1)[sort_ind]
        ccs_err_std_arr = np.std(ccs_err_, axis=1)[sort_ind]
        unique_tag_ = np.array(unique_tag)[sort_ind]

        ccs_avg_arr_list.append(ccs_err_avg_arr)

        ccs_err_flat = np.array(ccs_err_).flatten()
        ccs_err_min = np.min(ccs_err_flat)
        ccs_err_max = np.max(ccs_err_flat)
        ccs_err_avg = np.average(ccs_err_flat)
        ccs_err_std = np.std(ccs_err_flat)

        min_ccs_err_df = df[(df['ccs_error_percent'] == ccs_err_min )]
        max_ccs_err_df = df[(df['ccs_error_percent'] == ccs_err_max)]

        min_wh = min_ccs_err_df['wh'].values[0]
        min_wv = min_ccs_err_df['wv'].values[0]
        max_wh = max_ccs_err_df['wh'].values[0]
        max_wv = max_ccs_err_df['wv'].values[0]

        min_id = min_ccs_err_df['id'].values[0]
        min_oligo = min_ccs_err_df['oligomer'].values[0]
        min_charge = min_ccs_err_df['charge'].values[0]

        max_id = max_ccs_err_df['id'].values[0]
        max_oligo = max_ccs_err_df['oligomer'].values[0]
        max_charge = max_ccs_err_df['charge'].values[0]

        min_err_key_id = '_'.join([str(min_id), str(min_oligo), str(min_charge)])
        max_err_key_id = '_'.join([str(max_id), str(max_oligo), str(max_charge)])


        plot_bar_chart(ccs_err_avg_arr, ccs_err_std_arr, ccs_err_avg, ccs_err_std, unique_tag_, dirpath, file_identity+'_300WV_400WV_')

        # header = '#caltyple,avg_CCS_err,std_CCS_err,min_CCS_err,max_CCS_err,min_CCSerr_wh,min_CCSerr_wv,max_CCSerr_wh,max_CCSerr_wv,min_CCSerr_id,max_CCSerr_id\n'

        line = '{},{},{},{},{},{},{},{},{},{},{}\n'.format(file_identity, ccs_err_avg, ccs_err_std, ccs_err_min, ccs_err_max, min_wh, min_wv, max_wh, max_wv, min_err_key_id, max_err_key_id)

        outlines += line


    with open(os.path.join(dirpath, '300WV_400WV_ccs_error_stats.csv'), 'w') as ccserrstat:
        ccserrstat.write(outlines)
        ccserrstat.close()

    outlines2 = ''
    header2 = '#caltype1,caltype2,t-stat,p-value\n'
    outlines2 += header2

    for ind, (file_id_combo, ccs_avg_arr_combo) in enumerate(zip(itertools.combinations(file_id_list, 2), itertools.combinations(ccs_avg_arr_list, 2))):
        ttest = stats.ttest_ind(ccs_avg_arr_combo[0], ccs_avg_arr_combo[1], equal_var=False)
        line_2 = '{},{},{},{}\n'.format(file_id_combo[0], file_id_combo[1], ttest[0], ttest[1])
        outlines2 += line_2

    with open(os.path.join(dirpath, '300WV_400WV_ccs_error_stats_ttest.csv'), 'w') as ttestfile:
        ttestfile.write(outlines2)
        ttestfile.close()




    print('heho')