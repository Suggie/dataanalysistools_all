# curate gauss fit files to filter some wh wv conditions

import os
import pandas as pd

def get_files(dirpath, endid):
    """
    get files
    :param dirpath:
    :param endid:
    :return: list
    """
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files


def curate_wh_wv_entries(dirpath, outdirpath, file, wh_wv_list):
    """
    don't include the wh wv condition from the list
    create
    :param dirpath:
    :param outdirpath: directory for saving the curated file
    :param file:
    :param wh_wv_list: [[wh, wv], ...]
    :return: none
    """
    outfname = str(file).split('.csv')[0]
    outfname = (outfname.split('#')[1]).split('.txt')[0] + '_gaussfit.csv'

    df = pd.read_csv(os.path.join(dirpath, file))
    for ind, wh_wv_ in enumerate(wh_wv_list):
        df = df.loc[(df.iloc[:, 0] != wh_wv_[0]) | (df.iloc[:, 1] != wh_wv_[1])]

    df.to_csv(os.path.join(outdirpath, outfname), index=False)

def curate_with_files_in_dir(dirpath, endid, outdirpath, wh_wv_list):
    """
    curate all files
    :param dirpath:
    :param outdirpath:
    :param wh_wv_list:
    :return: none
    """
    file_list = get_files(dirpath, endid=endid)
    for ind, file in enumerate(file_list):
        curate_wh_wv_entries(dirpath, outdirpath, file, wh_wv_list)


if __name__ == '__main__':

    dirpath = r"T:\Sugyan\G1 data for CCS calibration\PolyA CSV\Low IMS\09_11_POLYA_1MIN_LOWERIMS_07"
    outdirpath = r"T:\Sugyan\G1 data for CCS calibration\PolyA CSV\Low IMS\09_11_POLYA_1MIN_LOWERIMS_07\curate"
    wh_wv_list = [[30.0, 400.0], [20.0, 600.0]]
    curate_with_files_in_dir(dirpath, 'gaussfit.csv', outdirpath, wh_wv_list)