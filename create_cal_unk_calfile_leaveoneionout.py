# read the all ion cal file and generate cal and unk file by leaving one ion out

import os
import numpy as np
import pandas as pd


def create_one_ion_out_cal_unk_file(cal_file, dirpath):
    """
    create cal and uncal file by giving all ion cal file at one experiment condition
    :param cal_file:cal file all ion
    :param dirpath:directory to save cal and uncal file
    :return:save cal and uncal file plus a summary file
    """
    with open(cal_file, 'r') as cfile:


        cal_header = ''
        uncal_header = ''

        cfile_ = cfile.read().splitlines()
        header_ind = 9
        header_lines = cfile_[:header_ind]
        for line in header_lines:
            cal_header += line+'\n'
            if line.startswith('#Waveht'):
                waveht = line.split(',')[1]
            if line.startswith('#Wavevel'):
                wavevel = line.split(',')[1]
        header_line_unk = cfile_[8] + '\n'
        uncal_header += header_line_unk
        init_lines = cfile_[header_ind:]

        for num in range(len(init_lines)):
            input_cal_string = ''
            unk_cal_string = ''
            unk_line = init_lines[num]
            unk_cal_string += unk_line + '\n'
            unk_line_chars = unk_line.split(',')
            unk_id = '_'.join([unk_line_chars[0], unk_line_chars[1], unk_line_chars[3]])
            if num == 0:
                rest_lines = init_lines[num+1:]
            else:
                rest_lines = init_lines[:num] + init_lines[num+1:]

            for line in rest_lines:
                input_cal_string += line + '\n'

            gen_id = unk_id + '_wv_' + wavevel + '_wh_' + waveht +'.csv'
            cal_fname = 'input_'+ gen_id
            unk_fname = 'unk_' + gen_id


            with open(os.path.join(dirpath, cal_fname), 'w') as cal_in_file:
                outstring = cal_header + input_cal_string
                cal_in_file.write(outstring)
                cal_in_file.close()

            with open(os.path.join(dirpath, unk_fname), 'w') as unk_in_file:
                outstring = uncal_header + unk_cal_string
                unk_in_file.write(outstring)
                unk_in_file.close()


def create_one_species_out_cal_unk_file(cal_file, dirpath):
    with open(cal_file, 'r') as cfile:

        cal_header = ''
        uncal_header = ''

        cfile_ = cfile.read().splitlines()
        header_ind = 9
        header_lines = cfile_[:header_ind]
        init_lines = cfile_[header_ind:]
        for line in header_lines:
            cal_header += line+'\n'
            if line.startswith('#Waveht'):
                waveht = line.split(',')[1]
            if line.startswith('#Wavevel'):
                wavevel = line.split(',')[1]
        header_line_unk = cfile_[8] + '\n'
        uncal_header += header_line_unk

        init_lines = cfile_[header_ind:]

        # species_key = []
        # species_id = []
        # species_oligomer = []
        # species_mass = []
        # species_charge = []
        # species_dt = []

        df = pd.read_csv(cal_file, header=8)

        species_key = []
        for index, (id_, oligo_) in enumerate(zip(df['#id'], df['oligomer'])):
            key_ = '_'.join([str(id_), str(oligo_)])
            species_key.append(key_)

        # for init_line in init_lines:
        #     chars = init_line.split(',')
        #     key = '_'.join([chars[0], chars[1]])
        #     species_key.append(key)
        #     species_id.append(chars[0])
        #     species_oligomer.append(int(chars[1]))
        #     species_mass.append(float(chars[2]))
        #     species_charge.append(int(chars[3]))
        #     species_dt.append(float(chars[4]))

        # df_cols = ['id', 'oligomer', 'mass', 'charge', 'dt']
        # df_data = [np.array(species_id), np.array(species_oligomer), np.array(species_mass), np.array(species_charge),
        #            np.array(species_dt)]

        # df = pd.DataFrame({"id": np.array(species_id), "oligomer": np.array(species_oligomer),
        #                    "mass": np.array(species_mass), "charge": np.array(species_charge),
        #                    "dt": np.array(species_dt)})



        for ind, key in enumerate(species_key):
            spec = key[:-2]
            olig = int(key[-1])
            if spec.startswith('PolyAla'):
                spec = 'PolyAla'
                for charge in range(1, 4):
                    df_unk = df[(df['#id'].str.startswith(spec))]
                    df_unk = df_unk[(df_unk['charge'] == charge)]
                    df_cal = df[(df['#id'].str.startswith(spec) == False)]# & ((df['id'].str.startswith(spec)) & (df['charge'] != charge))]
                    df_cal_pol = df[(df['#id'].str.startswith(spec)) & (df['charge'] != charge)]
                    df_cal = df_cal.append(df_cal_pol)

                    file_id = '_'.join([spec, str(charge)])


                    uncal_string = ''
                    cal_string = ''

                    for ind, (id, oligomer, mass, charge, superclass, subclass, dt) in enumerate(zip(df_cal['#id'],
                                                                               df_cal['oligomer'],
                                                                               df_cal['mass'],
                                                                               df_cal['charge'],
                        df_cal['superclass'],
                        df_cal['subclass'],
                                                                               df_cal['dt'])):
                        line='{},{},{},{},{},{},{}\n'.format(str(id), str(oligomer), str(mass), str(charge),
                                                             str(superclass), str(subclass), str(dt))
                        cal_string += line

                    for ind, (id, oligomer, mass, charge, superclass, subclass, dt) in enumerate(zip(df_unk['#id'],
                                                                               df_unk['oligomer'],
                                                                               df_unk['mass'],
                                                                               df_unk['charge'],
                        df_unk['superclass'],
                        df_unk['subclass'],
                                                                               df_unk['dt'])):
                        line='{},{},{},{},{},{},{}\n'.format(str(id), str(oligomer), str(mass), str(charge),
                                                             str(superclass), str(subclass), str(dt))
                        uncal_string += line

                    gen_id = file_id + '_wv_' + wavevel + '_wh_' + waveht + '.csv'
                    cal_fname = 'input_' + gen_id
                    unk_fname = 'unk_' + gen_id

                    with open(os.path.join(dirpath, cal_fname), 'w') as cal_in_file:
                        outstring = cal_header + cal_string
                        cal_in_file.write(outstring)
                        cal_in_file.close()

                    with open(os.path.join(dirpath, unk_fname), 'w') as unk_in_file:
                        outstring = uncal_header + uncal_string
                        unk_in_file.write(outstring)
                        unk_in_file.close()




            if not spec.startswith('PolyAla'):
                spec = spec
                df_cal = df[(df['#id'] != spec)]
                df_unk = df[(df['#id'] == spec)]

                file_id = '_'.join([spec, str(olig)])

                uncal_string = ''
                cal_string = ''

                for ind, (id, oligomer, mass, charge, superclass, subclass, dt) in enumerate(zip(df_cal['#id'],
                                                                           df_cal['oligomer'],
                                                                           df_cal['mass'],
                                                                           df_cal['charge'],
                    df_cal['superclass'],
                    df_cal['subclass'],
                                                                           df_cal['dt'])):
                    line = '{},{},{},{},{},{},{}\n'.format(str(id), str(oligomer), str(mass), str(charge),
                                                           str(superclass), str(subclass), str(dt))
                    cal_string += line

                for ind, (id, oligomer, mass, charge, superclass, subclass, dt) in enumerate(zip(df_unk['#id'],
                                                                           df_unk['oligomer'],
                                                                           df_unk['mass'],
                                                                           df_unk['charge'],
                    df_unk['superclass'],
                    df_unk['subclass'],
                                                                           df_unk['dt'])):
                    line = '{},{},{},{},{},{},{}\n'.format(str(id), str(oligomer), str(mass), str(charge),
                                                           str(superclass), str(subclass), str(dt))
                    uncal_string += line

                gen_id = file_id + '_wv_' + wavevel + '_wh_' + waveht + '.csv'
                cal_fname = 'input_' + gen_id
                unk_fname = 'unk_' + gen_id

                with open(os.path.join(dirpath, cal_fname), 'w') as cal_in_file:
                    outstring = cal_header + cal_string
                    cal_in_file.write(outstring)
                    cal_in_file.close()

                with open(os.path.join(dirpath, unk_fname), 'w') as unk_in_file:
                    outstring = uncal_header + uncal_string
                    unk_in_file.write(outstring)
                    unk_in_file.close()

            print('haho')

        print('heho')






if __name__ == '__main__':
    cal_input_file_list = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\powerlaw\smallmol\all\cal_input_files.csv"
    # outputdir_n = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_nofixa\Peptides\LeaveOneIon"
    outputdir_t = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\powerlaw\smallmol\leaveonespec"
    cal_input_file_df = pd.read_csv(cal_input_file_list)
    for ind, cal_file in enumerate(cal_input_file_df['#cal_input_files'].values):
        # create_one_ion_out_cal_unk_file(cal_file, outputdir_n)
        create_one_species_out_cal_unk_file(cal_file, outputdir_t)