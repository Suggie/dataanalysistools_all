import os
import tkinter
import tkinter.filedialog as filedialog

def change_fname_sarah(fname):
    name_chars = fname.split('_')
    lig_id = (name_chars[-2].split('.')[0]).split("#")[-1]
    assemble_list = name_chars[1:-2]
    assemble_name = lig_id+'_'+('_').join(x for x in assemble_list)+'_raw.csv'
    return assemble_name

def cut_fname_twimextract(fname):
    name_chars = fname.split('_')
    assemble_list = name_chars[2:]
    assemble_name = ('_').join(x for x in assemble_list)
    return assemble_name

if __name__ == '__main__':
    root = tkinter.Tk()
    root.withdraw()
    filelist = filedialog.askopenfilenames(parent=root, title='Choose files')
    print(filelist)
    dirpath = os.path.dirname(filelist[-1])
    for file in filelist:
        fname = os.path.split(file)[-1]
        # fname_new = change_fname(fname)
        fname_new = cut_fname_twimextract(fname)
        print(fname + '------------>' + fname_new)
        os.rename(file, os.path.join(dirpath, fname_new))
    print('Selected files changed in '+dirpath)