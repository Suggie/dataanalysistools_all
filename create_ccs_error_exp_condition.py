import os
import numpy as np
import pandas as pd

def get_files(dirpath, startid, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith(startid)]
    files = [x for x in files if x.endswith(endid)]
    return files

def gen_list_dfs(file_list, dirpath):
    data_store = []
    for file in file_list:
        df = pd.read_csv(os.path.join(dirpath, file), sep=',')
        data_store.append(df)
    return data_store

def write_output(list, wh, wv, startid, dirpath):
    outline = ''
    # header1 = '# ' + str(wh) + 'WH ' + str(wv) + 'WV\n'
    header2 = 'id,oligo,mass,charge,drift_time,exp_avg_vel,ccs,pred_corr_ccs,pred_ccs,pred_ccs_error_percent\n'
    # outline += header1
    outline += header2
    for item in list:
        string = ','.join(str(x) for x in item)
        line = '{}\n'.format(string)
        outline += line

    caloutname = 'unk_output_exp_' + startid + '_exp_#_' + str(wh) + '_WH_' + str(wv) + '_WV_.csv'
    with open(os.path.join(dirpath, caloutname), 'w') as outfile:
        outfile.write(outline)
        outfile.close()


def sort_wh_wv_all(df_list, startid, dirpath):
    unique_wh = np.unique(df_list[0]['wh'])
    unique_wv = np.unique(df_list[0]['wv'])

    for item1, un_wh in enumerate(unique_wh):
        for item2, un_wv in enumerate(unique_wv):
            data_wh_wv = []
            for ind, df in enumerate(df_list):
                for index, (wh, wv) in enumerate(zip(df['wh'], df['wv'])):

                    if un_wh == wh:
                        if un_wv == wv:
                            molid = df['id'][index]
                            mololigo = df['oligo'][index]
                            mass = df['mass'][index]
                            charge = df['charge'][index]
                            dt = df['drift_time'][index]
                            exp_avg_vel = df['exp_avg_vel'][index]
                            ccs = df['ccs'][index]
                            pred_corr_ccs = df['pred_corr_ccs'][index]
                            pred_ccs = df['pred_ccs'][index]
                            pred_ccs_error_percent = df['pred_ccs_error_percent'][index]
                            data_wh_wv.append([molid, mololigo, mass, charge, dt, exp_avg_vel, ccs, pred_corr_ccs, pred_ccs, pred_ccs_error_percent])


            write_output(data_wh_wv, un_wh, un_wv, startid, dirpath)
            print('heho')



    print('heho')




dirpath = r'C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\PolyAlanine\Cross_Val_each_ion'

start_id_list = ['ccs_error_powerfit_powerfitexp_False',
                 'ccs_error_powerfit_powerfitexp_True',
                 'ccs_error_relaxation_True_terms_4',
                 'ccs_error_relaxation_True_terms_6',
                 'ccs_error_relaxation_True_terms_10',
                 'ccs_error_relaxation_False_terms_4',
                 'ccs_error_relaxation_False_terms_6',
                 'ccs_error_relaxation_False_terms_10',
                 'ccs_error_relaxationexp_True_terms_4',
                 'ccs_error_relaxationexp_True_terms_6',
                 'ccs_error_relaxationexp_True_terms_10']

endid = '.csv'

for startid in start_id_list:

# startid = 'ccs_error_relaxationexp_True_terms_6'

    file_list = get_files(dirpath, startid, endid)

    df_list = gen_list_dfs(file_list, dirpath)
    sort_wh_wv_all(df_list, startid, dirpath)
    print('heho')