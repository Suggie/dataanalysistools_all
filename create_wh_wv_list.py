#determine the wave height and wave velocity for specific ratio or conditions
import numpy as np
import itertools
import os

def make_file_without_ratio_bounds(dirpath, fname, wh_list, wv_list):
    """
    create wh wv list file
    :param dirpath: directory path
    :param fname: file name
    :param wh_list: wh list
    :param wv_list: wv list
    :return:
    """
    num = 1
    with open(os.path.join(dirpath, fname), 'w') as outfile:
        header = 'num, wh, wv, \n'
        outfile.write(header)
        for product in itertools.product(wh_list, wv_list):
            # print(num)
            line = '{}, {}, {}, \n'.format(num, product[0], product[1])
            outfile.write(line)
            num += 1
        outfile.close()

def make_file_with_ratio_bounds(dirpath, fname, wh_list, wv_list, ratio_bounds=(0.08, 0.1)):
    """
    create wh wv list file with filter of conditions with bound of wh/wv ratio
    :param dirpath: directory
    :param fname: file name
    :param wh_list: wh list
    :param wv_list: wv list
    :param ratio_bounds: wh/wv ratio bounds = [low_bound, high_bound]
    :return:
    """
    num = 1
    with open(os.path.join(dirpath, fname), 'w') as outfile:
        header = 'num, wh, wv, \n'
        outfile.write(header)
        for product in itertools.product(wh_list, wv_list):
            ratio = product[0]/product[1]
            if ratio > ratio_bounds[0]:
                if ratio < ratio_bounds[1]:
                    print(product[0], product[1])
                    # print(num)
                    line = '{}, {}, {}, \n'.format(num, product[0], product[1])
                    outfile.write(line)
                    num += 1
        outfile.close()


if __name__ == '__main__':
    wv_list = np.linspace(300, 1000, num=8)
    wh_list = np.linspace(20, 40, num=5)

    dirpath = r"C:\Users\sugyan\Documents"
    fname = 'Sample_IMS_wh_wv_list.csv'

    make_file_without_ratio_bounds(dirpath, fname, wh_list, wv_list)