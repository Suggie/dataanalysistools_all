import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def read_calout_combine_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('combined_WH_WV.csv')]
    return files

def create_dataframe(file, dirpath):
    file_chars = str(file).split('_')
    file_type = file_chars[1]
    df = pd.read_csv(os.path.join(dirpath, file), sep=',')
    identity = df['id']

    unique_ids = np.unique(identity)
    id_arr = []
    oligomer_arr = []
    mass_arr = []
    charge_arr = []
    pub_ccs = []
    avg_pred_ccs = []
    std_pred_ccs = []
    avg_percent_ccs_dev = []
    std_percent_ccs_dev = []

    for index, item in enumerate(unique_ids):
        df_1_ = df[df['id'] == item]
        oligomer = df_1_['oligomer']
        for inde, oligo in enumerate(np.unique(oligomer)):
            df_2_ = df_1_[df_1_['oligomer'] == oligo]
            charge_ = df_2_['charge']
            unique_charge = np.unique(charge_)
            for ind, chz in enumerate(unique_charge):
                df_3_ = df_2_[df_2_['charge'] == chz]
                id_arr.append(item)
                oligomer_arr.append(oligo)
                mass_arr.append(np.array(df_3_['mass'])[0])
                charge_arr.append(np.array(df_3_['charge'])[0])

                if file_type == 'mob':
                    pub_ccs.append(np.array(df_3_['pub_ccs'])[0])
                    avg_pred_ccs.append(np.average(df_3_['pred_ccs']))
                    std_pred_ccs.append(np.std(df_3_['pred_ccs']))
                    avg_percent_ccs_dev.append(np.average(df_3_['pubccs_error_percent']))
                    std_percent_ccs_dev.append(np.std(df_3_['pubccs_error_percent']))

                if file_type == 'relax':
                    pub_ccs.append(np.array(df_3_['pub_ccs'])[0])
                    avg_pred_ccs.append(np.average(df_3_['pred_ccs']))
                    std_pred_ccs.append(np.std(df_3_['pred_ccs']))
                    avg_percent_ccs_dev.append(np.average(df_3_['pubccs_error_percent']))
                    std_percent_ccs_dev.append(np.std(df_3_['pubccs_error_percent']))

                if file_type == 'ruotoloetal':
                    pub_ccs.append(np.array(df_3_['pub_ccs'])[0])
                    avg_pred_ccs.append(np.average(df_3_['est_ccs']))
                    std_pred_ccs.append(np.std(df_3_['est_ccs']))
                    avg_percent_ccs_dev.append(np.average(df_3_['ccs_deviation']))
                    std_percent_ccs_dev.append(np.std(df_3_['ccs_deviation']))

    data_arr = np.array([id_arr, oligomer_arr, mass_arr, charge_arr, pub_ccs, avg_pred_ccs, std_pred_ccs,
                         avg_percent_ccs_dev, std_percent_ccs_dev])

    df_out = pd.DataFrame(data=data_arr.T, columns=['id', 'oligomer', 'mass', 'charge', 'pub_ccs',
                                                     'avg_pred_ccs', 'std_pred_ccs', 'avg_percent_ccs_dev', 'std_percent_ccs_dev'])
    df_out = df_out.sort_values(by=['id', 'mass'], ascending=[True, True])
    df_out.to_csv(os.path.join(dirpath, str(file).split('.')[0]+'_average.csv'))

    print('heho')


    # return df


if __name__=="__main__":
    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\cal_all_6terms"
    cal_out_files = read_calout_combine_files(dirpath)
    for file in cal_out_files:
        print(file)
        dataframe = create_dataframe(file, dirpath)
        print('heho')