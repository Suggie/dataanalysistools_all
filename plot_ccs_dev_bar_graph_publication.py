## input files and generate bar graph \

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import pandas as pd
import os


def gen_color_dict(df):
    """
    read the dataframe and generate a dictionary
    :param df: dataframe input
    :return: dictionary
    """

    color_group_list = ['gray' for _ in range(len(df['#id']))]
    color_species_list = ['gray' for _ in range(len(df['#id']))]
    moltype_list = ['peptide' for _ in range(len(df['#id']))]
    label_list = ['label' for _ in range(len(df['#id']))]


    for index, (id_, oligo_, mass_, charge_) in enumerate(zip(df['#id'], df['oligomer'], df['mass'], df['charge'])):
        label = '_'.join([id_, str(oligo_), str(charge_)])
        label_list[index] = label
        if mass_ < 4000:
            if id_.find('PolyAla') >=0:
                if charge_ == 1:
                    color_group_list[index] = 'blueviolet'
                    color_species_list[index] = 'blueviolet'
                    moltype_list[index] = 'peptide_1'
                if charge_ == 2:
                    color_group_list[index] = 'fuchsia'
                    color_species_list[index] = 'fuchsia'
                    moltype_list[index] = 'peptide_2'
                if charge_ == 3:
                    color_group_list[index] = 'teal'
                    color_species_list[index] = 'teal'
                    moltype_list[index] = 'peptide_3'
        else:
            if id_.find('denat') >=0:
                color_group_list[index] = 'darkorange'
                moltype_list[index] = 'denature_protein'
                if id_.find('cytochrome') >= 0:
                    color_species_list[index] = 'red'
                if id_.find('ubiquitin') >= 0:
                    color_species_list[index] = 'limegreen'
            else:
                color_group_list[index] = 'crimson'
                moltype_list[index] = 'native_protein'
                if id_.find('GDH') >= 0:
                    color_species_list[index] = 'steelblue'
                if id_.find('PVK') >= 0:
                    color_species_list[index] = 'orchid'
                if id_.find('transthyretin') >= 0:
                    color_species_list[index] = 'saddlebrown'
                if id_.find('conA') >= 0:
                    color_species_list[index] = 'pink'
                if id_.find('b-lactoglobulin') >= 0:
                    if oligo_ == 1:
                        color_species_list[index] = 'goldenrod'
                    if oligo_ == 2:
                        color_species_list[index] = 'khaki'
                if id_.find('cytochrome') >= 0:
                    color_species_list[index] = 'springgreen'
                if id_.find('ADH') >= 0:
                    color_species_list[index] = 'dodgerblue'
                if id_.find('avidin') >= 0:
                    color_species_list[index] = 'maroon'
                if id_.find('bsa') >= 0:
                    color_species_list[index] = 'midnightblue'

        # generate unique wh and wv conditions

    # wh_wv_key_list = []
    # for index, (wh_, wv_) in enumerate(zip(df['wh'], df['wv'])):
    #     wh_wv_key = '_'.join([str(wh_), str(wv_)])
    #     wh_wv_key_list.append(wh_wv_key)
    #
    # wh_wv_key_unique = np.unique(wh_wv_key_list)
    # color_wh_wv_iter = cm.rainbow(np.linspace(0, 1, len(wh_wv_key_unique)))
    #
    # color_wh_wv = ['gray' for _ in range(len(df['#id']))]
    #
    # for index, (wh_, wv_) in enumerate(zip(df['wh'], df['wv'])):
    #     wh_wv_key = '_'.join([str(wh_), str(wv_)])
    #     for ind, uniq_key in enumerate(wh_wv_key_unique):
    #         if wh_wv_key == uniq_key:
    #             color_wh_wv[index] = color_wh_wv_iter[ind]


    color_dict = dict()
    color_dict['label_list'] = label_list
    # color_dict['wh_wv_label_list'] = wh_wv_key_list
    color_dict['moltype'] = moltype_list
    color_dict['color_group'] = color_group_list
    color_dict['color_species'] = color_species_list
    # color_dict['color_wh_wv'] = color_wh_wv

    return color_dict




def plot_bar_graph_with_error(y_, y_err, x_labels, y_label, color_label):
    num = len(y_)
    ind = np.arange(num)
    width = 0.7
    hatch_type = ''
    fig, ax = plt.subplots(figsize=(20,10))
    if type(color_label) == str:
        rects = ax.bar(ind, y_, width, color='black', edgecolor='black',
                       hatch=hatch_type, alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
    else:
        for index, (ypos, ydata, ydata_err, color_) in enumerate(zip(ind, y_, y_err, color_label)):
            rects = ax.bar(ypos, ydata, width, color=color_,
                           edgecolor='black', hatch=hatch_type, alpha=1, yerr=ydata_err,
                           error_kw=dict(lw=1, capsize=1, capthick=1))
    plt.xticks(ind, x_labels, rotation='vertical')
    # plt.ylim((-15, 25))
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)


def plot_bar_graph_without_error(y_, x_labels, y_label, color_label):
    num = len(y_)
    ind = np.arange(num)
    width = 0.7
    hatch_type = ''
    fig, ax = plt.subplots(figsize=(20,10))
    if type(color_label) == str:
        rects = ax.bar(ind, y_, width, color='black', edgecolor='black',
                       hatch=hatch_type, alpha=1)
    else:
        for index, (ypos, ydata, color_) in enumerate(zip(ind, y_, color_label)):
            rects = ax.bar(ypos, ydata, width, color=color_,
                           edgecolor='black', hatch=hatch_type, alpha=1)
    plt.xticks(ind, x_labels, rotation='vertical')
    # plt.ylim((-15, 25))
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)


def plot_bar_graph_two(y1_, y1_err, y2_, y2_err, colorlabel1_, colorlabel2_, x_labels, y_label):
    num = len(y1_)
    ind = np.arange(num)
    width = 0.45
    fig, ax = plt.subplots(figsize=(20,10))
    hatch_type = '////'
    if type(colorlabel1_) == str and type(colorlabel2_) == str:
        rects1 = ax.bar(ind, y1_, width, color=colorlabel1_, edgecolor='black', alpha=1, yerr=y1_err,
                        error_kw=dict(lw=1, capsize=1, capthick=1))
        rects2 = ax.bar(ind+width, y2_, width, color=colorlabel2_, edgecolor='black', hatch=hatch_type, alpha=0.7, yerr=y1_err,
                        error_kw=dict(lw=1, capsize=1, capthick=1))
    else:
        for index, (ypos, ydata1, ydata2, yerr1, yerr2, colorlab1, colorlab2) in enumerate(zip(ind, y1_, y2_, y1_err,
                                                                                               y2_err, colorlabel1_,
                                                                                               colorlabel2_)):
            rects1 = ax.bar(ypos, ydata1, width, color=colorlab1, edgecolor='black', alpha=1, yerr=yerr1,
                            error_kw=dict(lw=1, capsize=1, capthick=1))
            rects2 = ax.bar(ypos+width, ydata2, width, color=colorlab2, edgecolor='black', hatch=hatch_type, alpha=0.8, yerr=yerr2,
                            error_kw=dict(lw=1, capsize=1, capthick=1))
    plt.xticks(ind+width/2, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)


def generate_color_label_list(x_label, color_dict):
    color_label_list = ['grey' for _ in range(len(x_label))]
    for ind, (x_id) in enumerate(x_label):
        if x_id.find('PolyAla') >= 0:
            polyala_charge = int(x_id.split('_')[-1])
            if polyala_charge == 1:
                if not color_dict:
                    color_label_list[ind] = 'blueviolet'
                else:
                    color_label_list[ind] = color_dict['polyala_z1']
            if polyala_charge == 2:
                if not color_dict:
                    color_label_list[ind] = 'fuchsia'
                else:
                    color_label_list[ind] = color_dict['polyala_z2']
            if polyala_charge == 3:
                if not color_dict:
                    color_label_list[ind] = 'teal'
                else:
                    color_label_list[ind] = color_dict['polyala_z3']
        else:
            if x_id.find('denat') >= 0:
                if not color_dict:
                    color_label_list[ind] = 'darkorange'
                else:
                    color_label_list[ind] = color_dict['denature_proteins']
            else:
                if not color_dict:
                    color_label_list[ind] = 'crimson'
                else:
                    color_label_list[ind] = color_dict['native_proteins']
    return color_label_list



def plot_data_two_bar_(fpath1, fpath2, y1_key, yerr1_key, y2_key, yerr2_key, outfname, color_mode, color_dict=None):
    """
    plot the data using plot two bar graph
    :param fpath: file path
    :param y1_key: y key for pandas
    :param yerr1_key: y err key for pandas
    :param y2_key: y key for pandas
    :param yerr2_key: y err key for pandas
    :param outfname: output fname
    :param color_mode: output directory location
    :param color_dict: color dict optional
    :return:
    """
    dirloc1, fname1 = os.path.split(fpath1)
    dirloc2, fname2 = os.path.split(fpath2)

    df1 = pd.read_csv(fpath1)
    df1 = df1.drop_duplicates()
    df1 = df1.sort_values(['mass', 'charge'], ascending=[True, True])

    df2 = pd.read_csv(fpath2)
    df2 = df2.drop_duplicates()
    df2 = df2.sort_values(['mass', 'charge'], ascending=[True, True])

    x_label = []
    for ind, (id, oligo, charge) in enumerate(zip(df1['#id'], df1['oligomer'], df1['charge'])):
        label_string = '_'.join([id, str(oligo), str(charge)])
        x_label.append(label_string)

    # plot_bar_graph_two(y1_, y1_err, y2_, y2_err, colorlabel1_, colorlabel2_, x_labels, y_label)

    if color_mode == 'str':
        color_label1 = 'black'
        color_label2 = 'lightgray'
        plot_bar_graph_two(df1[y1_key], df1[yerr1_key], df2[y2_key], df2[yerr2_key], color_label1, color_label2,
                           x_label, y1_key)
    else:
        color_label_list = generate_color_label_list(x_label, color_dict=color_dict)
        plot_bar_graph_two(df1[y1_key], df1[yerr1_key], df2[y2_key], df2[yerr2_key], color_label_list, color_label_list,
                           x_label, y1_key)

    outname = str(fname1).split('.csv')[0] + '_' + outfname
    plt.savefig(os.path.join(dirloc1, outname))
    plt.close()


def plot_data_one_bar_(fpath, y_key, yerr_key, outfname, color_mode):
    """
    plot the data using plot one bar graph
    :param fpath: file path
    :param y_key: y key for pandas
    :param yerr_key: y err key for pandas
    :param color_mode: str or list bool option
    :param color_label: str or list type
    :param outfname: output fname
    :param outdirloc: output directory location
    :return:
    """
    dirloc, fname = os.path.split(fpath)
    df = pd.read_csv(fpath)
    df = df.drop_duplicates()
    df = df.sort_values(['mass', 'charge'], ascending=[True, True])

    color_dict_ = gen_color_dict(df)

    mol_types = ['peptide_1', 'peptide_2', 'peptide_3', 'denature_protein', 'native_protein']

    color_label = []
    ydata = []
    yerr_data = []
    x_label = []

    if yerr_key:

        for mol in mol_types:
            for ind, (ydata_, yerr_data_, dict_moltype, dict_label, dict_color) in enumerate(zip(df[y_key], df[yerr_key],
                                                                                   color_dict_['moltype'],
                                                                                   color_dict_['label_list'],
                                                                                                 color_dict_['color_species'])):
                if dict_moltype == mol:
                    ydata.append(ydata_)
                    yerr_data.append(yerr_data_)
                    x_label.append(dict_label)
                    color_label.append(dict_color)

        plot_bar_graph_with_error(ydata, yerr_data, x_label, y_key, color_label)
        outname = str(fname).split('.csv')[0] + '_' + outfname
        plt.savefig(os.path.join(dirloc, outname))
        plt.close()

    else:
        for mol in mol_types:
            for ind, (ydata_, dict_moltype, dict_label, dict_color) in enumerate(
                    zip(df[y_key],
                        color_dict_['moltype'],
                        color_dict_['label_list'],
                        color_dict_['color_species'])):
                if dict_moltype == mol:
                    ydata.append(ydata_)
                    x_label.append(dict_label)
                    color_label.append(dict_color)

        plot_bar_graph_without_error(ydata, x_label, y_key, color_label)
        outname = str(fname).split('.csv')[0] + '_' + outfname
        plt.savefig(os.path.join(dirloc, outname))
        plt.close()



if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\CrossClass\CalDenat_UncalNative"

    # cal_mode_string_list = ['power_law_True',
    #                         'power_law_True_exp_True',
    #                         'relax_True_terms_6',
    #                         'relax_True_terms_6_exp_True',
    #                         'blended_True',
    #                         'blended_True_exp_True']

    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True',
                            'blended_True',
                            'blended_True_exp_True']

    cal_mode_string_list_2bar = [['power_law_True',
                            'power_law_True_exp_True'],
                                 ['relax_True_terms_6',
                            'relax_True_terms_6_exp_True'],
                                 ['blended_True',
                            'blended_True_exp_True']]
    for cal_mode in cal_mode_string_list:
        ccs_dev_fname = cal_mode + '_ccs_dev_output_species.csv'
        ccsdev_fpath = os.path.join(dirpath, ccs_dev_fname)
        plot_data_one_bar_(fpath=ccsdev_fpath,
                           y_key='ccs_dev_avg',
                           yerr_key='ccs_dev_std',
                           color_mode='list',
                           outfname='ccs_dev_species.png')
        plot_data_one_bar_(fpath=ccsdev_fpath,
                           y_key='ccs_rmse',
                           yerr_key=None,
                           color_mode='list',
                           outfname='ccs_rmse_species.png')
    # for cal_mode_2 in cal_mode_string_list_2bar:
    #     ccs_dev_fname1 = cal_mode_2[0] + '_ccs_dev_output.csv'
    #     ccs_dev_fname2 = cal_mode_2[1] + '_ccs_dev_output.csv'
    #     ccsdev_fpath1 = os.path.join(dirpath, ccs_dev_fname1)
    #     ccsdev_fpath2 = os.path.join(dirpath, ccs_dev_fname2)
    #     # plot_data_two_bar_(fpath1, fpath2, y1_key, yerr1_key, y2_key, yerr2_key, outfname, color_mode, color_dict=None)
    #     plot_data_two_bar_(fpath1=ccsdev_fpath1,
    #                        fpath2=ccsdev_fpath2,
    #                        y1_key='ccs_dev_avg',
    #                        yerr1_key='ccs_dev_std',
    #                        y2_key='ccs_dev_avg',
    #                        yerr2_key='ccs_dev_std',
    #                        color_mode='list',
    #                        outfname='ccs_dev_list_mode_2bar.pdf')