# get ccs and vel deviation from each condition and get average and std deviation

import os
import pandas as pd
import numpy as np


def get_file_endid(dirpath, endid='cal_output'):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files

def gen_calibrants_dev_output_wh_wv(dirpath, cal_mode_string):
    end_id = cal_mode_string + '_calibrants_output.csv'
    file_list = get_file_endid(dirpath, endid=end_id)

    wh_arr = []
    wv_arr = []
    id_arr = []
    oligomer_arr = []
    mass_arr = []
    charge_arr = []
    pred_ccs_arr = []
    pred_mob_arr = []
    vel_dev_arr = []
    ccs_dev_arr = []
    for file in file_list:
        with open(os.path.join(dirpath, file), 'r') as calout_file:
            calib_f = calout_file.read().splitlines()
            for line in calib_f:
                if line.startswith('#Waveht'):
                    wh = line.split(',')[-1]
                    wh_arr.append(float(wh))
                if line.startswith('#Wavevel'):
                    wv = line.split(',')[-1]
                    wv_arr.append(float(wv))

            df = pd.read_csv(os.path.join(dirpath, file), header=11)
            pred_ccs_arr.append(df['pred_ccs'].values)
            pred_mob_arr.append(df['pred_mobility'].values)
            ccs_dev_arr.append(df['ccs_deviation'].values)
            vel_dev_arr.append(df['vel_deviation'].values)

            id_arr.append(df['#id'].values)
            oligomer_arr.append(df['oligomer'].values)
            mass_arr.append(df['mass'].values)
            charge_arr.append(df['charge'].values)


    id_tp = np.array(id_arr).T
    oligo_tp = np.array(oligomer_arr).T
    mass_tp = np.array(mass_arr).T
    charge_tp = np.array(charge_arr).T

    pred_ccs_tp = np.array(pred_ccs_arr).T
    pred_mob_tp = np.array(pred_mob_arr).T
    ccs_dev_tp = np.array(ccs_dev_arr).T
    vel_dev_tp = np.array(vel_dev_arr).T



    for ind, id_array_ in enumerate(id_tp):

        output_string = ''
        header= '#id,oligomer,mass,charge,wh,wv,pred_ccs,pred_mob,ccs_dev,vel_dev\n'
        output_string += header

        for num in range(len(id_array_)):
            line = '{},{},{},{},{},{},{},{},{},{}\n'.format(id_array_[num],
                                                            oligo_tp[ind][num],
                                                            mass_tp[ind][num],
                                                            charge_tp[ind][num],
                                                            wh_arr[num],
                                                            wv_arr[num],
                                                            pred_ccs_tp[ind][num],
                                                            pred_mob_tp[ind][num],
                                                            ccs_dev_tp[ind][num],
                                                            vel_dev_tp[ind][num])
            output_string += line

        spec_id = str(id_array_[0]) + '_' + str(oligo_tp[ind][0]) + '_' + str(charge_tp[ind][0])
        out_fname = cal_mode_string + '_' + spec_id + '_species_ccs_dev_output.csv'


        with open(os.path.join(dirpath, out_fname), 'w') as outfile:
            outfile.write(output_string)
            outfile.close()


        print('heho')


# add the wh wv exception list

def gen_calibrants_dev_output(dirpath, cal_mode_string, wh_wv_exception_list):
    end_id = cal_mode_string + '_calibrants_output.csv'
    file_list = get_file_endid(dirpath, endid=end_id)

    pred_ccs_arr = []

    pred_mob_arr = []
    vel_dev_arr = []
    ccs_dev_arr = []
    for file in file_list:
        with open(os.path.join(dirpath, file), 'r') as calout_file:
            calib_f = calout_file.read().splitlines()
            for line in calib_f:
                if line.startswith('#Waveht'):
                    wh = line.split(',')[-1]
                    # wh = float(wh)
                if line.startswith('#Wavevel'):
                    wv = line.split(',')[-1]
                    # wv = float(wv)

            wh_wv_key = '_'.join([wh, wv])
            wh_wv_ex_key_list = []
            for ind, wh_wv_ex_list in enumerate(wh_wv_exception_list):
                wh_ex = float(wh_wv_ex_list[0])
                wv_ex = float(wh_wv_ex_list[1])
                wh_wv_ex_key = '_'.join([str(wh_ex), str(wv_ex)])
                wh_wv_ex_key_list.append(wh_wv_ex_key)

            if wh_wv_key not in wh_wv_ex_key_list:


                df = pd.read_csv(os.path.join(dirpath, file), header=11)
                pred_ccs_arr.append(df['pred_ccs'].values)
                pred_mob_arr.append(df['pred_mobility'].values)
                ccs_dev_arr.append(df['ccs_deviation'].values)
                vel_dev_arr.append(df['vel_deviation'].values)

                id_arr = df['#id'].values
                oligomer = df['oligomer'].values
                mass_arr = df['mass'].values
                charge = df['charge'].values

                superclass = df['superclass'].values
                subclass = df['subclass'].values



    pred_ccs_avg = np.average(pred_ccs_arr, axis=0)
    pred_ccs_std = np.std(pred_ccs_arr, axis=0)

    pred_mob_avg = np.average(pred_mob_arr, axis=0)
    pred_mob_std = np.std(pred_mob_arr, axis=0)

    ccs_dev_avg = np.average(ccs_dev_arr, axis=0)
    ccs_dev_std = np.std(ccs_dev_arr, axis=0)

    vel_dev_avg = np.average(vel_dev_arr, axis=0)
    vel_dev_std = np.std(vel_dev_arr, axis=0)

    output_string = ''
    header = '#id,oligomer,mass,charge,superclass,subclass,pred_ccs_avg,pred_ccs_std,pred_mob_avg,pred_mob_std,ccs_dev_avg,ccs_dev_std,vel_dev_avg,vel_dev_std\n'
    output_string += header

    for num in range(len(id_arr)):
        line = '{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(id_arr[num],
                                                        oligomer[num],
                                                        mass_arr[num],
                                                        charge[num],
                                                              superclass[num],
                                                              subclass[num],
                                                        pred_ccs_avg[num],
                                                        pred_ccs_std[num],
                                                        pred_mob_avg[num],
                                                        pred_mob_std[num],
                                                        ccs_dev_avg[num],
                                                        ccs_dev_std[num],
                                                        vel_dev_avg[num],
                                                              vel_dev_std[num])
        output_string += line

    out_fname = cal_mode_string + '_ccs_dev_output.csv'

    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()

    print('heho')



if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\051819_CalProcessing\All"
    # cal_mode_string_list = ['blended_True',
    #                         'blended_True_exp_True',
    #                         'power_law_True',
    #                         'power_law_True_exp_True',
    #                         'relax_True_terms_6',
    #                         'relax_True_terms_6_exp_True']
    cal_mode_string_list = ['blended_True',
                            'blended_True_exp_True',
                            'power_law_True',
                            'power_law_True_exp_True',]
    exception_list = [[35,1000], [20,300]]
    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        gen_calibrants_dev_output(dirpath, cal_mode_string, exception_list)
        # gen_calibrants_dev_output_wh_wv(dirpath, cal_mode_string)