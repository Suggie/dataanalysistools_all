import os
import numpy as np

def list_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('outarraygaussfit_blur.csv')]
    return files

def sort_files(file_list):
    file_id_arr = []
    for file in file_list:
        file_id = str(file).split('#')[-1]
        file_id_arr.append(file_id)
    file_id_unique = np.unique(file_id_arr)
    sort_file_list = [[] for i in range(len(file_id_unique))]
    for ind, unique_id in enumerate(file_id_unique):
        for index, id in enumerate(file_id_arr):
            if unique_id == id:
                sort_file_list[ind].append(file_list[index])
    return file_id_unique, sort_file_list

def sort_combine_array_wh_wv(array):
    wv_arr = array[:, 1]
    wh_arr = array[:, 0]
    ind = np.lexsort((wh_arr, wv_arr))
    # sort_ind_1 = np.argsort(wv_arr)
    # sort_1 = array[sort_ind_1]
    # sort_ind_2 = np.argsort(wh_arr)
    sort_2 = array[ind]
    return sort_2

def combine_files(file_id_unique, sort_file_list, dirpath):
    for ind, (unique_id, sorted_list) in enumerate(zip(file_id_unique, sort_file_list)):
        combine_arr = []
        for file in sorted_list:
            data = np.genfromtxt(os.path.join(dirpath, file), delimiter=',', skip_header=1)
            combine_arr.append(data)
        combine_arr = np.vstack(combine_arr)
        sort_arr = sort_combine_array_wh_wv(combine_arr)
        header = 'WH,WV,y0,Amp,xc,w,fwhm,res,lnreg_slope,lnreg_intercept,lnreg_r2,lnreg_adjrsq,gauss_blur_sigma'
        fname = unique_id.split('outarraygaussfit')[0] + 'blur_combined_gaussfit.csv'
        np.savetxt(os.path.join(dirpath, fname), sort_arr, fmt='%s', delimiter=',', header=header)

if __name__=='__main__':
    dirpath = r"C:\MassLynx\Default.pro\Data\070519\TWIMExtract_Output\SDGRG"
    file_list = list_files(dirpath)
    file_id_unique, sort_file_list = sort_files(file_list)
    combine_files(file_id_unique, sort_file_list, dirpath)