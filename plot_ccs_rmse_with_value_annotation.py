
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors


def plot_imshow_scale(data_grid, colormap, vmin, vmax, scale):
    """
    plot imshow with different scales (logarithmic, sym log, power law)
    :param z: z grid
    :param x: x vals
    :param y: y vals
    :param xkey: label x
    :param ykey: label y
    :param zkey: label z
    :param scale: log, sym log, power law
    :param colormap: color map used for plotting
    :param vmin: min value for color
    :param vmax: max value for color
    :return: void
    """

    curr_cmap = plt.cm.get_cmap(colormap)
    curr_cmap.set_bad(color='grey', alpha=1)
    if scale == 'log':
        norm_color = colors.LogNorm(vmin=vmin, vmax=vmax)
    if scale == 'sym_log':
        norm_color = colors.SymLogNorm(linthresh=0.03, linscale=0.03, vmin=vmin, vmax=vmax)
    if scale == 'sqrt':
        norm_color = colors.PowerNorm(gamma=0.5, vmin=vmin, vmax=vmax)

    plt.imshow(data_grid, cmap=curr_cmap, norm=norm_color)
    plt.colorbar()



def gen_contour_plot(fpath, colormap, vmin, vmax, scale):

    dirpath, fname = os.path.split(fpath)
    data = np.genfromtxt(fpath, delimiter=',')
    plot_imshow_scale(data, colormap, vmin, vmax, scale)
    plt.savefig(os.path.join(dirpath, fname+'_contour.pdf'))
    plt.close()
    print('heho')

def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files

if __name__=='__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_fixa\_CrossCls"
    files = get_files(dirpath, endid='ccs_rmse.csv')
    for ind, file in enumerate(files):
        fpath = os.path.join(dirpath, file)
        gen_contour_plot(fpath, colormap='PuBuGn', vmin=1, vmax=90, scale='sqrt')

    # fpath = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_fixa\_CrossCls\cross_cls_ccs_rmse_summary_v2.csv"
    # gen_contour_plot(fpath)