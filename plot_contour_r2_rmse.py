import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
from scipy import stats


# def plot_tricontourf(x, y, z, levels=25):
#     plt.tricontourf(wh, wv, r2, cmap='jet')
#     plt.colorbar()
#     plt.xlabel('Wave height')
#     plt.ylabel('Wave velocity')
#     plt.title('CCS Calibration '+'$R^2$')

def construct_grid(wh, wv, data):
    wv_unique = np.unique(wv)
    wh_unique = np.unique(wh)
    data_grid = np.zeros(shape=(len(wv_unique), len(wh_unique)))
    ind_new = 0
    for ind, (wv_, wh_) in enumerate(zip(wv, wh)):
        for i, wv_un in enumerate(wv_unique):
            for j, wh_un in enumerate(wh_unique):
                if wv_un == wv_:
                    if wh_un == wh_:
                        data_grid[i, j] = data[ind_new]
                        ind_new += 1

    data_grid[data_grid == 0] = 'nan'
    return data_grid

def plot_imshow(r2_grid, wh, wv, clim = None):
    curr_cmap = plt.cm.get_cmap('inferno')
    curr_cmap.set_bad(color='grey')
    plt.imshow(r2_grid, origin='lower', cmap=curr_cmap)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(wh)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(wv)), 1))
    ax.set_xticklabels(np.unique(wh))
    ax.set_yticklabels(np.unique(wv))
    if clim != None:
        plt.clim(clim)
    plt.colorbar()
    plt.xlabel('Wave height')
    plt.ylabel('Wave velocity')
    # plt.title('CCS Calibration ' + '$R^2$')

def plot_wh_wv_ratio_rmse(wh, wv, rmse):
    whwv = wh/wv
    plt.scatter(whwv, rmse, color='black')
    plt.xlabel('wave height / wave velocity')
    plt.ylabel('% RMSE')

def plot_wv_rmse(wv, rmse):
    plt.scatter(wv, rmse, color='black')
    plt.xlabel('wave velocity')
    plt.ylabel('% RMSE')

def plot_wh_rmse(wh, rmse):
    plt.scatter(wh, rmse, color='black')
    plt.xlabel('wave height')
    plt.ylabel('% RMSE')


def getdata_cal_all(file, path):
    fpath = os.path.join(path, file)
    df = pd.read_csv(fpath, sep=',')
    # df = df[(df['wh'] != 40) | (df['wv'] != 300)]
    df = df[(df['wv'] == 300) | (df['wv'] == 400)]
    # df = df[(df['wh'] != 40) | (df['wv'] != 300)]
    # df = df[(df['wh'] != 35) | (df['wv'] != 300)]
    # df = df[(df['wh'] != 30) | (df['wv'] != 300)]
    # df = df[(df['wh'] != 40) | (df['wv'] != 400)]
    # df = df[(df['wh'] != 35) | (df['wv'] != 1000)]
    return df

def getfiles(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('#calibration_.csv')]
    # files = [x for x in files if x.endswith('cal_output.csv')]
    return files


if __name__=='__main__':
    dirloc = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\NativeProteins\All_ions"
    files = getfiles(dirloc)
    store_array = []
    rmse_store = []
    rmse_file_list = []
    file_id_list = []

    outlines = ''
    header = '#caltype,avg_rmse,std_rmse,min_rmse,max_rmse,min_rmse_wh,min_rmse_wv,max_rmse_wh,max_rmse_wv\n'
    outlines += header

    for file in files:

        cal_type = '_'.join(str(file).split('_')[:4])
        file_id_list.append(cal_type)

        df= getdata_cal_all(file, dirloc)
        wh = df['wh'].values
        wv = df['wv'].values
        rmse = df['rmse_percent'].values
        rmse_file_list.append(rmse)
        r2 = df['R2'].values

        min_rmse = np.min(rmse)
        max_rmse = np.max(rmse)
        avg_rmse = np.average(rmse)
        std_rmse = np.std(rmse)

        min_rmse_df = df[(df['rmse_percent'] == min_rmse )]
        max_rmse_df = df[(df['rmse_percent'] == max_rmse )]

        min_wh = min_rmse_df['wh'].values[0]
        min_wv = min_rmse_df['wv'].values[0]

        max_wh = max_rmse_df['wh'].values[0]
        max_wv = max_rmse_df['wv'].values[0]

        line = '{},{},{},{},{},{},{},{},{}\n'.format(cal_type, avg_rmse, std_rmse, min_rmse, max_rmse, min_wh, min_wv, max_wh, max_wv)
        outlines += line


        fname_r2 = str(file) + '_300WV_400WV_calibrationR2'
        fname_rmse = str(file) + '_300WV_400WV_calibrationRMSE'
        r2_grid = construct_grid(wh, wv, r2)
        rmse_grid = construct_grid(wh, wv, rmse)

        rmse_store.append(rmse_grid)
        store_array.append([wh, wv, rmse, rmse_grid, fname_rmse])


    with open(os.path.join(dirloc, '300WV_400WV_rmse_stats.csv'), 'w') as rmsestat:
        rmsestat.write(outlines)
        rmsestat.close()


    outlines2 = ''
    header2 = '#caltype1,caltype2,t-stat,p-value\n'
    outlines2 += header2

    for ind, (file_id_combo, rmse_combo) in enumerate(zip(itertools.combinations(file_id_list, 2), itertools.combinations(rmse_file_list, 2))):
        ttest = stats.ttest_ind(rmse_combo[0], rmse_combo[1])
        line2 = '{},{},{},{}\n'.format(file_id_combo[0], file_id_combo[1], ttest[0], ttest[1])
        outlines2 += line2

    with open(os.path.join(dirloc, '300WV_400WV_rmse_stats_ttest.csv'), 'w') as rmsettest:
        rmsettest.write(outlines2)
        rmsettest.close()



    flat_rmse_arr = np.array(rmse_store).flatten()
    min_rmse = np.nanmin(flat_rmse_arr)
    max_rmse = np.nanmax(flat_rmse_arr)
    for arr in store_array:
        plot_imshow(arr[3], arr[0], arr[1], clim=(min_rmse, max_rmse))
        plt.savefig(os.path.join(dirloc, arr[4]+'_samecolorbar.png'))
        plt.close()

        plot_imshow(arr[3], arr[0], arr[1])
        plt.savefig(os.path.join(dirloc, arr[4]+'.png'))
        plt.close()

        plot_wh_wv_ratio_rmse(arr[0], arr[1], arr[2])
        plt.savefig(os.path.join(dirloc, arr[4] + '_whoverwv.png'))
        plt.close()

        plot_wv_rmse(arr[1], arr[2])
        plt.savefig(os.path.join(dirloc, arr[4] + '_wv.png'))
        plt.close()

        plot_wh_rmse(arr[0], arr[2])
        plt.savefig(os.path.join(dirloc, arr[4] + '_wh.png'))
        plt.close()