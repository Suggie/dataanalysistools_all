import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt

def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files


def plot_violin_plot(data, color_labels, marker_list, outid, image_type, dirpath, xlim=None, ylim=None, y_int=1.0):

    for ind, (data_, color_, marker_) in enumerate(zip(data, color_labels, marker_list)):
        x_rep = np.random.normal(ind+1, 0.02, size=len(data_))
        plt.scatter(x_rep, data_, marker=marker_, color=color_, alpha=0.7)


    violin_body_dict = dict(color='r', alpha=0.5)
    violin_plot = plt.violinplot(data, vert=True, showmeans=True, showextrema=True, widths=0.25, bw_method='scott')

    # box_prop_dict = dict(linewidth=1.0)
    # whisker_prop_dict = dict(linewidth=1.0)
    # cap_prop_dict = dict(linewidth=1.0)
    # median_prop_dict = dict(linestyle='-', color='black', linewidth=1.0)
    # mean_prop_dict = dict(linestyle='-', color='red', linewidth=1.0)
    # bp = plt.boxplot(data, showfliers=False, meanline=True, showmeans=True,
    #                  boxprops=box_prop_dict,
    #                  whiskerprops=whisker_prop_dict,
    #                  capprops=cap_prop_dict,
    #                  medianprops=median_prop_dict,
    #                  meanprops=mean_prop_dict)


    outname = outid + '_violin_plot' + image_type

    plt.savefig(os.path.join(dirpath, outname))
    plt.close()





def plot_violin_for_charge_state(dirpath, pred_file_name, outid, image_type='.png'):
    """
    use the dataframe to create blox whisker plot for each charge state
    :param df: pandas dataframe
    :return:
    """
    df = pd.read_csv(os.path.join(dirpath, pred_file_name), header=12)
    uniq_charge = np.unique(df['charge'].values)
    data_ = [[] for _ in range(len(uniq_charge))]
    for ind, charge in enumerate(uniq_charge):
        df_charge = df[df['charge']==charge]
        pred_ccs = df_charge['pred_ccs'].values
        data_[ind]=pred_ccs

    color_labels = ['r', 'g', 'b']
    marker_list = ['o', 'o', 'o']

    plot_violin_plot(data_, color_labels, marker_list, outid, image_type, dirpath)



def plot_charge_ccs_multiple_conf(dirpath, ccs_stats_file, marker_color, outid, image_type='.pdf'):

    df = pd.read_csv(os.path.join(dirpath, ccs_stats_file))

    charge = df['charge'].values
    mean_ccs = df['mean'].values
    err = df['total_err'].values

    plt.errorbar(charge, mean_ccs, yerr=err, marker='o', ls='none', capsize=2, capthick=1, ecolor='black', elinewidth=2,
                 markeredgecolor='black',
                 markerfacecolor=marker_color,
                 markersize=10)
    plt.xlabel('z')
    plt.ylabel('CCS')
    plt.ylim((8,23))
    plt.xticks(np.arange(5, 24))

    outname = outid + '_ccs_vs_charge' + image_type

    plt.savefig(os.path.join(dirpath, outname))
    plt.close()






def generate_ccs_stats(dirpath, pred_file_name, outid, database_cal_err=3):
    """
    give the charge state and conformation specific num, mean, median, std, ccsrmse_err
    :param pred_file_name: pred file output from twim ccs calibration
    :return:
    """

    ccs_rmse = 0

    with open(os.path.join(dirpath, pred_file_name), 'r') as predfile:
        predfile = predfile.read().splitlines()
        for line in predfile:
            if line.startswith('#CCS_RMSE'):
                chars = line.split(',')
                ccs_rmse += float(chars[1])


    df = pd.read_csv(os.path.join(dirpath, pred_file_name), header=12)
    uniq_charge = np.unique(df['charge'].values)


    mean_arr = []
    median_arr = []
    std_arr = []
    num_arr = []
    total_err_arr = []
    mass_arr = []
    conf_arr = []


    for ind, charge in enumerate(uniq_charge):
        df_charge = df[df['charge']==charge]
        uniq_conf = np.unique(df_charge['#id'].values)
        conf_arr.append(uniq_conf)
        mass_ = [[] for _ in range(len(uniq_conf))]
        mean_ = [[] for _ in range(len(uniq_conf))]
        median_ = [[] for _ in range(len(uniq_conf))]
        std_ = [[] for _ in range(len(uniq_conf))]
        num_ = [[] for _ in range(len(uniq_conf))]
        total_err_ = [[] for _ in range(len(uniq_conf))]

        for index, conf in enumerate(uniq_conf):

            df_conf = df_charge[df_charge['#id']==conf]
            pred_ccs = df_conf['pred_ccs'].values
            mass_[index] = df_conf['mass'].values[0]
            num_[index] = len(pred_ccs)
            if len(pred_ccs) > 1:
                mean_[index] = np.mean(pred_ccs)
                median_[index] = np.median(pred_ccs)
                std_[index] = np.std(pred_ccs)
                cal_err = (ccs_rmse/100) * np.mean(pred_ccs)
                total_err = sqrt(np.std(pred_ccs)**2 + cal_err**2 + ((database_cal_err/100)*np.mean(pred_ccs))**2)
                total_err_[index] = total_err
            else:
                mean_[index] = pred_ccs[0]
                median_[index] = np.nan
                std_[index] = np.nan
                total_err = sqrt((ccs_rmse/100) * np.mean(pred_ccs) + ((database_cal_err/100)*np.mean(pred_ccs))**2)
                total_err_[index] = total_err


        mean_arr.append(mean_)
        median_arr.append(median_)
        std_arr.append(std_)
        num_arr.append(num_)
        total_err_arr.append(total_err_)
        mass_arr.append(mass_)


    header = 'conf,charge,mass,num,mean,median,std,cal_ccs_rmse_percent,database_err_percent,total_err\n'

    data_string = ''

    for index, (charge, mass, conf_, num_, mean_, median_, std_, total_err_i) in enumerate(zip(uniq_charge, mass_arr, conf_arr, num_arr, mean_arr,
                                                                                  median_arr, std_arr, total_err_arr)):
        for num, conf in enumerate(conf_):

            line = '{},{},{},{},{},{},{},{},{},{}\n'.format(conf, charge, mass[num], num_[num], mean_[num], median_[num], std_[num],
                                                         ccs_rmse, database_cal_err, total_err_i[num])

            data_string += line

    output_string = header + data_string

    outname = str(pred_file_name).split('.csv')[0] + '_' + outid + '_ccs_stats.csv'

    with open(os.path.join(dirpath, outname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


def gen_ccs_stats_multiple_files(dirpath, file_list, outid, database_cal_err=3):

    ccs_rmse_arr = []
    ccs_pred_arr = []
    ccs_cal_err = []
    id_arr = []
    oligo_arr = []
    mass_arr = []
    charge_arr = []

    for index, file in enumerate(file_list):

        with open(os.path.join(dirpath, file), 'r') as predfile:
            predfile = predfile.read().splitlines()
            for line in predfile:
                if line.startswith('#CCS_RMSE'):
                    chars = line.split(',')
                    ccs_rmse = float(chars[1])
                    ccs_rmse_arr.append(ccs_rmse)

        df = pd.read_csv(os.path.join(dirpath, file), header=12)
        ccs_preds = df['pred_ccs'].values
        ccs_cal_err.append((ccs_rmse/100)*ccs_preds)
        ccs_pred_arr.append(ccs_preds)
        id_arr.append(df['#id'].values)
        oligo_arr.append(df['oligomer'].values)
        mass_arr.append(df['mass'].values)
        charge_arr.append(df['charge'].values)


    mean_ccs = np.mean(ccs_pred_arr, axis=0)
    std_ccs = np.std(ccs_pred_arr, axis=0)
    total_cal_err_terms = np.sum(np.square(ccs_cal_err), axis=0)
    std_err_term = np.square(std_ccs)
    total_err = np.sqrt(np.square((database_cal_err/100)*mean_ccs) + total_cal_err_terms + std_err_term)

    header = '#id,oligomer,mass,charge,mean,std,total_err\n'

    data_string = ''

    for index, (id_, oligo_, mass_, charge_, mean_, std_, total_err_) in enumerate(
            zip(id_arr[0], oligo_arr[0], mass_arr[0], charge_arr[0], mean_ccs, std_ccs, total_err)):
        line = '{},{},{},{},{},{},{}\n'.format(id_,
                                               oligo_,
                                               mass_,
                                               charge_,
                                               mean_,
                                               std_,
                                               total_err_)
        data_string += line

    output_string = header + data_string

    outname = outid + '_ccs_stats.csv'

    with open(os.path.join(dirpath, outname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


if __name__ == '__main__':

    # dirpath = r"C:\Users\sugyan\Documents\Processed data\071819_Nanodisc_peptide_datanalysis\CCS_rIAPP_Lipids"
    # pred_files = get_files(dirpath, endid='_pred.csv')
    #
    # for file in pred_files:
    #
    #     generate_ccs_stats(dirpath, file, outid='riapp_exp', database_cal_err=3)


    dirpath = r"C:\Users\sugyan\Documents\Processed data\071819_Nanodisc_peptide_datanalysis\CCS_rIAPP_Lipids"
    pred_files_list = get_files(dirpath, endid='blended_True_exp_True_pred.csv')
    gen_ccs_stats_multiple_files(dirpath, pred_files_list, outid='rIAPP_blended_True_exp_True')

