import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def get_files(startid, endid, dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith(startid)]
    files = [x for x in files if x.endswith(endid)]
    return files

def makenewdir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
    return dirpath

def construct_grid(wh, wv, data):
    wv_unique = np.unique(wv)
    wh_unique = np.unique(wh)
    data_grid = np.zeros(shape=(len(wv_unique), len(wh_unique)))
    ind_new = 0
    for ind, (wv_, wh_) in enumerate(zip(wv, wh)):
        for i, wv_un in enumerate(wv_unique):
            for j, wh_un in enumerate(wh_unique):
                if wv_un == wv_:
                    if wh_un == wh_:
                        data_grid[i, j] = data[ind_new]
                        ind_new += 1

    data_grid[data_grid == 0] = 'nan'
    return data_grid

def plot_imshow(r2_grid, wh, wv, clim = None):
    curr_cmap = plt.cm.get_cmap('inferno')
    curr_cmap.set_bad(color='grey')
    plt.imshow(r2_grid, origin='lower', cmap=curr_cmap)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(wh)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(wv)), 1))
    ax.set_xticklabels(np.unique(wh))
    ax.set_yticklabels(np.unique(wv))
    if clim != None:
        plt.clim(clim)
    plt.colorbar()
    plt.xlabel('Wave height')
    plt.ylabel('Wave velocity')

dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\All_ions\Cross_Val_each_ion"

start_id_list = ['ccs_error_powerfit_powerfitexp_False',
                 'ccs_error_powerfit_powerfitexp_True',
                 'ccs_error_relaxation_True_terms_4',
                 'ccs_error_relaxation_True_terms_6',
                 'ccs_error_relaxation_True_terms_10',
                 'ccs_error_relaxation_False_terms_4',
                 'ccs_error_relaxation_False_terms_6',
                 'ccs_error_relaxation_False_terms_10',
                 'ccs_error_relaxationexp_True_terms_4',
                 'ccs_error_relaxationexp_True_terms_6',
                 'ccs_error_relaxationexp_True_terms_10']
endid = '.csv'

for startid in start_id_list:
    print(startid + ' ...')
    outdirpath = os.path.join(dirpath, startid)
    outdir = makenewdir(outdirpath)

    files = get_files(startid, endid, dirpath)
    for file in files:

        fname = str(file)
        df = pd.read_csv(os.path.join(dirpath, fname), sep=',')
        df = df[(df['wh'] != 40) | (df['wv'] != 300)]
        df = df[(df['wh'] != 35) | (df['wv'] != 300)]
        # df = df[(df['wh'] != 30) | (df['wv'] != 300)]
        df = df[(df['wh'] != 40) | (df['wv'] != 400)]
        # df.drop(df[df.wh == 40 | df.wv == 300])
        # df[df.wv != 300 and df.wh != 40]
        waveht = df['wh'].values
        wavevel = df['wv'].values
        voverv = np.divide(waveht, wavevel)
        ccserr = df['pred_ccs_error_percent'].values
        ccserr_grid = construct_grid(waveht, wavevel, ccserr)

        plt.scatter(voverv, ccserr)
        # plt.xlim((0.02, 0.12))
        plt.xlabel('V/v')
        plt.ylabel('% CCS error')
        plt.savefig(os.path.join(u"\\\\?\\"+ outdir, fname + 'Voverv_ccserror.png'), dpi=500)
        plt.close()

        plot_imshow(ccserr_grid, waveht, wavevel)  # , clim=(1, 10))
        plt.savefig(os.path.join(outdir, fname + 'WH_WV_ccserror.png'), dpi=500)
        plt.close()