# plot contour

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from midpoint_normalize_class import MidpointNormalize
import matplotlib.colors as colors



def get_rmse_file(dirpath, endid):
    files = os.listdir(dirpath)
    files=[x for x in files if x.endswith(endid)]
    return files


def plot_imshow_scale(data_grid, x_val, y_val, x_key, y_key, z_key, colormap, vmin, vmax, scale):
    """
    plot imshow with different scales (logarithmic, sym log, power law)
    :param z: z grid
    :param x: x vals
    :param y: y vals
    :param xkey: label x
    :param ykey: label y
    :param zkey: label z
    :param scale: log, sym log, power law
    :param colormap: color map used for plotting
    :param vmin: min value for color
    :param vmax: max value for color
    :return: void
    """

    curr_cmap = plt.cm.get_cmap(colormap)
    curr_cmap.set_bad(color='grey', alpha=1)
    if scale == 'log':
        norm_color = colors.LogNorm(vmin=vmin, vmax=vmax)
    if scale == 'sym_log':
        norm_color = colors.SymLogNorm(linthresh=0.03, linscale=0.03, vmin=vmin, vmax=vmax)
    if scale == 'sqrt':
        norm_color = colors.PowerNorm(gamma=0.5, vmin=vmin, vmax=vmax)
    # if not scale == str:
    #     norm_color = colors.PowerNorm(gamma=scale, vmin=vmin, vmax=vmax)


    plt.imshow(data_grid, origin='lower', cmap=curr_cmap, norm=norm_color)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(x_val)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(y_val)), 1))
    ax.set_xticklabels(np.unique(x_val))
    ax.set_yticklabels(np.unique(y_val))
    plt.colorbar()
    plt.xlabel(x_key)
    plt.ylabel(y_key)
    plt.title(z_key)




def plot_rmse(dirpath, x_key, y_key, z_key, colormap, color_mode, endid, outfid, ext, exception_list=None, scale=None, clim=None):
    list_files = get_rmse_file(dirpath, endid=endid)

    min_rmse, max_rmse = [], []

    for file in list_files:
        df = pd.read_csv(os.path.join(dirpath, file))
        df = df.sort_values([x_key, y_key], ascending=[True, True])
        if exception_list:
            for ind, except_list in enumerate(exception_list):
                df = df[(df[x_key] != except_list[0]) | (df[y_key] != except_list[1])]
        z_val = df[z_key].values
        min_rmse.append(z_val.min())
        max_rmse.append(z_val.max())

    min_rmse_val = np.min(min_rmse)
    max_rmse_val = np.max(max_rmse)


    for file in list_files:

        out_fname = str(file).split('.')[0]

        df = pd.read_csv(os.path.join(dirpath, file))
        df = df.sort_values([x_key, y_key], ascending=[True, True])
        if exception_list:
            for ind, except_list in enumerate(exception_list):
                df = df[(df[x_key] != except_list[0]) | (df[y_key] != except_list[1])]
        x_val = df[x_key].values
        y_val = df[y_key].values
        z_val = df[z_key].values


        z_val_grid = construct_grid(x_val, y_val, z_val)

        if scale == None:
            plot_imshow(z_val_grid, x_val, y_val, x_key, y_key, z_key, colormap, color_mode, clim=clim)
        else:
            plot_imshow_scale(z_val_grid, x_val, y_val, x_key, y_key, z_key, colormap, vmin=min_rmse_val, vmax=max_rmse_val, scale=scale)

        plt.savefig(os.path.join(dirpath, out_fname+ '_' + str(scale) + '_' + outfid + ext))
        plt.close()

        print('heho')


def plot_imshow(data_grid, x_val, y_val, x_key, y_key, z_key, colormap, color_mode, clim = None):

    if color_mode == 'diverge':
        vmax = np.max(data_grid[~np.isnan(data_grid)])
        vmin = np.min(data_grid[~np.isnan(data_grid)])
        norm = MidpointNormalize(vmin=vmin, vmax=vmax, midpoint=0)
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey', alpha=1)
        plt.imshow(data_grid, origin='lower', cmap=curr_cmap, clim=(vmin, vmax), norm=norm)
    else:
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey')
        plt.imshow(data_grid, origin='lower', cmap=curr_cmap)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(x_val)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(y_val)), 1))
    ax.set_xticklabels(np.unique(x_val))
    ax.set_yticklabels(np.unique(y_val))
    if clim != None:
        plt.clim(clim)
    plt.colorbar()
    plt.xlabel(x_key)
    plt.ylabel(y_key)
    plt.title(z_key)


def construct_grid(wh, wv, data):
    wv_unique = np.unique(wv)
    wh_unique = np.unique(wh)
    data_grid = np.zeros(shape=(len(wv_unique), len(wh_unique)))
    ind_new = 0
    for ind, (wv_, wh_) in enumerate(zip(wv, wh)):
        for i, wv_un in enumerate(wv_unique):
            for j, wh_un in enumerate(wh_unique):
                if wv_un == wv_:
                    if wh_un == wh_:
                        data_grid[i, j] = data[ind_new]
                        ind_new += 1

    data_grid[data_grid == 0] = 'nan'
    return data_grid

if __name__=='__main__':

    exception_list = [[35, 300], [40, 300]]
    x_key = 'wh'
    y_key = 'wv'
    z_key = 'ccs_rmse'
    endid = 'rmse.csv'
    outfid = 'ccs_rmse_bone'
    colormap = 'bone'
    color_mode = 'notdiverge'

    pic_ext = '.jpeg'

    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\fig_data\fig1\ccs_rmse"

    # plot_rmse(dirpath,
    #           x_key='wh',
    #           y_key='wv',
    #           z_key='ccs_rmse',
    #           endid='rmse.csv',
    #           outfid='ccs_rmse',
    #           ext='.pdf',
    #           exception_list=exception_list,
    #           colormap='bone_r',
    #           color_mode='notdiverge',
    #           scale=None,
    #           clim=(0.6, 2.2))

    plot_rmse(dirpath,
              x_key='wh',
              y_key='wv',
              z_key='ccs_rmse',
              endid='rmse.csv',
              outfid='ccs_rmse',
              ext=pic_ext,
              exception_list=exception_list,
              colormap='bone_r',
              color_mode='notdiverge',
              scale='sqrt')


    # plot_rmse(dirpath,
    #           x_key=x_key,
    #           y_key=y_key,
    #           z_key=z_key,
    #           endid=endid,
    #           outfid=outfid,
    #           ext='.png',
    #           exception_list=exception_list,
    #           colormap=colormap,
    #           color_mode=color_mode,
    #           scale=None,clim=None)