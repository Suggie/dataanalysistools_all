import os
import numpy as np
import sys

def write_cal_files(data, fname):
    outlines = ''
    header = '# add notes\n'
    outlines += header

    try:
        for line in data:
            line_ = '{}\n'.format(line)
            outlines += line_
    except TypeError:
        line_ = '{}\n'.format(data)
        outlines += line_


    with open(fname, 'w') as calfile:
        calfile.write(outlines)
        calfile.close()


def gen_cal_leave_one_out_files(calfile):

    outlines = ''
    header = '# add notes\n'
    outlines += header

    dirpath = os.path.split(calfile)[0]
    cal_uncal_calfile_list = []
    with open(calfile, 'r') as cal_file:
        cal_all = cal_file.read().splitlines()
        for num in range(len(cal_all)):
            if not cal_all[num].startswith('#'):
                new_data = []
                unk_data = cal_all[num]
                unk_id = os.path.split(unk_data)[1].split('.')[0]
                for num_2 in range(len(cal_all)):
                    if not cal_all[num_2].startswith('#'):
                        if cal_all[num_2] != unk_data:
                            new_data.append(cal_all[num_2])
                cal_file_data = np.array(new_data)
                cal_file_fname = 'calfile_calibrants_' + unk_id +'.csv'
                cal_unk_file_data = np.array(unk_data)
                cal_unk_file_fname = 'calfile_unknown_' + unk_id + '.csv'
                cal_uncal_calfile_list.append([os.path.join(dirpath, cal_file_fname), os.path.join(dirpath, cal_unk_file_fname)])
                write_cal_files(cal_file_data, os.path.join(dirpath, cal_file_fname))
                write_cal_files(cal_unk_file_data, os.path.join(dirpath, cal_unk_file_fname))
                print('heho')

    for num in range(len(cal_uncal_calfile_list)):
        string_ = ','.join(x for x in cal_uncal_calfile_list[num])
        line = '{}\n'.format(string_)
        outlines += line

    with open(os.path.join(dirpath, 'cal_uncal_csv_file_list_.csv'), 'w') as outfile:
        outfile.write(outlines)
        outfile.close()

    # return cal_uncal_calfile_list


if __name__=='__main__':
    cal_all = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\All_ions_exclude_Avd_TTR\calfiles_all.csv"
    gen_cal_leave_one_out_files(cal_all)
