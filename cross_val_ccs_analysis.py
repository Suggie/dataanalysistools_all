import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
from scipy import stats


def pred_error_percent(pred_values, true_values):
    y = np.multiply(np.divide(np.subtract(pred_values, true_values), true_values), 100)
    return y

def get_diff_cal_out_csv_files(dirpath, fileid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('.csv')]
    files = [x for x in files if x.startswith(fileid)]
    return files

def get_ccsdatabse_data(ccsdatabase):
    df = pd.read_csv(ccsdatabase)
    return df

def plot_bar_chart(ydata, yerr_data, y_avg, y_std, xlabel, dirpath, file_id):
    num = len(ydata)
    ind = np.arange(num)
    width = 0.7
    fig, ax = plt.subplots()
    rects = ax.bar(ind, ydata, width, color='blue', yerr=yerr_data, error_kw=dict(lw=1, capsize=1, capthick=1))
    plt.fill_between(ind, [y_avg+y_std]*len(ind), [y_avg-y_std]*len(ind), color='red', alpha=0.5)
    plt.axhline(y_avg, ls='--', color='red')
    ax.set_ylabel('% CCS Error')
    # plt.ylim((-15, 24))
    plt.xticks(ind, tuple(xlabel), rotation='vertical')
    plt.subplots_adjust(bottom=0.22)
    plt.tick_params(labelsize=5.5)

    plt.savefig(os.path.join(dirpath, 'cross_val_ccs_error_' + file_id + '_.png'), dpi=500)
    plt.close()


if __name__=='__main__':

    ccs_db_file = r"C:\Users\sugyan\Documents\CCSCalibration\ccsdatabse_positive.csv"
    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\All_ions\Cross_Val_each_ion"

    powerfit_id = 'powerfit_powerfitexp_False_calibration_unk_output'
    powerfit_exp_id = 'powerfit_powerfitexp_True_calibration_unk_output'
    ccs_only_4_id = 'relaxation_False_terms_4_calibration_unk_output'
    ccs_only_6_id = 'relaxation_False_terms_6_calibration_unk_output'
    ccs_only_10_id = 'relaxation_False_terms_10_calibration_unk_output'
    relaxation_4_id = 'relaxation_True_terms_4_calibration_unk_output'
    relaxation_6_id = 'relaxation_True_terms_6_calibration_unk_output'
    relaxation_10_id = 'relaxation_True_terms_10_calibration_unk_output'
    relaxationexp_4_id = 'relaxationexp_True_terms_4_calibration_unk_output'
    relaxationexp_6_id = 'relaxationexp_True_terms_6_calibration_unk_output'
    relaxationexp_10_id = 'relaxationexp_True_terms_10_calibration_unk_output'

    file_id_list = [powerfit_id, powerfit_exp_id, ccs_only_4_id, ccs_only_6_id, relaxation_4_id, relaxation_6_id,
                    relaxationexp_4_id, relaxationexp_6_id]

    ccs_err_avg_arr_list = []

    ccs_stats_lines = ''
    ccs_stats_header = '#caltype,avg_ccs_err,std_ccs_err,min_ccs_err,max_ccs_err,min_ccserr_id,max_ccserr_id\n'
    ccs_stats_lines += ccs_stats_header


    for file_id in file_id_list:
        unk_out_files = get_diff_cal_out_csv_files(dirpath, file_id)

        ccs_database = get_ccsdatabse_data(ccs_db_file)

        cal_out_comb_data_arr = []

        for file in unk_out_files:

            df = pd.read_csv(os.path.join(dirpath, file))
            df = df[(df['wh'] != 40) | (df['wv'] != 300)]
            df = df[(df['wv'] == 300) | (df['wv'] == 400)]
            # df = df[(df['wh'] != 30) | (df['wv'] != 300)]
            # df = df[(df['wh'] != 40) | (df['wv'] != 400)]
            # df = df[(df['wh'] != 40) | (df['wv'] != 1000)]
            df = df[(df['id'] != '')]
            id_ = df['id'][0]
            oligo = df['oligo'][0]
            mass = df['mass'][0]
            charge = df['charge'][0]
            pred_ccs = df['pred_ccs']
            for index in range(len(ccs_database['id'])):
                if ccs_database['id'][index] == id_:
                    if ccs_database['n_oligomers'][index] == oligo:
                        if ccs_database['z'][index] == charge:
                            true_ccs = ccs_database['ccs_n2'][index]
            true_ccs_arr = np.repeat(true_ccs, len(pred_ccs))
            ccs_error = pred_error_percent(pred_ccs, true_ccs)
            ccs_error_avg = np.average(ccs_error)
            ccs_error_std = np.std(ccs_error)
            df['ccs'] = true_ccs_arr
            df['pred_ccs_error_percent'] = ccs_error
            err_csv_fname = 'ccs_error_' + str(file)
            cal_out_comb_data_arr.append([id_, oligo, mass, charge, true_ccs, ccs_error_avg, ccs_error_std])
            df.to_csv(os.path.join(dirpath, err_csv_fname), sep=',', index=False)


        cal_out_array = np.array(cal_out_comb_data_arr)
        mass_ = np.array(cal_out_array[:, 2], dtype='float')
        charge_ = np.array(cal_out_array[:, 3], dtype='int')
        sort_ind = np.lexsort((charge_, mass_))
        sort_arr = cal_out_array[sort_ind]
        ccs_err_avg_arr = np.array(sort_arr[:, 5], dtype='float')
        ccs_err_std_arr = np.array(sort_arr[:, 6], dtype='float')

        ccs_err_avg = np.average(ccs_err_avg_arr)
        ccs_err_avg_arr_list.append(ccs_err_avg_arr)
        ccs_err_std = np.std(ccs_err_avg_arr)

        ccs_err_min = np.min(ccs_err_avg_arr)
        ccs_err_max = np.max(ccs_err_avg_arr)

        min_id = sort_arr[:, 0][np.where(ccs_err_avg_arr == ccs_err_min)][0]
        min_oligo = sort_arr[:, 1][np.where(ccs_err_avg_arr == ccs_err_min)][0]
        min_charge = sort_arr[:, 3][np.where(ccs_err_avg_arr == ccs_err_min)][0]

        min_keyid = '_'.join([str(min_id), str(min_oligo), str(min_charge)])

        max_id = sort_arr[:, 0][np.where(ccs_err_avg_arr == ccs_err_max)][0]
        max_oligo = sort_arr[:, 1][np.where(ccs_err_avg_arr == ccs_err_max)][0]
        max_charge = sort_arr[:, 3][np.where(ccs_err_avg_arr == ccs_err_max)][0]

        max_keyid = '_'.join([str(max_id), str(max_oligo), str(max_charge)])

        x_label = [str(x) + '_' + str(y) for x, y in zip(sort_arr[:, 0], sort_arr[:, 3])]

        plot_bar_chart(ccs_err_avg_arr, ccs_err_std_arr, ccs_err_avg, ccs_err_std, x_label, dirpath, file_id+'_300WV_400WV_')

        outlines = ''
        header = 'id,oligo,mass,charge,ccs,ccs_error_avg,ccs_error_std\n'
        outlines += header
        for num in range(len(sort_arr)):
            string_ = (',').join(str(x) for x in sort_arr[num])
            line = '{}\n'.format(string_)
            outlines += line

        with open(os.path.join(dirpath, 'cross_val_ccs_error_' + file_id+'_300WV_400WV.csv'), 'w') as crossval_file:
            crossval_file.write(outlines)
            crossval_file.close()

        # ccs_stats_header = '#caltype,avg_ccs_err,std_ccs_err,min_ccs_err,max_ccs_err,min_ccserr_id,max_ccserr_id\n'
        line2 = '{},{},{},{},{},{},{}\n'.format(file_id, ccs_err_avg, ccs_err_std, ccs_err_min, ccs_err_max, min_keyid, max_keyid)
        ccs_stats_lines += line2

    with open(os.path.join(dirpath, '300WV_400WV_ccs_error_stats.csv'), 'w') as errstats:
        errstats.write(ccs_stats_lines)
        errstats.close()


    outlines2 = ''
    header2 = '#caltype1,caltype2,t-stat,p-value\n'
    outlines2 += header2

    for ind, (cal_combo, ccs_avg_err_combo) in enumerate(zip(itertools.combinations(file_id_list, 2), itertools.combinations(ccs_err_avg_arr_list, 2))):
        ttest = stats.ttest_ind(ccs_avg_err_combo[0], ccs_avg_err_combo[1], equal_var=False)
        line_2 = '{},{},{},{}\n'.format(cal_combo[0], cal_combo[1], ttest[0], ttest[1])
        outlines2 += line_2

    with open(os.path.join(dirpath, '300WV_400WV_ccs_error_stats_ttest.csv'), 'w') as ttestfile:
        ttestfile.write(outlines2)
        ttestfile.close()

    print('heho')