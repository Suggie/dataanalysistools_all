import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def rms_error(predict_values, true_values):
    """
    Calculate the percent error and computes the rms error in %
    :param predict_values: predict values
    :param true_values: true values
    :return: % rmse
    """
    diff = np.subtract(predict_values, true_values)
    percent_error = np.multiply(np.divide(diff, true_values), 100)
    percent_error_sq = np.square(percent_error)
    rmse = np.sqrt(np.divide(np.sum(percent_error_sq), len(percent_error_sq)))
    return rmse



def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files



def gen_ccs_dev_ions(fpath, wh_wv_exception_list):

    dirpath, fname = os.path.split(fpath)

    df = pd.read_csv(fpath)

    if wh_wv_exception_list:
        for ind, arr in enumerate(wh_wv_exception_list):
            df = df[(df['wh'] != arr[0]) | (df['wv'] != arr[1])]

    uniq_ids = np.unique(df['#id'].values)

    header = '#id,oligomer,mass,charge,superclass,subclass,ccs_rmse_avg,ccs_rmse_std,pred_ccs_avg,pred_ccs_std,pred_mob_avg,pred_mob_std,ccs_dev_avg,ccs_dev_std,ccs_rmse\n'
    data_string = ''

    for ind1, uniq_id in enumerate(uniq_ids):
        df_uniq_id = df[(df['#id'] == uniq_id)]
        uniq_oligo_id = np.unique(df_uniq_id['oligomer'].values)
        for ind3, uniq_oligo in enumerate(uniq_oligo_id):
            df_uniq_oligo_id = df_uniq_id[(df_uniq_id['oligomer'] == uniq_oligo)]
            uniq_charge_oligo_id = np.unique(df_uniq_oligo_id['charge'].values)
            for ind4, uniq_charge in enumerate(uniq_charge_oligo_id):
                df_uniq_charge_oligo_id = df_uniq_oligo_id[(df_uniq_oligo_id['charge'] == uniq_charge)]
                pred_ccs_values = df_uniq_charge_oligo_id['pred_ccs'].values
                true_ccs_values = df_uniq_charge_oligo_id['true_ccs'].values
                ccs_rmse = rms_error(pred_ccs_values, true_ccs_values)
                mean_pred_ccs = np.mean(pred_ccs_values)
                std_pred_ccs = np.std(pred_ccs_values)
                mean_pred_mob = np.mean(df_uniq_charge_oligo_id['pred_mob'].values)
                std_pred_mob = np.std(df_uniq_charge_oligo_id['pred_mob'].values)
                mean_ccs_dev = np.mean(df_uniq_charge_oligo_id['ccs_dev'].values)
                std_ccs_dev = np.std(df_uniq_charge_oligo_id['ccs_dev'].values)
                mean_ccs_rmse = np.mean(df_uniq_charge_oligo_id['ccs_rmse'].values)
                std_ccs_rmse = np.std(df_uniq_charge_oligo_id['ccs_rmse'].values)

                line = '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(df_uniq_charge_oligo_id['#id'].values[0],
                                                                   df_uniq_charge_oligo_id['oligomer'].values[0],
                                                                   df_uniq_charge_oligo_id['mass'].values[0],
                                                                   uniq_charge,
                                                                         df_uniq_charge_oligo_id['superclass'].values[0],
                                                                         df_uniq_charge_oligo_id['subclass'].values[0],
                                                                   mean_ccs_rmse,
                                                                   std_ccs_rmse,
                                                                   mean_pred_ccs,
                                                                   std_pred_ccs,
                                                                   mean_pred_mob,
                                                                   std_pred_mob,
                                                                   mean_ccs_dev,
                                                                   std_ccs_dev,
                                                                      ccs_rmse)
                data_string += line




    output_string = header + data_string

    outname = str(fname).split('.csv')[0]+'_species.csv'
    with open(os.path.join(dirpath, outname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


def gen_ccs_dev_ions_with_dir(dirpath, endid, wh_wv_exception_list):
    files = get_files(dirpath, endid=endid)
    for ind, file in enumerate(files):
        fpath = os.path.join(dirpath, file)
        gen_ccs_dev_ions(fpath, wh_wv_exception_list)


if __name__ == '__main__':
    exception_list = [[40, 300], [35, 300]]#, [30, 300], [40, 400]]

    dpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\CrossClass\CalDenat_UncalNative"
    files = get_files(dpath, endid='ccs_dev_output.csv')
    for ind, file in enumerate(files):
        fpath = os.path.join(dpath, file)
        gen_ccs_dev_ions(fpath, exception_list)