# get rmse (ccs and vel) from cal output
#

import os

def get_file_endid(dirpath, endid='cal_output'):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files

def get_wh_wv_rmse(dirpath, list_of_files):
    wh, wv, vel_rmse, ccs_rmse = [],[],[],[]
    for file in list_of_files:
        with open(os.path.join(dirpath, file), 'r') as cal_output_file:
            cal_out = cal_output_file.read().splitlines()
            for line in cal_out:
                if line.startswith('#Waveht'):
                    wh.append(line.split(',')[-1])
                if line.startswith('#Wavevel'):
                    wv.append(line.split(',')[-1])
                if line.startswith('#rmse'):
                    if not line.split(',')[1] == 'vel_percent':
                        vel_rmse.append(line.split(',')[1])
                        ccs_rmse.append(line.split(',')[2])
    return wh, wv, vel_rmse, ccs_rmse

def generate_rmse_out_caltype_file_list(dirpath, cal_mode_string):
    end_id = cal_mode_string + '_cal_output.csv'
    file_list = get_file_endid(dirpath, endid=end_id)
    wh, wv, vel_rmse, ccs_rmse = get_wh_wv_rmse(dirpath, file_list)
    output_string = ''
    header = 'wh,wv,vel_rmse,ccs_rmse\n'
    output_string += header
    for ind, (waveht, wavevel, vel_rmse_, ccs_rmse_) in enumerate(zip(wh, wv, vel_rmse, ccs_rmse)):
        line = '{},{},{},{}\n'.format(waveht, wavevel, vel_rmse_, ccs_rmse_)
        output_string += line

    out_fname = cal_mode_string + '_rmse.csv'

    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\Native_Proteins\All_Ion"
    cal_mode_string_list = ['blended_True',
                            'blended_True_exp_True',
                            'power_law_True',
                            'power_law_True_exp_True',
                            'relax_True_terms_6',
                            'relax_True_terms_6_exp_True']

    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        generate_rmse_out_caltype_file_list(dirpath, cal_mode_string)