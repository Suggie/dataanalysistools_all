import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files



def plot_bar_graph(y_, y_err, x_labels, y_label):
    num = len(y_)
    ind = np.arange(num)
    width = 0.5
    hatch_type = ''
    fig, ax = plt.subplots()
    rects = ax.bar(ind, y_, width, color='dodgerblue', edgecolor='black',
                   hatch=hatch_type, alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
    plt.xticks(ind, x_labels, rotation=45)
    # plt.ylim((-15, 25))
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)

def plot_bar_graph_stats(fpath, cal_type_exception_list=None, image_type='.png'):

    dirpath, fname = os.path.split(fpath)
    id_ = str(fname).split('_all_stats.csv')[0]
    outname = id_ + 'bar_graph' + image_type

    df = pd.read_csv(fpath)

    if cal_type_exception_list:

        for ind, exception in enumerate(cal_type_exception_list):

            df = df[(df['#caltype']!=exception)]

    y_ = df['mean'].values
    yerr = df['std'].values
    x_labels = df['#caltype'].values




    plot_bar_graph(y_, yerr, x_labels, y_label='%')
    plt.title(id_)
    plt.tight_layout()
    plt.savefig(os.path.join(dirpath, outname))
    plt.close()


def plot_bar_graph_from_stats_file(dirpath, endid='all_stats.csv', exception_list=None, image_type='.pdf'):

    files = get_files(dirpath, endid=endid)

    for ind, file in enumerate(files):

        fpath = os.path.join(dirpath, file)

        plot_bar_graph_stats(fpath, cal_type_exception_list=exception_list, image_type=image_type)


if __name__ == '__main__':

    dpath = r"C:\Users\sugyan\Documents\Processed data\051819_CalProcessing\All_Ion\LeaveOneIon_CrossVal"

    files = get_files(dpath, endid='all_stats.csv')

    exception_list = ['relax_True_terms_6', 'relax_True_terms_6_exp_True']
    # exception_list = None


    for ind, file in enumerate(files):

        fpath = os.path.join(dpath, file)

        plot_bar_graph_stats(fpath, cal_type_exception_list=exception_list, image_type='.pdf')

