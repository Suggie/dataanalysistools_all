
# cal object file unk file pair

import os


def get_file_list_start_end_id(dirpath, start_id, end_id):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(end_id)]
    files = [x for x in files if x.startswith(start_id)]
    return files


def write_cal_obj_unk_csv_list(dirpath):
    """
    gets a list of unk csv file, loop through each, and append each cal scheme obj file to that unk file
    :param dirpath: directory where unk and cal files exist
    :return: csv output of unk file and cal obj file pairs
    """
    output_string = ''
    header = 'unk_csv_fpath,cal_obj_fpath\n'
    output_string += header
    unk_csv_file_list = get_file_list_start_end_id(dirpath, 'unk_', '.csv')
    for unk_csv_file in unk_csv_file_list:
        print(unk_csv_file, '.....')
        unk_csv_fpath = os.path.join(dirpath, unk_csv_file)
        unk_start_id = str(unk_csv_file).split('.csv')[0]
        cal_start_id = unk_start_id.replace('unk', 'input')
        cal_obj_file_list = get_file_list_start_end_id(dirpath, cal_start_id, '.cal')
        for cal_obj_file in cal_obj_file_list:
            print(cal_obj_file)
            cal_obj_fpath = os.path.join(dirpath, cal_obj_file)
            line = '{},{}\n'.format(unk_csv_fpath, cal_obj_fpath)
            output_string += line


    out_fname = 'cal_predict_files.csv'
    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


def write_cal_obj_unk_csv_list_wh_wv(dirpath):
    """
    get a list of unk csv file and append each cal scheme obj file matching wave height and wave velocity id.
    For mix class type calibration
    :param dirpath: directory where unk and cal files exist
    :return: void. csv ourput of unk file and cal obj file pairs
    """
    output_string = ''
    header = 'unk_csv_fpath,cal_obj_fpath\n'
    output_string += header
    unk_csv_file_list = get_file_list_start_end_id(dirpath, 'unk_', '.csv')
    for unk_csv_file in unk_csv_file_list:
        print(unk_csv_file, '.....')
        unk_csv_fpath = os.path.join(dirpath, unk_csv_file)
        wh_wv_string = str(unk_csv_file).split('unk_input_')[1]
        wh_wv_string = wh_wv_string.split('.csv')[0]
        cal_obj_file_list = get_file_list_start_end_id(dirpath, 'cal_input', '.cal')
        for cal_obj_file in cal_obj_file_list:
            cal_obj_fpath = os.path.join(dirpath, cal_obj_file)
            if str(cal_obj_file).find(wh_wv_string) > 0:
                line = '{},{}\n'.format(unk_csv_fpath, cal_obj_fpath)
                output_string += line

    out_fname = 'cal_predict_files.csv'
    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()




if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\powerlaw\smallmol\leaveonespec"
    write_cal_obj_unk_csv_list(dirpath)
    # write_cal_obj_unk_csv_list_wh_wv(dirpath)