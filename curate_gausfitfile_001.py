import os
import numpy as np

def getfiles(path, file_end_string):
    files = os.listdir(path)
    files = [x for x in files if x.endswith(file_end_string+'.csv')]
    return files

def getdata(absfilepath):
    data = np.genfromtxt(absfilepath, delimiter=',', skip_header=1, dtype='str')
    return data

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\UBQDENAT\TWIMExtract_Output"
    files = getfiles(dirpath, 'gaussfit')
    for file in files:
        fname = str(file)
        fname_strip = fname.split('_')[:-1]
        data = getdata(os.path.join(dirpath, fname))
        newdata = np.delete(data, [20, 25, 30, 31, 35, 36, 37], axis=0)
        newfname = ('_').join(x for x in fname_strip)+'_curated.csv'
        header1 = 'WH, WV, y0, Amp, xc, w, fwhm, res, lnreg_slope, lnreg_intercept, lnreg_r2, lnreg_adjrsq'
        header2 = 'id, wv, wh, oligomer, mass, charge, pressure, dt'
        np.savetxt(os.path.join(dirpath, newfname), newdata, fmt='%s', delimiter=',', header=header1)
        print(newfname+' Done!')