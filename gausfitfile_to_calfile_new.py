# author: Suggie
# date: 02/15/19

import os
import pandas as pd


def get_gauss_file_dfs(gauss_file_list_df):
    cal_in_dict_list = []
    gauss_df_list = []
    for index, gauss_fpath in enumerate(gauss_file_list_df['gauss_fpath']):
        cal_in_dict = dict()
        cal_in_dict['species_name'] = gauss_file_list_df['species_name'].values[index]
        cal_in_dict['oligomer'] = gauss_file_list_df['oligomer'].values[index]
        cal_in_dict['charge'] = gauss_file_list_df['charge'].values[index]
        cal_in_dict['mass'] = gauss_file_list_df['mass'].values[index]
        cal_in_dict['pressure'] = gauss_file_list_df['pressure'].values[index]
        cal_in_dict['edc'] = gauss_file_list_df['edc'].values[index]
        cal_in_dict['superclass'] = gauss_file_list_df['superclass'].values[index]
        cal_in_dict['subclass'] = gauss_file_list_df['subclass'].values[index]
        df = pd.read_csv(gauss_fpath)
        cal_in_dict_list.append(cal_in_dict)
        gauss_df_list.append(df)

    return cal_in_dict_list, gauss_df_list



def prepare_cal_input(gauss_file_list_df, pot_factor, wave_lambda, temp, twim_length, dirpath):
    cal_in_dict_list, gauss_df_list = get_gauss_file_dfs(gauss_file_list_df)

    ## get info on how many tw conditions from the first gauss df
    ## make sure all the other gauss df have same wh wv conditions
    wave_ht_conditions = gauss_df_list[0].iloc[:, 0].values
    wave_vel_conditions = gauss_df_list[0].iloc[:, 1].values


    for index, (wave_ht, wave_vel) in enumerate(zip(wave_ht_conditions, wave_vel_conditions)):
        output_string = ''

        header1 = '#Waveht,{}\n#Wavevel,{}\n#Pressure,{}\n#Temperature,{}\n#TWIM_length,{}\n#EDC,{}\n#Pot_factor,{}\n#Wave_lambda,{}\n'.format(str(wave_ht), str(wave_vel), str(cal_in_dict_list[0]['pressure']), temp, twim_length,str(cal_in_dict_list[0]['edc']), str(pot_factor), str(wave_lambda))
        header2 = '#id,oligomer,mass,charge,superclass,subclass,dt\n'
        output_string += header1
        output_string += header2
        for ind, (cal_dict, gauss_df) in enumerate(zip(cal_in_dict_list, gauss_df_list)):
            df_ = gauss_df[gauss_df.iloc[:, 0] == wave_ht]
            df_ = df_[df_.iloc[:, 1] == wave_vel]
            drift_time = df_.iloc[:, 4].values[0]
            line = '{},{},{},{},{},{},{}\n'.format(cal_dict['species_name'], cal_dict['oligomer'], cal_dict['mass'],
                                             cal_dict['charge'], cal_dict['superclass'], cal_dict['subclass'], drift_time)
            output_string += line

        wave_ht = float(wave_ht)
        wave_vel = float(wave_vel)
        cal_fname = 'cal_input_wv_'+str(wave_vel)+'_wh_'+str(wave_ht)+'.csv'

        with open(os.path.join(dirpath, cal_fname), 'w') as calfile:
            calfile.write(output_string)
            calfile.close()


def prepare_unk_input(gauss_file_list_df, dirpath):
    cal_in_dict_list, gauss_df_list = get_gauss_file_dfs(gauss_file_list_df)

    ## get info on how many tw conditions from the first gauss df
    ## make sure all the other gauss df have same wh wv conditions
    wave_ht_conditions = gauss_df_list[0].iloc[:, 0].values
    wave_vel_conditions = gauss_df_list[0].iloc[:, 1].values

    for index, (wave_ht, wave_vel) in enumerate(zip(wave_ht_conditions, wave_vel_conditions)):
        output_string = ''

        # header1 = '#Waveht,{}\n#Wavevel,{}\n#Pressure,{}\n#Temperature,{}\n#TWIM_length,{}\n#EDC,{}\n#Pot_factor,{}\n#Wave_lambda,{}\n'.format(
        #     str(wave_ht), str(wave_vel), str(cal_in_dict_list[0]['pressure']), temp, twim_length,
        #     str(cal_in_dict_list[0]['edc']), str(pot_factor), str(wave_lambda))
        header2 = '#id,oligomer,mass,charge,superclass,subclass,dt\n'
        # output_string += header1
        output_string += header2
        for ind, (cal_dict, gauss_df) in enumerate(zip(cal_in_dict_list, gauss_df_list)):
            df_ = gauss_df[gauss_df.iloc[:, 0] == wave_ht]
            df_ = df_[df_.iloc[:, 1] == wave_vel]
            drift_time = df_.iloc[:, 4].values[0]
            line = '{},{},{},{},{},{},{}\n'.format(cal_dict['species_name'], cal_dict['oligomer'], cal_dict['mass'],
                                             cal_dict['charge'], cal_dict['superclass'], cal_dict['subclass'], drift_time)
            output_string += line

        wave_ht = float(wave_ht)
        wave_vel = float(wave_vel)
        cal_fname = 'unk_input_wv_' + str(wave_vel) + '_wh_' + str(wave_ht) + '.csv'

        with open(os.path.join(dirpath, cal_fname), 'w') as calfile:
            calfile.write(output_string)
            calfile.close()



if __name__=='__main__':
    dirloc = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\powerlaw\smallmol\all"
    gauss_file_list_csv = "gauss_file_list_for_cal.csv"
    # gaus_file_list_csv_unk = "gauss_file_list_for_unk.csv"
    # gauss_file_list_df_unk = pd.read_csv(os.path.join(dirloc, gaus_file_list_csv_unk))
    gauss_file_list_df = pd.read_csv(os.path.join(dirloc, gauss_file_list_csv))
    temp = 298.0
    twim_length = 0.254
    pot_factor = 0.41675
    wave_lambda = 0.012
    prepare_cal_input(gauss_file_list_df, pot_factor, wave_lambda, temp, twim_length, dirloc)
    # prepare_unk_input(gauss_file_list_df_unk, dirloc)
    print('heho')