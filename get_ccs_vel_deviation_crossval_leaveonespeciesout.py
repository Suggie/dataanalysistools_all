# get ccs and vel deviation from each condition and get average and std deviation with one ion out cross val

import os
import pandas as pd
import numpy as np

def get_file_endid(dirpath, endid='cal_output'):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    # files = [x for x in files if x.startswith()]
    return files

def file_with_mol_id(filelist):
    mol_id = []
    for file in filelist:
        id_ = str(file).split('_wv_')[0]
        mol_id.append(id_)
    mol_id_unique = np.unique(mol_id)
    return mol_id_unique

def get_ccs_dev_output_unk_pred(dirpath, cal_mode_string, ccs_databse_fpath, wh_wv_exception_list):
    ccs_database_df = pd.read_csv(ccs_databse_fpath)
    end_id = cal_mode_string + '_pred.csv'
    file_list = get_file_endid(dirpath, endid=end_id)
    unique_mol_id = file_with_mol_id(file_list)
    pub_ccs = []
    pred_ccs_avg = []
    pred_ccs_std = []
    ccs_dev_avg = []
    ccs_dev_std = []
    id_ = []
    oligomer = []
    mass = []
    charge = []
    ccs_rmse_avg = []
    ccs_rmse_std = []
    superclass_ = []
    subclass_ = []
    for uniq_mol_id in unique_mol_id:
        pred_ccs_arr = []
        ccs_dev_arr = []
        ccs_rmse_arr = []
        for file in file_list:
            if str(file).startswith(uniq_mol_id):
                with open(os.path.join(dirpath, file), 'r') as calout_file:
                    calib_f = calout_file.read().splitlines()
                    for line in calib_f:
                        if line.startswith('#Waveht'):
                            wh = line.split(',')[-1]
                        if line.startswith('#Wavevel'):
                            wv = line.split(',')[-1]
                        if line.startswith(('#CCS_RMSE')):
                            ccs_rmse = line.split(',')[-1]
                            ccs_rmse_arr.append(float(ccs_rmse))

                    wh_wv_key = '_'.join([wh, wv])

                    wh_wv_ex_key_list = []
                    for ind, wh_wv_ex_list in enumerate(wh_wv_exception_list):
                        wh_ex = float(wh_wv_ex_list[0])
                        wv_ex = float(wh_wv_ex_list[1])
                        wh_wv_ex_key = '_'.join([str(wh_ex), str(wv_ex)])
                        wh_wv_ex_key_list.append(wh_wv_ex_key)

                    if wh_wv_key not in wh_wv_ex_key_list:


                    # if wh_wv_key != '35.0_300.0' and wh_wv_key != '40.0_300.0':# and wh_wv_key != '35.0_1000.0':# and wh_wv_key != '40.0_400.0':
                        df = pd.read_csv(os.path.join(dirpath, file), header=12)
                        true_ccs_arr = []

                        for ind, (idd, oligod, massd, charged, predccs) in enumerate(zip(df['#id'], df['oligomer'], df['mass'], df['charge'], df['pred_ccs'])):
                            true_ccs = ccs_database_df[(ccs_database_df['id'] == idd) &
                                                   (ccs_database_df['n_oligomers'] == oligod) &
                                                   (ccs_database_df['z'] == charged)]['ccs_n2'].values[0]
                            true_ccs_arr.append(true_ccs)

                        pred_ccs = df['pred_ccs'].values

                        ccs_dev = (pred_ccs - true_ccs_arr)*100/true_ccs_arr

                        pred_ccs_arr.append(pred_ccs)
                        ccs_dev_arr.append(ccs_dev)

        id_.append(df['#id'].values)
        oligomer.append(df['oligomer'].values)
        charge.append(df['charge'].values)
        superclass_.append(df['superclass'].values)
        subclass_.append(df['subclass'].values)
        mass.append(df['mass'].values)
        pub_ccs.append(true_ccs_arr)
        pred_ccs_avg.append(np.average(pred_ccs_arr, axis=0))
        pred_ccs_std.append(np.std(pred_ccs_arr, axis=0))
        ccs_dev_avg.append(np.average(ccs_dev_arr, axis=0))
        ccs_dev_std.append(np.std(ccs_dev_arr, axis=0))



        ccs_rmse_avg.append(np.repeat(np.average(ccs_rmse_arr), len(ccs_dev_arr[0])))
        ccs_rmse_std.append(np.repeat(np.std(ccs_rmse_arr), len(ccs_dev_arr[0])))



    id_ = np.concatenate(id_)
    oligomer = np.concatenate(oligomer)
    mass = np.concatenate(mass)
    charge = np.concatenate(charge)
    superclass_ = np.concatenate(superclass_)
    subclass_ = np.concatenate(subclass_)
    pub_ccs = np.concatenate(pub_ccs)
    pred_ccs_avg = np.concatenate(pred_ccs_avg)
    pred_ccs_std = np.concatenate(pred_ccs_std)
    ccs_dev_avg = np.concatenate(ccs_dev_avg)
    ccs_dev_std = np.concatenate(ccs_dev_std)

    ccs_rmse_avg = np.concatenate(ccs_rmse_avg)
    ccs_rmse_std = np.concatenate(ccs_rmse_std)




    print('heho')

    output_string = ''
    header = '#id,oligomer,mass,charge,superclass,subclass,pub_ccs,pred_ccs_avg,pred_ccs_std,ccs_dev_avg,ccs_dev_std,ccs_rmse_avg,ccs_rmse_std\n'
    output_string += header
    for num in range(len(id_)):
        line = '{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(id_[num],
                                                     oligomer[num],
                                                     mass[num],
                                                     charge[num],
                                                           superclass_[num],
                                                           subclass_[num],
                                                     pub_ccs[num],
                                                     pred_ccs_avg[num],
                                                     pred_ccs_std[num],
                                                     ccs_dev_avg[num],
                                                     ccs_dev_std[num],
                                                           ccs_rmse_avg[num],
                                                           ccs_rmse_std[num])
        output_string += line

    out_fname = cal_mode_string + '_ccs_dev_output.csv'

    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()

    print('heho')


def get_ccs_dev_output_unk_pred_wh_wv(dirpath, cal_mode_string, ccs_databse_fpath):
    ccs_database_df = pd.read_csv(ccs_databse_fpath)
    end_id = cal_mode_string + '_pred.csv'
    file_list = get_file_endid(dirpath, endid=end_id)
    unique_mol_id = file_with_mol_id(file_list)

    for ind, uniq_mol_id in enumerate(unique_mol_id):
        pub_ccs_arr = []
        pred_ccs_arr = []
        pred_mob_arr = []
        ccs_dev_arr = []
        id_arr = []
        oligomer_arr = []
        mass_arr = []
        charge_arr = []
        wh_arr = []
        wv_arr = []
        ccs_rmse_arr = []
        for file in file_list:
            if str(file).startswith(uniq_mol_id):
                with open(os.path.join(dirpath, file), 'r') as calout_file:
                    calib_f = calout_file.read().splitlines()
                    for line in calib_f:
                        if line.startswith('#Waveht'):
                            wh = line.split(',')[-1]
                            wh_arr.append(float(wh))
                        if line.startswith('#Wavevel'):
                            wv = line.split(',')[-1]
                            wv_arr.append(float(wv))
                        if line.startswith('#CCS_RMSE'):
                            ccs_rmse = line.split(',')[-1]
                            ccs_rmse_arr.append(float(ccs_rmse))


                    df = pd.read_csv(os.path.join(dirpath, file), header=12)
                    true_ccs_arr = []


                    for ind, (idd, oligod, massd, charged, predccs) in enumerate(zip(df['#id'], df['oligomer'], df['mass'], df['charge'], df['pred_ccs'])):
                        true_ccs = ccs_database_df[(ccs_database_df['id'] == idd) &
                                               (ccs_database_df['n_oligomers'] == oligod) &
                                               (ccs_database_df['z'] == charged)]['ccs_n2'].values[0]
                        true_ccs_arr.append(true_ccs)

                    pred_ccs = df['pred_ccs'].values
                    pred_mob = df['pred_mob'].values
                    id_ = df['#id'].values
                    oligo = df['oligomer'].values
                    mass = df['mass'].values
                    charge = df['charge'].values


                    ccs_dev = (pred_ccs - true_ccs_arr)*100/true_ccs_arr

                    pub_ccs_arr.append(true_ccs_arr)
                    pred_ccs_arr.append(pred_ccs)
                    ccs_dev_arr.append(ccs_dev)
                    pred_mob_arr.append(pred_mob)
                    id_arr.append(id_)
                    oligomer_arr.append(oligo)
                    mass_arr.append(mass)
                    charge_arr.append(charge)


        pub_ccs_arr = np.array(pub_ccs_arr).T
        pred_ccs_arr = np.array(pred_ccs_arr).T
        pred_mob_arr = np.array(pred_mob_arr).T
        ccs_dev_arr = np.array(ccs_dev_arr).T
        id_arr = np.array(id_arr).T
        oligomer_arr = np.array(oligomer_arr).T
        mass_arr = np.array(mass_arr).T
        charge_arr = np.array(charge_arr).T

        for index, id_ in enumerate(id_arr):

            output_string = ''
            header = '#id,oligomer,mass,charge,wh,wv,ccs_rmse,pred_ccs,true_ccs,pred_mob,ccs_dev\n'
            output_string += header

            for num in range(len(wh_arr)):

                line = '{},{},{},{},{},{},{},{},{},{},{}\n'.format(id_[num],
                                                                   oligomer_arr[index][num],
                                                                   mass_arr[index][num],
                                                                   charge_arr[index][num],
                                                                   wh_arr[num],
                                                                   wv_arr[num],
                                                                   ccs_rmse_arr[num],
                                                                   pred_ccs_arr[index][num],
                                                                   pub_ccs_arr[index][num],
                                                                   pred_mob_arr[index][num],
                                                                   ccs_dev_arr[index][num])
                output_string += line

            spec_id = str(id_arr[index][0]) + '_' + str(oligomer_arr[index][0]) + '_' + str(charge_arr[index][0])
            out_fname = cal_mode_string + '_' + spec_id + '_species_ccs_dev_output.csv'


            with open(os.path.join(dirpath, out_fname), 'w') as outfile:
                outfile.write(output_string)
                outfile.close()

            print('heho')




        # print('heho')







if __name__=='__main__':
    ccs_db_file = r"C:\Users\sugyan\Documents\CCSCalibration\ccsdatabse_positive.csv"
    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\Native_Proteins\LeaveOneSpecies_CrossVal"
    cal_mode_string_list = ['blended_True',
                            'blended_True_exp_True',
                            'power_law_True',
                            'power_law_True_exp_True',
                            'relax_True_terms_6',
                            'relax_True_terms_6_exp_True']

    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        get_ccs_dev_output_unk_pred_wh_wv(dirpath, cal_mode_string, ccs_db_file)
        get_ccs_dev_output_unk_pred(dirpath, cal_mode_string, ccs_db_file)

