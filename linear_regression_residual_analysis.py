import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress


def lin_reg_fit(x_, y_):
    linreg = linregress(x_, y_)
    y_fit = linreg[0]*(x_) + linreg[1]
    return linreg, y_fit

if __name__=='__main__':

    file_ = r"C:\Users\sugyan\Documents\Processed data\071819_Nanodisc_peptide_datanalysis\CCS_rIAPP_Lipids\rIAPP_exp_ccs_stats.csv"
    df = pd.read_csv(file_)
    x_ = df['oligomer'].values
    y_ = df['mean'].values
    y_err = df['total_err'].values

    linreg, y_fit = lin_reg_fit(x_, y_)
    residual_all = y_ - y_fit

    #fitting data with two linreg
    set_term = 7
    linreg_1, y_fit_1 = lin_reg_fit(x_[:set_term], y_[:set_term])
    residual_1 = y_[:set_term] - y_fit_1

    linreg_2, y_fit_2 = lin_reg_fit(x_[set_term:], y_[set_term:])
    residual_2 = y_[set_term:] - y_fit_2

    print('heho')


    figs, axs = plt.subplots(nrows=2, ncols=2, sharex=True)
    ax = axs[0,0]
    ax.errorbar(x_, y_, y_err, fmt='o', color='black')
    ax.plot(x_, y_fit, ls='--', color='red')

    ax2 = axs[1,0]
    ax2.scatter(x_, residual_all, color='black')

    ax3 = axs[0,1]
    ax3.errorbar(x_, y_, y_err, fmt='o', color='black')
    ax3.plot(x_[:set_term], y_fit_1, ls='--', color='magenta')
    ax3.plot(x_[set_term:], y_fit_2, ls='--', color='green')

    ax4 = axs[1,1]
    ax4.scatter(x_[:set_term], residual_1, color='black')
    ax4.scatter(x_[set_term:], residual_2, color='black')

    plt.show()
    plt.close()
