import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from midpoint_normalize_class import MidpointNormalize


def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files


def construct_grid(wh, wv, data):
    wv_unique = np.unique(wv)
    wh_unique = np.unique(wh)
    data_grid = np.zeros(shape=(len(wv_unique), len(wh_unique)))
    ind_new = 0
    for ind, (wv_, wh_) in enumerate(zip(wv, wh)):
        for i, wv_un in enumerate(wv_unique):
            for j, wh_un in enumerate(wh_unique):
                if wv_un == wv_:
                    if wh_un == wh_:
                        data_grid[i, j] = data[ind_new]
                        ind_new += 1

    data_grid[data_grid == 0] = 'nan'
    return data_grid


def plot_imshow(data_grid, x_val, y_val, x_key, y_key, z_key, colormap, color_mode, midpoint=1, clim = None):

    if color_mode == 'diverge':
        vmax = np.max(data_grid[~np.isnan(data_grid)])
        vmin = np.min(data_grid[~np.isnan(data_grid)])

        norm = MidpointNormalize(vmin=vmin, vmax=vmax, midpoint=midpoint)
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey', alpha=1)
        plt.imshow(data_grid, origin='lower', cmap=curr_cmap, norm=norm)
        curr_cmap.set_bad(color='grey', alpha=1)
    else:
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey')
        plt.imshow(data_grid, origin='lower', cmap=curr_cmap)
    ax = plt.gca()
    ax.set_xticks(np.arange(0, len(np.unique(x_val)), 1))
    ax.set_yticks(np.arange(0, len(np.unique(y_val)), 1))
    ax.set_xticklabels(np.unique(x_val))
    ax.set_yticklabels(np.unique(y_val))
    if clim != None:
        plt.clim(clim)
    plt.colorbar()
    plt.xlabel(x_key)
    plt.ylabel(y_key)
    plt.title(z_key)


def plot_fit_params_(filepath, wh_wv_exception_list=None):

    dirpath, fname = os.path.split(filepath)

    df = pd.read_csv(filepath)

    if wh_wv_exception_list:

        for ind, arr in enumerate(wh_wv_exception_list):

            df = df[(df['wh'] != arr[0]) | (df['wv'] != arr[1])]


    wh_data = df['wh'].values
    wv_data = df['wv'].values

    param_a = df['a'].values
    param_b = df['g'].values

    if fname.find('exp') >= 0:
        param_c = df['exp'].values
        params_ = [param_a, param_b, param_c]
    else:
        params_ = [param_a, param_b]

    param_label = ['a', 'g', 'cexp']
    diverge_key = [True, True, False]
    colormap = ['seismic', 'seismic', 'bone']



    outname = str(fname).split('.csv')[0] + '.pdf'

    with PdfPages(os.path.join(dirpath, outname)) as pdf:

        for ind, params in enumerate(params_):

            plt.scatter(wh_data, params, marker='o', color='black')
            plt.xlabel('WH')
            plt.ylabel(param_label[ind])
            pdf.savefig()
            plt.close()

            plt.scatter(wv_data, params, marker='o', color='black')
            plt.xlabel('WV')
            plt.ylabel(param_label[ind])
            pdf.savefig()
            plt.close()

            plt.scatter(wh_data/wv_data, params, marker='o', color='black')
            plt.xlabel('WH/WV')
            plt.ylabel(param_label[ind])
            pdf.savefig()
            plt.close()

            data_grid = construct_grid(wh_data, wv_data, params)
            diverge_ = diverge_key[ind]
            if diverge_:
                diverge_k = 'diverge'
            else:
                diverge_k = 'notdiverge'

            plot_imshow(data_grid, wh_data, wv_data, 'wh', 'wv', 'params_' + str(ind), colormap=colormap[ind],
                        color_mode=diverge_k)
            pdf.savefig()
            plt.close()

if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\PolyAla\LeaveOneSpecies_CrossVal"
    fit_param_files = get_files(dirpath, endid='fit_params.csv')
    exception_list = [[40, 300], [35, 300]]#, [30, 300], [40, 400]]
    for ind, file in enumerate(fit_param_files):
        plot_fit_params_(os.path.join(dirpath, file), wh_wv_exception_list=exception_list)
