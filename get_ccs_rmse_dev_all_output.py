## get ccs rmse dev output from species ccs dev output files

import os
import numpy as np
import pandas as pd


def get_files(dirpath, endid='species_ccs_dev_output.csv'):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files


def get_ccs_rmse_dev_output_all(dirpath, endid, cal_mode):
    """
    loop through each type of cal func and store data for box whisker plot
    :param dirpath: directory to look for files
    :param endid: file ends with key
    :param data_key: column name for pandas
    :return: data and data labels list
    """

    file_list = get_files(dirpath, endid=endid)

    df_list = []

    for ind, file in enumerate(file_list):
        if str(file).startswith(cal_mode):
            if cal_mode.find('exp') >= 0:
                df = pd.read_csv(os.path.join(dirpath, file))
                df_list.append(df)
            else:
                if str(file).split(cal_mode)[1].find('exp') == -1:
                    df = pd.read_csv(os.path.join(dirpath, file))
                    df_list.append(df)

    df_combo = pd.concat(df_list)

    df_combo = df_combo.drop_duplicates()

    out_fname = 'ccs_dev_rmse_all_out_'+cal_mode+'.csv'
    df_combo.to_csv(os.path.join(dirpath, out_fname), index=False)

    print('heho')


if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\Native_Proteins\LeaveOneSpecies_CrossVal"
    endid = 'species_ccs_dev_output.csv'

    cal_mode_string_list = ['blended_True',
                            'blended_True_exp_True',
                            'power_law_True',
                            'power_law_True_exp_True',
                            'relax_True_terms_6',
                            'relax_True_terms_6_exp_True']

    for cal_mode in cal_mode_string_list:
        get_ccs_rmse_dev_output_all(dirpath, endid, cal_mode)