import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def rms_error(predict_values, true_values):
    """
    Calculate the percent error and computes the rms error in %
    :param predict_values: predict values
    :param true_values: true values
    :return: % rmse
    """
    diff = np.subtract(predict_values, true_values)
    percent_error = np.multiply(np.divide(diff, true_values), 100)
    percent_error_sq = np.square(percent_error)
    rmse = np.sqrt(np.divide(np.sum(percent_error_sq), len(percent_error_sq)))
    return rmse


def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files


def gen_wh_wv_key_(wh_val, wv_val):
    key_ = str(wh_val)+'_'+str(wv_val)
    return key_


def gen_ccs_rmse_at_wh_wv_include_all_ions(fpath):

    dirpath, fname = os.path.split(fpath)

    df = pd.read_csv(fpath)

    wh_wv_key_list = []
    for ind, (wh, wv) in enumerate(zip(df['wh'].values, df['wv'].values)):
        wh_wv_key = gen_wh_wv_key_(wh, wv)
        wh_wv_key_list.append(wh_wv_key)

    unique_wh_wv_key_list = np.unique(wh_wv_key_list)

    uniq_wh_wv_pred_ccs_arr = [[] for _ in range(len(unique_wh_wv_key_list))]
    uniq_wh_wv_true_ccs_arr = [[] for _ in range(len(unique_wh_wv_key_list))]

    for index, uniq_wh_wv in enumerate(unique_wh_wv_key_list):
        for ind, (wh, wv) in enumerate(zip(df['wh'].values, df['wv'].values)):
            wh_wv_key = gen_wh_wv_key_(wh, wv)
            if wh_wv_key == uniq_wh_wv:
                pred_ccs_val = df['pred_ccs'].values[ind]
                true_ccs_val = df['true_ccs'].values[ind]
                uniq_wh_wv_pred_ccs_arr[index].append(pred_ccs_val)
                uniq_wh_wv_true_ccs_arr[index].append(true_ccs_val)



    header = 'wh,wv,ccs_rmse\n'

    data_string = ''

    for index, (uniq_wh_wv, pred_ccs_arr, true_ccs_arr) in enumerate(zip(unique_wh_wv_key_list, uniq_wh_wv_pred_ccs_arr,
                                                                         uniq_wh_wv_true_ccs_arr)):
        rmse_ = rms_error(pred_ccs_arr, true_ccs_arr)

        wh, wv = uniq_wh_wv.split('_')

        line = '{},{},{}\n'.format(wh, wv, rmse_)
        data_string += line

    output_string = header + data_string

    outname = str(fname).split('.csv')[0]+'_wh_wv_rmse.csv'

    with open(os.path.join(dirpath, outname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()



def gen_ccs_rmse_allions_allconditions(dirpath, endid='ccs_dev_output.csv'):
    """
    get ccs rmse from pred and true values of all ions at all conditions
    :param fpath: file path containing the information
    :return: fname and ccs rmse val
    """

    files = get_files(dirpath, endid=endid)

    header = 'fname,ccs_rmse\n'
    data_string = ''

    for ind, file in enumerate(files):

        fname_char = str(file).split('_')[:-3]
        cal_type = ('_').join([x for x in fname_char])

        fpath = os.path.join(dirpath, file)

        df = pd.read_csv(fpath)

        rmse_ = rms_error(df['pred_ccs'].values, df['true_ccs'].values)

        line = '{},{}\n'.format(cal_type, rmse_)

        data_string += line

    output_string = header + data_string

    outname = 'ccs_rmse_allions_allcond.csv'

    with open(os.path.join(dirpath, outname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()



def gen_ccs_rmse_wh_wv_all_ion_dir(dirpath, endid):
    files = get_files(dirpath, endid=endid)
    for ind, file in enumerate(files):
        fpath = os.path.join(dirpath, file)
        gen_ccs_rmse_at_wh_wv_include_all_ions(fpath)

if __name__ == '__main__':

    dpath = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_fixa\_CrossCls\CalPeptide_NatProt"
    endid_ = 'ccs_dev_output.csv'
    files = get_files(dpath, endid=endid_)
    # for ind, file in enumerate(files):
    #     fpath = os.path.join(dpath, file)
    #     gen_ccs_rmse_at_wh_wv_include_all_ions(fpath)

    gen_ccs_rmse_allions_allconditions(dpath, endid=endid_)