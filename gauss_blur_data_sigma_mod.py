import os
from math import pi, sqrt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sklearn.linear_model import LinearRegression, Ridge, Lasso


def power_law_fun(x, a, b):
    y = a*x**b
    return y


def get_calibrants_output_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('calibrants_output.csv')]
    files = [x for x in files if x.find('power_law_True_exp_') >= 0]
    return files

def conv_exp_vel_to_exp_time(vel, twim_length):
    time = twim_length/vel
    return time

def yield_sigma_mod(file, dirpath, twim_length=0.254, res_expect=10):
    df = pd.read_csv(os.path.join(dirpath, file), header=11)
    time_ = conv_exp_vel_to_exp_time(df['exp_avg_vel'].values, twim_length)
    mob_ = df['mobility'].values
    popt, pcov = curve_fit(power_law_fun, mob_, time_)
    yfit = power_law_fun(mob_, *popt)
    sigma_mod = get_sigma_for_gauss_blur(yfit*1000, res_expect=res_expect)
    return df, sigma_mod


def get_sigma_for_gauss_blur(ypred,res_expect=10):
    fwhm_expect = ypred/res_expect
    sigma_expect = (1/2.35482) * fwhm_expect
    return sigma_expect


def get_gauss_filt_stdev(sigma_real, sigma_mod):
    """
    using the sigma expected for expected resolution, what is the stdev needed in gauss filter
    :param sigma_real: sigma observed in experiment
    :param sigma_mod: sigma expect
    :return: gauss filter stdev
    """
    gaus_filt_stdev = sigma_real/(sigma_mod * 2 * sqrt(pi))
    return gaus_filt_stdev


def assemble_sigma_species_wh_wv(list_of_cal_out_files, dirpath, twim_length, res_expect):
    """
    assemble sigma to model for gauss blur for all species at each wh and wv conditions
    :param list_of_cal_out_files: list of cal out files
    :param dirpath: directory location
    :param twim_length: length of twim cell 0.254
    :param res_expect: expected resolution
    :return: file output
    """

    output_string = ''
    header = '#id,oligomer,mass,charge,wh,wv,sigma_mod\n'
    output_string += header

    for index, file in enumerate(list_of_cal_out_files):
        file_chars = str(file).split('_')
        wh = float(file_chars[5])
        wv = float(file_chars[3])
        df, sigma_mod = yield_sigma_mod(file, dirpath, twim_length=twim_length, res_expect=res_expect)
        for num in range(len(df['#id'].values)):
            line = '{},{},{},{},{},{},{}\n'.format(df['#id'].values[num],
                                                   df['oligomer'].values[num],
                                                   df['mass'].values[num],
                                                   df['charge'].values[num],
                                                   wh,
                                                   wv,
                                                   sigma_mod[num])
            output_string += line

    out_fname = 'gauss_blus_sigma_mod.csv'
    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()



if __name__ == '__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\_All\All_Ion"
    calibrants_out_files = get_calibrants_output_files(dirpath)
    assemble_sigma_species_wh_wv(calibrants_out_files, dirpath, twim_length=0.254, res_expect=10)
    # for file in calibrants_out_files:
    #     yield_sigma_mod(file, dirpath, res_expect=10)