import os
import numpy as np
import matplotlib.pyplot as plt


def getfiles(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('combined_WH_WV_average.csv')]
    return files

def plot_bar_graph_with_error(y, yerr):
    num = len(y)
    ind = np.arange(num)
    # width = 0.2

    fig, ax = plt.subplots()

    rects = ax.bar(ind, y, yerr=yerr, lw=0.1, capsize=2)
    ax.set_ylabel('Avg Percent Error')

def plot_scatter_mz_vs_error(x, y, yerr):
    plt.errorbar(x, y, yerr=yerr, capsize=2, marker='o', ls='none')
    plt.xlabel('m/z')
    plt.ylabel('CCS error(%)')



if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\cal_all_4terms"
    files = getfiles(dirpath)
    for file in files:
        data = np.genfromtxt(os.path.join(dirpath, file), delimiter=',', skip_header=1)
        avg_percent_error = data[:, 8]
        std_percent_error = data[:, 9]
        identity = data[:, 1]

        # plot_bar_graph_with_error(avg_percent_error, std_percent_error)
        # plt.ylim((-11, 11))
        # plt.savefig(os.path.join(dirpath, str(file).split('.')[0]+'avg_percent_error.pdf'))
        # plt.close()

        masstocharge = np.divide(data[:, 3], data[:, 4])

        plot_scatter_mz_vs_error(masstocharge, avg_percent_error, std_percent_error)
        plt.savefig(os.path.join(dirpath, str(file).split('.')[0]+'mz_vs_avg_percent_error.pdf'))
        plt.close()