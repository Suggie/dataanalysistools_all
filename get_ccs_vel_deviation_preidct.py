#get ccs dev from pred files sort according to calibration type

import os
import pandas as pd
import numpy as np

def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files

def get_ccs_dev_rmse_(dirpath, cal_mode_string, ccs_database_fpath):
    """
    get list of files corresponding to a cal mode type and assemble the file to one
    :param dirpath: directory
    :param cal_mode_string: cal mode string
    :param ccs_database_fpath: ccs database file path
    :return: void.
    """

    ccs_database_df = pd.read_csv(ccs_database_fpath)
    end_id = cal_mode_string + '_pred.csv'
    file_list = get_files(dirpath, endid=end_id)

    pub_ccs_arr = []
    pred_ccs_arr = []
    pred_mob_arr = []
    ccs_dev_arr = []
    id_arr = []
    oligomer_arr = []
    mass_arr = []
    charge_arr = []
    wh_arr = []
    wv_arr = []
    ccs_rmse_arr = []
    superclass_arr = []
    subclass_arr = []

    for index, file in enumerate(file_list):
        with open(os.path.join(dirpath, file), 'r') as calout_file:
            calib_f = calout_file.read().splitlines()
            for line in calib_f:
                if line.startswith('#Waveht'):
                    wh = line.split(',')[-1]
                    wh_arr.append(float(wh))
                if line.startswith('#Wavevel'):
                    wv = line.split(',')[-1]
                    wv_arr.append(float(wv))
                if line.startswith('#CCS_RMSE'):
                    ccs_rmse = line.split(',')[-1]
                    ccs_rmse_arr.append(float(ccs_rmse))

        df = pd.read_csv(os.path.join(dirpath, file), header=12)
        true_ccs_arr = []

        for ind, (idd, oligod, massd, charged, predccs) in enumerate(
                zip(df['#id'], df['oligomer'], df['mass'], df['charge'], df['pred_ccs'])):
            true_ccs = ccs_database_df[(ccs_database_df['id'] == idd) &
                                       (ccs_database_df['n_oligomers'] == oligod) &
                                       (ccs_database_df['z'] == charged)]['ccs_n2'].values[0]
            true_ccs_arr.append(true_ccs)

        pred_ccs = df['pred_ccs'].values
        pred_mob = df['pred_mob'].values
        id_ = df['#id'].values
        oligo = df['oligomer'].values
        mass = df['mass'].values
        charge = df['charge'].values

        superclass = df['superclass'].values
        subclass = df['subclass'].values

        ccs_dev = (pred_ccs - true_ccs_arr) * 100 / true_ccs_arr

        pub_ccs_arr.append(true_ccs_arr)
        pred_ccs_arr.append(pred_ccs)
        ccs_dev_arr.append(ccs_dev)
        pred_mob_arr.append(pred_mob)
        id_arr.append(id_)
        oligomer_arr.append(oligo)
        mass_arr.append(mass)
        charge_arr.append(charge)

        superclass_arr.append(superclass)
        subclass_arr.append(subclass)

    pub_ccs_arr = np.array(pub_ccs_arr)
    pred_ccs_arr = np.array(pred_ccs_arr)
    ccs_dev_arr = np.array(ccs_dev_arr)
    pred_mob_arr = np.array(pred_mob_arr)
    id_arr = np.array(id_arr)
    oligomer_arr = np.array(oligomer_arr)
    mass_arr = np.array(mass_arr)
    charge_arr = np.array(charge_arr)

    superclass_arr = np.array(superclass_arr)
    subclass_arr = np.array(subclass_arr)


    data_string = ''
    header = '#id,oligomer,mass,charge,superclass,subclass,wh,wv,ccs_rmse,pred_ccs,true_ccs,pred_mob,ccs_dev\n'

    for index2, (
    id_sub, olig_sub, mass_sub, charge_sub, superclass_sub, subclass_sub, predccs_sub, pubccs_sub, predmob_sub, ccsdev_sub) in enumerate(
            zip(id_arr, oligomer_arr, mass_arr, charge_arr, superclass_arr, subclass_arr, pred_ccs_arr, pub_ccs_arr, pred_mob_arr, ccs_dev_arr)):


            for num in range(len(id_sub)):
                line = '{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(id_sub[num],
                                                               olig_sub[num],
                                                               mass_sub[num],
                                                               charge_sub[num],
                                                                         superclass_sub[num],
                                                                         subclass_sub[num],
                                                               wh_arr[index2],
                                                               wv_arr[index2],
                                                                   ccs_rmse_arr[index2],
                                                               predccs_sub[num],
                                                               pubccs_sub[num],
                                                               predmob_sub[num],
                                                               ccsdev_sub[num])
                data_string += line


    output_string = header + data_string

    out_fname = cal_mode_string + '_ccs_dev_output.csv'

    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()



if __name__ == '__main__':

    ccs_db_file = r"C:\Users\sugyan\Documents\CCSCalibration\ccsdatabse_positive.csv"
    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\CrossClass\CalDenat_UncalNative"

    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True',
                            'blended_True',
                            'blended_True_exp_True',
                            'relax_True_terms_6',
                            'relax_True_terms_6_exp_True']

    # cal_mode_string_list = ['power_law_True',
    #                         'power_law_True_exp_True',
    #                         'blended_True',
    #                         'blended_True_exp_True']

    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        get_ccs_dev_rmse_(dirpath, cal_mode_string, ccs_db_file)

