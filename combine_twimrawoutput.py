import os
import numpy as np
import itertools


def get_sub_directories(root_dir):
    """
    get directories under the root directory
    :param root_dir: root dir path
    :return: list of sub directories
    """
    sub_dirs = [x[0] for x in os.walk(root_dir)]
    return sub_dirs

def get_twim_raw_files(dirpath):
    """
    get twim raw csv files under dirpath
    :param dirpath: directory location
    :return: twim raw csv files
    """
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('raw.csv')]
    return files


def combine_raw_data(list_of_dirs):

    file_id = []
    file_content = [[] for _ in range(len(list_of_dirs[1:]))]

    for index, dirs in enumerate(list_of_dirs[1:]):
        list_twim_files = get_twim_raw_files(dirs)
        for ind, file in enumerate(list_twim_files):
            with open(os.path.join(dirs, file), 'r') as twimfile:
                twim_file = twimfile.read()
                file_content[index].append(twim_file)


    fcontent = list(map(list,zip(*file_content)))

    for index, species_cont in enumerate(fcontent):
        range_fname = [[] for _ in range(len(species_cont))]
        wh_arr = [[] for _ in range(len(species_cont))]
        wv_arr = [[] for _ in range(len(species_cont))]
        data_arr = [[] for _ in range(len(species_cont))]
        for ind, fcont in enumerate(species_cont):
            file_read = fcont.splitlines()
            for line in file_read:
                if line.startswith('#Range'):
                    range_fname[ind].append((line.split(',')[1]))
                elif line.startswith('$WaveHt'):
                    wh_arr[ind].append((line.split(',')[1:]))
                elif line.startswith('$WaveVel'):
                    wv_arr[ind].append((line.split(',')[1:]))
                else:
                    data_arr[ind].append((line.split(',')[1:]))

        wh_arr_comb = np.hstack(wh_arr)
        wv_arr_comb = np.hstack(wv_arr)
        data_arr_comb = np.hstack(data_arr)
        print('heho')




    print('heho')


if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\UBQDENAT\TWIMExtract_Output"
    list_sub_dirs = get_sub_directories(dirpath)
    combine_raw_data(list_sub_dirs)