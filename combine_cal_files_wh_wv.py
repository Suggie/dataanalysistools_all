import os
import numpy as np
import pandas as pd


def get_cal_files_wh_wv(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('WV_.csv')]
    files_mob = [x for x in files if x.startswith('caloutput_mob')]
    files_relax = [x for x in files if x.startswith('caloutput_relax')]
    files_ruotolo_vel = [x for x in files if x.startswith('caloutput_ruotoloetal_vel')]
    files_ruotolo_dt = [x for x in files if x.startswith('caloutput_ruotoloetal_dt')]
    return files_mob, files_relax, files_ruotolo_vel, files_ruotolo_dt

def getdata(file, path):
    fname_long = str(file)
    if fname_long.startswith('caloutput_ruotoloetal'):
        fchars = fname_long.split('_')[:3]
    else:
        fchars = fname_long.split('_')[:2]
    fname_small = ('_').join(x for x in fchars)
    data = np.genfromtxt(os.path.join(path, file), delimiter=',', skip_header=1, dtype='str')
    data = data[:, : -1]
    return data, fname_small

def combine_data(data_combined_array, fname_small, path):
    fname = fname_small + '_combined_WH_WV.csv'
    if fname_small == 'caloutput_ruotoloetal_vel':
        data_comb_array = np.vstack(data_combined_array)
        df = pd.DataFrame(data=data_comb_array, columns=['wh', 'wv', 'id', 'oligomer', 'mass', 'charge', 'ccs',
                                                         'corr_ccs', 'exp_vel', 'est_vel', 'vel_deviation',
                                                         'ln_exp_vel', 'ln_corr_ccs'])
        df = df.sort_values(by=['id', 'oligomer', 'charge', 'wv', 'wh'], ascending=[True, True, True, True, True],
                            kind='mergesort')
        df.to_csv(os.path.join(path, fname))
        print('heho')

    elif fname_small == 'caloutput_ruotoloetal_dt':
        data_comb_array = np.vstack(data_combined_array)
        df = pd.DataFrame(data=data_comb_array, columns=['wh', 'wv', 'id', 'oligomer', 'mass', 'charge', 'pub_ccs',
                                                         'est_ccs', 'ccs_deviation', 'ln_corr_pub_ccs', 'ln_corr_time'])
        df = df.sort_values(by=['id', 'oligomer', 'charge', 'wv', 'wh'], ascending=[True, True, True, True, True],
                            kind='mergesort')
        df.to_csv(os.path.join(path, fname))
        print('heho')

    else:
        data_comb_array = np.vstack(data_combined_array)

        # wh = data_comb_array[:, 0]
        # wv = data_comb_array[:, 1]
        # identity = data_comb_array[:, 2]
        # oligomer = data_comb_array[:, 3]
        # charge = data_comb_array[:, 5]
        #
        # sort_ind = np.lexsort((wh, wv, charge, oligomer, identity))
        #
        # sort_array = data_comb_array[sort_ind]

        df = pd.DataFrame(data=data_comb_array, columns=['wh', 'wv', 'id', 'oligomer', 'mass', 'charge', 'pub_ccs',
                                                         'corr_ccs', 'exp_avg_vel', 'pred_ccs', 'pred_corr_ccs',
                                                         'pred_avg_vel', 'pubccs_error_percent',
                                                         'corrccs_error_percent', 'avg_vel_error_percent'])
        df = df.sort_values(by=['id', 'oligomer', 'charge', 'wv', 'wh'], ascending=[True, True, True, True, True],
                            kind='mergesort')
        df.to_csv(os.path.join(path, fname))

        print('heho')



if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\082018_CalData\_calfiles\cal_blac_7_8_blacdi_11_12_cytc_6_7_polyala33"
    file_list = get_cal_files_wh_wv(dirpath)
    for files in file_list:
        datastore = []
        for file in files:
            data, fname_small = getdata(file, dirpath)
            datastore.append(data)
        datastore = np.asarray(datastore)
        combine_data(datastore, fname_small, dirpath)
        print('heho')
