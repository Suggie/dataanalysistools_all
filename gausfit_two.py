# gaussfit_001.py

import os

import numpy as np
from math import sqrt, log
from scipy.optimize import curve_fit
from scipy.stats import linregress
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages as pdfpage


def fileread(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('raw_blur.csv')]
    return files


def getdata(file):
    data = np.genfromtxt(file, delimiter=',', skip_header=1)
    xdata = data[2:, 0]
    ydata = data[2:, 1:]
    waveht = data[0, 1:]
    wavevel = data[1, 1:]
    return xdata, ydata, waveht, wavevel


def gaussfunc(x, y0, A, xc, w):
    rxc = ((x - xc) ** 2) / (2 * (w ** 2))
    y = y0 + A * (np.exp(-rxc))
    return y


def gauss_two(x, y0_1, A_1, xc_1, w_1, y0_2, A_2, xc_2, w_2):
    gauss1 = gaussfunc(x, y0_1, A_1, xc_1, w_1)
    gauss2 = gaussfunc(x, y0_2, A_2, xc_2, w_2)
    y = gauss1 + gauss2
    return y


def adjrsquared(r2, param, num):
    y = 1 - (((1 - r2) * (num - 1)) / (num - param - 1))
    return y


def resandfwhm(xc, w):
    fwhm = 2 * (sqrt(2 * log(2))) * w
    res = xc / fwhm
    return fwhm, res


def estimateparam(array, xdata):
    ymax = np.max(array)
    maxindex = np.nonzero(array == ymax)[0]
    peakmax_x = xdata[maxindex]
    binsnum = np.nonzero(array)
    widthbin = len(binsnum[0])
    return peakmax_x, widthbin, ymax


def main():
    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\CYTC\TWIMExtract_Output\7cs"
    files = fileread(dirpath)
    # files = files[2]
    for file in files:
        print (str(file))
        f_abs_path = os.path.join(dirpath, file)
        xdata, ydata, wave_ht, wave_vel = getdata(f_abs_path)
        fname = str(file)
        gaussfit_arr = []
        gaussfit1_arr = []
        gaussfit2_arr = []
        y0_1_arr = []
        y0_2_arr = []
        xc_1_arr = []
        xc_2_arr = []
        width_1_arr = []
        width_2_arr = []
        amp_1_arr = []
        amp_2_arr = []
        fwhm_1_arr = []
        fwhm_2_arr = []
        res_1_arr = []
        res_2_arr = []
        slope_arr = []
        intercept_arr = []
        r2_arr = []
        adjrsq_arr = []
        for index, col in enumerate(ydata.T):
            # print(wave_ht[index], wave_vel[index])
            td_o, w_o, amp_o = estimateparam(col, xdata)
            td_1, w_1, amp_1 = td_o[0] + 2.7, w_o, amp_o * 0.05
            # print(td_o[0], w_o, amp_o)
            popt, pcov = curve_fit(gauss_two, xdata, col, method='lm', p0=[0, amp_o, td_o[0], 1., 0, amp_1, td_1, 0.5], maxfev=10000)
            yprime = gauss_two(xdata, *popt)
            gaussfit1 = gaussfunc(xdata, 0, popt[1], popt[2], popt[3])
            gaussfit2 = gaussfunc(xdata, 0, popt[5], popt[6], popt[7])
            gaussfit1_arr.append(gaussfit1)
            gaussfit2_arr.append(gaussfit2)
            fwhm_1, res_1 = resandfwhm(popt[2], popt[3])
            fwhm_2, res_2 = resandfwhm(popt[6], popt[7])
            y0_1_arr.append(popt[0])
            y0_2_arr.append(popt[4])
            amp_1_arr.append(popt[1])
            amp_2_arr.append(popt[5])
            xc_1_arr.append(popt[2])
            xc_2_arr.append(popt[6])
            width_1_arr.append(popt[3])
            width_2_arr.append(popt[7])
            fwhm_1_arr.append(fwhm_1)
            fwhm_2_arr.append(fwhm_2)
            res_1_arr.append(res_1)
            res_2_arr.append(res_2)
            gaussfit_arr.append(yprime)


            slope, intercept, rvalue, pvalue, stderr = linregress(col, yprime)
            adjrsq = adjrsquared(rvalue ** 2, 8, len(col))
            slope_arr.append(slope)
            intercept_arr.append(intercept)
            r2_arr.append(rvalue ** 2)
            adjrsq_arr.append(adjrsq)

        gaussfit_arr = np.array(gaussfit_arr)

        pdf_fig = pdfpage(os.path.join(dirpath, fname+'_2gauss_Gausfitdata.pdf'))
        for k in range(len(wave_ht)):
            plt.figure()
            plt.scatter(xdata, ydata.T[k])
            plt.plot(xdata, gaussfit_arr[k], ls='--', color='black')
            plt.plot(xdata, gaussfit1_arr[k], ls = '--', color='red')
            plt.plot(xdata, gaussfit2_arr[k], ls='--', color='green')
            plt.title(str(wave_ht[k])+'_WH_'+str(wave_vel[k])+'_WV')
            pdf_fig.savefig()
            plt.close()
        pdf_fig.close()

        outarray = np.array(
            [wave_ht, wave_vel, y0_1_arr, amp_1_arr, xc_1_arr, width_1_arr, fwhm_1_arr, res_1_arr, y0_2_arr, amp_2_arr, xc_2_arr, width_2_arr, fwhm_2_arr, res_2_arr, slope_arr, intercept_arr, r2_arr, adjrsq_arr],
            dtype='float')
        outarray2 = np.transpose(outarray)
        np.savetxt(os.path.join(dirpath, fname+'_2gauss_outarraygaussfit.csv'), outarray2, delimiter=',', fmt='%s',
                   header='WH, WV, y0_1, Amp_1, xc_1, w_1, fwhm_1, res_1, y0_2, Amp_2, xc_2, w_2, fwhm_2, res_2, lnreg_slope, lnreg_intercept, lnreg_r2, lnreg_adjrsq')
        print(str(file) + '_2gauss_outarraygaussfit.csv')

main()