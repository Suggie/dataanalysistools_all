import os
import numpy as np

def getfiles(path):
    files = os.listdir(path)
    files = [x for x in files if x.endswith('cal.csv')]
    return files

def getdata(absfilepath):
    data = np.genfromtxt(absfilepath, delimiter=',', skip_header=1, dtype='str')
    return data

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\062618_Polyala_caldata\TWIMExtractOutput\CCS_calibration_all"
    files = getfiles(dirpath)
    for file in files:
        fname = str(file)
        fname_strip = fname.split('_')[:-1]
        data = getdata(os.path.join(dirpath, fname))
        del_rows = [20, 25, 30, 31, 35, 36, 37]
        newdata = np.delete(data, del_rows, axis=0)
        # newfname = ('_').join(x for x in fname_strip)+'_curated.csv'
        header1 = 'WH, WV, y0, Amp, xc, w, fwhm, res, lnreg_slope, lnreg_intercept, lnreg_r2, lnreg_adjrsq'
        header2 = 'id, wv, wh, oligomer, mass, charge, pressure, dt'
        np.savetxt(os.path.join(dirpath, fname), newdata, fmt='%s', delimiter=',', header=header2)
        print(fname+' Done!')