# gauss blur data

# import re
import os
import numpy as np
# import matplotlib.pyplot as plt
# import pandas as pd
from math import sqrt, pi, log
from scipy.optimize import curve_fit, least_squares
from scipy.ndimage import gaussian_filter1d
from scipy.stats import linregress


def get_gauss_filt_stdev(sigma_real, sigma_mod):
    """
    using the sigma expected for expected resolution, what is the stdev needed in gauss filter.
    :param sigma_real: sigma observed in experiment
    :param sigma_mod: sigma expect
    :return: gauss filter stdev
    """
    gaus_filt_stdev = sigma_real/(sigma_mod * 2 * sqrt(pi))
    return gaus_filt_stdev


def get_twim_raw_file(dirpath):
    """
    get the raw.csv files from the directory
    :param dirpath: directory path
    :return: list of files
    """
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('raw.csv')]
    return files


def getdata(file):
    """
    get data from the twim output
    :param file: filepath
    :return: xdata list, ydata list, waveheight list, wavevelocity list
    """

    data = np.genfromtxt(file, delimiter=',', skip_header=1)
    xdata = data[2:, 0]
    ydata = data[2:, 1:]
    waveht = data[0, 1:]
    wavevel = data[1, 1:]
    return xdata, ydata, waveht, wavevel


def gaussfunc(x, y0, A, xc, w):
    """
    gaussian function
    :param x: x
    :param y0: y0
    :param A: amp
    :param xc: centroid
    :param w: sigma
    :return: gauss
    """
    rxc = ((x - xc) ** 2) / (2 * (w ** 2))
    y = y0 + A * (np.exp(-rxc))
    return y


def adjrsquared(r2, param, num):
    """
    calculate the adjusted r squared
    :param r2: r squared
    :param param: number of parameters used in function
    :param num: number of data points
    :return: adj r squared
    """
    y = 1 - (((1 - r2) * (num - 1)) / (num - param - 1))
    return y


def resandfwhm(xc, w):
    """
    calculate resolution and fwhm from the optimzied parameters
    :param xc: centroid
    :param w: sigma
    :return: fwhm, res
    """
    fwhm = 2 * (sqrt(2 * log(2))) * w
    res = xc / fwhm
    return fwhm, res


def estimateparam(array, xdata):
    """
    estimate the initial guess for gaussian fitting
    :param array: ydata
    :param xdata: xdata drift time
    :return: centroid guess, width guess, amplitude guess
    """
    ymax = np.max(array)
    maxindex = np.nonzero(array == ymax)[0]
    peakmax_x = xdata[maxindex]
    binsnum = np.nonzero(array)
    widthbin = len(binsnum[0])
    return peakmax_x, widthbin, ymax


def err_func_gauss_blur(gauss_blur_sigma, xdata, ydata, res_mod):
    """
    error function minimize the difference in resolution of mod and resolution of data after blur
    :param gauss_blur_sigma: for optimization
    :param xdata: x data drift time
    :param ydata: y data intensity
    :param res_mod: resolution to model
    :return: residuals
    """

    y_blur = gaussian_filter1d(ydata, gauss_blur_sigma)
    td_o, w_o, amp_o = estimateparam(y_blur, xdata)
    popt, pcov = curve_fit(gaussfunc, xdata, y_blur, method='lm', p0=[0, amp_o, td_o[0], 0.1], maxfev=10000)
    fwhm_blur, res_blur = resandfwhm(popt[2], popt[3])
    err = (res_blur - res_mod)/res_mod
    return err


def write_blur_raw_data(dirpath, original_twim_file, xdata, gauss_blur_raw_data, wh_list, wv_list):
    """
    write blur data
    :param dirpath: directory path
    :param original_twim_file: original twim file name
    :param gauss_blur_raw_data: gauss filter raw data
    :param wh_list: wh list
    :param wv_list: wv list
    :return: void. Writes output file
    """

    twim_range_fname = str(original_twim_file).split('#')[1]
    twim_range_fname = twim_range_fname.split('_raw')[0]
    output_string = ''
    range_fname_string = '#Range file name:,'+twim_range_fname+'\n'
    output_string += range_fname_string

    wh_string_list = ','.join([str(x) for x in wh_list])
    wv_string_list = ','.join([str(x) for x in wv_list])

    wh_line = '{},{}\n'.format('$WaveHt:', wh_string_list)
    wv_line = '{},{}\n'.format('$WaveVel:', wv_string_list)

    output_string += wh_line
    output_string += wv_line

    y_data = np.array(gauss_blur_raw_data).T

    for index, (dt_, y_data_) in enumerate(zip(xdata, y_data)):
        y_data_string = ','.join([str(x) for x in y_data_])
        line = '{},{}\n'.format(dt_, y_data_string)
        output_string += line

    out_fname = str(original_twim_file).split('.csv')[0] + '_blur.csv'

    with open(os.path.join(dirpath, out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()

def write_blurdata_gausfit_output(dirpath, orig_twim_fname, wh_list, wv_list, gauss_blur_sigma_arr, popt_arr,
                                      linreg_arr, len_data_points_arr):
    """
    write the gauss fit output for the blur data
    :param dirpath: directory
    :param orig_twim_fname: orig twim file name
    :param wh_list: wh list
    :param wv_list: wv list
    :param gauss_blur_sigma_arr: sigma used to blur the data
    :param popt_arr: popt arr
    :param linreg_arr: linreg arr
    :return: void. Writes the output
    """

    output_string = ''
    header = 'WH,WV,y0,Amp,xc,w,fwhm,res,lnreg_slope,lnreg_intercept,lnreg_r2,lnreg_adjrsq,gauss_blur_sigma\n'
    output_string += header

    for index, (wh_, wv_, gauss_blur_sigma, popt, linreg_, len_data_points) in enumerate(zip(wh_list,
                                                                            wv_list,
                                                                            gauss_blur_sigma_arr,
                                                                            popt_arr,
                                                                            linreg_arr,
                                                                                             len_data_points_arr)):
        y0, amp, xc, width = popt
        fwhm, res = resandfwhm(xc, width)
        slope, intercept, rvalue, pvalue, stderr = linreg_
        r_sq = rvalue ** 2
        adjrsq = adjrsquared(r_sq, 4, len_data_points)

        line = '{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(wh_,
                                                                 wv_,
                                                                 y0,
                                                                 amp,
                                                                 xc,
                                                                 width,
                                                                 fwhm,
                                                                 res,
                                                                 slope,
                                                                 intercept,
                                                                 r_sq,
                                                                 adjrsq,
                                                                 gauss_blur_sigma)

        output_string += line


    outfname = orig_twim_fname + '_outarraygaussfit_blur.csv'

    with open(os.path.join(dirpath, outfname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()



def blur_twim_raw_data(dirpath, list_of_files, res_mod = 10, gauss_blur_sigma_constant=0.5):

    for index, twim_file in enumerate(list_of_files):

        gauss_blur_ydata = []
        gauss_blur_sigma_arr = []

        popt_blur_arr = []
        linreg_arr = []

        len_data_points_arr = []

        xdata, ydata, wave_ht, wave_vel = getdata(os.path.join(dirpath, twim_file))

        for index, (wh, wv, raw_data) in enumerate(zip(wave_ht, wave_vel, ydata.T)):
            td_o, w_o, amp_o = estimateparam(raw_data, xdata)
            popt, pcov = curve_fit(gaussfunc, xdata, raw_data, method='lm', p0=[0, amp_o, td_o[0], 0.1], maxfev=10000)
            fwhm, res = resandfwhm(popt[2], popt[3])
            print('check res')

            if res < res_mod:
                gauss_blur_sigma = gauss_blur_sigma_constant
            else:
                x0_init_guess = 5
                args_tuple = (xdata, raw_data, res_mod)
                leastsq_ = least_squares(err_func_gauss_blur, x0_init_guess, method='trf', args=args_tuple,
                                         bounds=[0, np.inf])
                gauss_blur_sigma = leastsq_.x[0]

            #blur the raw data from the gauss blur sigma obtained from the above condition
            gaussblur_rawdata = gaussian_filter1d(raw_data, sigma=gauss_blur_sigma)
            td_o_blur, w_o_blur, amp_o_blur = estimateparam(gaussblur_rawdata, xdata)
            popt_blur, pcov_blur = curve_fit(gaussfunc, xdata, gaussblur_rawdata, method='lm',
                                             p0=[0, amp_o_blur, td_o_blur[0], 0.1],
                                             maxfev=10000)
            # here to check during debugging
            fwhm_blur, res_blur = resandfwhm(popt_blur[2], popt_blur[3])

            y_prime = gaussfunc(xdata, *popt_blur)
            linreg_ = linregress(gaussblur_rawdata, y_prime)

            linreg_arr.append(linreg_)
            gauss_blur_ydata.append(gaussblur_rawdata)
            gauss_blur_sigma_arr.append(gauss_blur_sigma)

            popt_blur_arr.append(popt_blur)

            len_data_points_arr.append(len(raw_data))

        write_blur_raw_data(dirpath, twim_file, xdata, gauss_blur_ydata, wave_ht, wave_vel)

        write_blurdata_gausfit_output(dirpath, twim_file, wave_ht, wave_vel, gauss_blur_sigma_arr, popt_blur_arr,
                                      linreg_arr, len_data_points_arr)


        print('heho')


if __name__ == '__main__':

    dirpath = r"C:\MassLynx\Default.pro\Data\090319\TWIMExtract_Output\polyala_200_90_100_8000_600_30_003"
    twim_files = get_twim_raw_file(dirpath)
    blur_twim_raw_data(dirpath, twim_files, res_mod=10)