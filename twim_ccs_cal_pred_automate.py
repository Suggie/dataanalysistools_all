import os
import time
from get_ccs_vel_deviation_preidct import get_ccs_dev_rmse_
from get_ccs_dev_unk_ions import gen_ccs_dev_ions_with_dir
from plot_box_whisker_rmse_err import plot_box_whisk_plot_and_save_output
from get_ccs_rmse_wh_wv import gen_ccs_rmse_wh_wv_all_ion_dir
from plot_rmse_caloutput_contour import plot_rmse
from plot_ccs_dev_bar_graph_publication import plot_data_one_bar_
from plot_ccs_dev_calibrants_output import plot_ccs_dev_bar_graph_with_species
from plot_rmse_ccsdev_bar_from_stats import plot_bar_graph_from_stats_file


def auto_gen_all_resuls(working_dir, ccs_db_file, cal_mode_string_list, wh_wv_exception_list, cal_exception_list, image_type):

    # get ccs_dev_output
    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        get_ccs_dev_rmse_(working_dir, cal_mode_string, ccs_db_file)

    time.sleep(2)

    #get ccs dev species

    gen_ccs_dev_ions_with_dir(working_dir, endid='ccs_dev_output.csv', wh_wv_exception_list=wh_wv_exception_list)

    time.sleep(2)

    # get ccs rmse and dev stats with box whisk plot

    plot_box_whisk_plot_and_save_output(working_dir, startid='ccs_dev_rmse_all_out', cal_mode_string_list=cal_mode_string_list,
                                        endid='ccs_dev_output', wh_wv_exception_list=wh_wv_exception_list)

    time.sleep(2)

    # plot rmse ccs dev bar from stats file

    plot_bar_graph_from_stats_file(working_dir, endid='all_stats.csv', exception_list=cal_exception_list, image_type=image_type)


    # get ccs rmse for wh wv including all ions

    gen_ccs_rmse_wh_wv_all_ion_dir(working_dir, endid='ccs_dev_output.csv')

    time.sleep(2)

    # plot rmse cal output contour

    plot_rmse(working_dir,
                  x_key='wh',
                  y_key='wv',
                  z_key='ccs_rmse',
                  endid='wh_wv_rmse.csv',
                  outfid='ccs_rmse',
                  ext=image_type,
                  exception_list=wh_wv_exception_list,
                  colormap='bone_r',
                  color_mode='notdiverge',
                  scale=None,
              clim=None)

    # plot ccs dev and rmse bar graph
    # plot_ccs_dev_bar_graph(dirpath, outid, ext, endid='ccs_dev_output.csv', y_key='ccs_dev_avg',
    #                        y_err_key='ccs_dev_std')
    plot_ccs_dev_bar_graph_with_species(working_dir, endid='ccs_dev_output_species.csv', outid='ccs_dev', ext=image_type, y_key='ccs_dev_avg',
                           y_err_key='ccs_dev_std')
    plot_ccs_dev_bar_graph_with_species(working_dir, endid='ccs_dev_output_species.csv', outid='ccs_rmse', ext=image_type,
                           y_key='ccs_rmse', y_err_key=None)

    # for cal_mode in cal_mode_string_list:
    #     ccs_dev_fname = cal_mode + '_ccs_dev_output_species.csv'
    #     ccsdev_fpath = os.path.join(working_dir, ccs_dev_fname)
    #     plot_data_one_bar_(fpath=ccsdev_fpath,
    #                        y_key='ccs_dev_avg',
    #                        yerr_key='ccs_dev_std',
    #                        color_mode='list',
    #                        outfname='ccs_dev_species.png')
    #     plot_data_one_bar_(fpath=ccsdev_fpath,
    #                        y_key='ccs_rmse',
    #                        yerr_key=None,
    #                        color_mode='list',
    #                        outfname='ccs_rmse_species.png')

if __name__ == '__main__':


    working_dir = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_fixa\_CrossCls\CalPeptide_NatProt"

    ccs_db_file = r"C:\Users\sugyan\PycharmProjects\twim_ccs_calibration\CCSDatabase\ccsdatabse_positive.csv"
    # cal_mode_string_list = ['power_law_True',
    #                         'power_law_True_exp_True',
    #                         'relax_True_terms_6',
    #                         'relax_True_terms_6_exp_True',
    #                         'blended_True',
    #                         'blended_True_exp_True']

    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True',
                            'blended_True',
                            'blended_True_exp_True']

    wh_wv_exception_list = [[40, 300], [35, 300]]#, [30, 300], [40, 400]]
    cal_exception_list = ['relax_True_terms_6', 'relax_True_terms_6_exp_True']

    auto_gen_all_resuls(working_dir, ccs_db_file, cal_mode_string_list, wh_wv_exception_list, cal_exception_list=None,
                        image_type='.pdf')
