# gaussfit_001.py

import os

import numpy as np
from math import sqrt, log
from scipy.optimize import curve_fit
from scipy.stats import linregress
from scipy.interpolate import interp2d, interp1d
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages as pdfpage


def fileread(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('raw.csv')]
    return files


def getdata(file):
    data = np.genfromtxt(file, delimiter=',', skip_header=1)
    xdata = data[2:, 0]
    ydata = data[2:, 1:]
    waveht = data[0, 1:]
    wavevel = data[1, 1:]
    return xdata, ydata, waveht, wavevel


def gaussfunc(x, y0, A, xc, w):
    rxc = ((x - xc) ** 2) / (2 * (w ** 2))
    y = y0 + A * (np.exp(-rxc))
    return y


def gauss_two(x, y0_1, A_1, xc_1, w_1, y0_2, A_2, xc_2, w_2):
    gauss1 = gaussfunc(x, y0_1, A_1, xc_1, w_1)
    gauss2 = gaussfunc(x, y0_2, A_2, xc_2, w_2)
    y = gauss1 + gauss2
    return y


def adjrsquared(r2, param, num):
    y = 1 - (((1 - r2) * (num - 1)) / (num - param - 1))
    return y


def resandfwhm(xc, w):
    fwhm = 2 * (sqrt(2 * log(2))) * w
    res = xc / fwhm
    return fwhm, res


def estimateparam(array, xdata):
    ymax = np.max(array)
    maxindex = np.nonzero(array == ymax)[0]
    peakmax_x = xdata[maxindex]
    binsnum = np.nonzero(array)
    widthbin = len(binsnum[0])
    return peakmax_x, widthbin, ymax


if __name__ == '__main__':
    dirpath = r"T:\Sugyan\G1 data for CCS calibration\PolyA CSV\Low IMS\09_11_POLYA_1MIN_LOWERIMS_07"
    files = fileread(dirpath)
    # files = files[2]
    for file in files:
        print (str(file))
        f_abs_path = os.path.join(dirpath, file)
        xdata, ydata, wave_ht, wave_vel = getdata(f_abs_path)
        x_new = np.linspace(0.5, np.max(xdata)+1, len(xdata))
        fname = str(file)
        gaussfit_arr = []
        y0_arr = []
        xc_arr = []
        width_arr = []
        amp_arr = []
        fwhm_arr = []
        res_arr = []
        slope_arr = []
        intercept_arr = []
        r2_arr = []
        adjrsq_arr = []
        for index, col in enumerate(ydata.T):
            # print(wave_ht[index], wave_vel[index])
            td_o, w_o, amp_o = estimateparam(col, xdata)
            # print(td_o[0], w_o, amp_o)
            popt, pcov = curve_fit(gaussfunc, xdata, col, method='lm', p0=[0, amp_o, td_o[0], 0.1], maxfev=10000)
            yprime = gaussfunc(xdata, *popt)
            fwhm, res = resandfwhm(popt[2], popt[3])
            y0_arr.append(popt[0])
            amp_arr.append(popt[1])
            xc_arr.append(popt[2])
            width_arr.append(popt[3])
            fwhm_arr.append(fwhm)
            res_arr.append(res)
            gaussfit_arr.append(yprime)


            slope, intercept, rvalue, pvalue, stderr = linregress(col, yprime)
            adjrsq = adjrsquared(rvalue ** 2, 4, len(col))
            slope_arr.append(slope)
            intercept_arr.append(intercept)
            r2_arr.append(rvalue ** 2)
            adjrsq_arr.append(adjrsq)


        pdf_fig = pdfpage(os.path.join(dirpath, fname+'_Gausfitdata.pdf'))
        for k in range(len(wave_ht)):
            plt.figure()
            plt.scatter(xdata, ydata.T[k])
            plt.plot(xdata, gaussfit_arr[k], ls='--', color='black')
            plt.title(str(wave_ht[k])+'_WH_'+str(wave_vel[k])+'_WV')
            pdf_fig.savefig()
            plt.close()
        pdf_fig.close()

        outarray = np.array(
            [wave_ht, wave_vel, y0_arr, amp_arr, xc_arr, width_arr, fwhm_arr, res_arr, slope_arr, intercept_arr, r2_arr, adjrsq_arr],
            dtype='float')
        outarray2 = np.transpose(outarray)
        np.savetxt(os.path.join(dirpath, fname+'_outarraygaussfit.csv'), outarray2, delimiter=',', fmt='%s',
                   header='WH, WV, y0, Amp, xc, w, fwhm, res, lnreg_slope, lnreg_intercept, lnreg_r2, lnreg_adjrsq')
        print(str(file) + '_outarraygaussfit.csv')