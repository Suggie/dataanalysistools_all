import pandas as pd
import os

dirpath = r"C:\Users\sugyan\Documents\CIUSuite"
fname = "matt_append_dataset_test1.csv"
df1 = pd.read_csv(os.path.join(dirpath, fname), sep=',', header=None)
df1.columns = ['MassCharge', 'intensity']
df1['Retention Time'] = 26

# df1.sort_values(by='intensity', ascending=False, inplace=True)

df2 = df1[df1['intensity'] > 15]

writer = pd.ExcelWriter(os.path.join(dirpath, '26new.xlsx'))
df2.to_excel(writer, 'Sheet1')
writer.save()