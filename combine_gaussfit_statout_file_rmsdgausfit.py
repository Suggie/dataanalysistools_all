import os


def get_files(dirpath):
    files = os.listdir(dirpath)
    files =[x for x in files if x.endswith('statout.csv')]
    return files

def read_data(file, dirpath):
    helix_num = int(str(file).split('_')[1])
    output_string = ''
    with open(os.path.join(dirpath, file), 'r') as statout:
        statdata = statout.read().splitlines()
        for line in statdata:
            if line.startswith(''):
                line_chars = line.split(',')
                output_string = str(helix_num)+','+','.join([x for x in line_chars[1:]])+'\n'
    return output_string


if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\MembraneMDfiles\AcAlaPeptide_ClusterAnalysis\300K"
    files = get_files(dirpath)
    output_fname = str(files[0]).split('_')[-3]+'_gausfit_statout_combined.csv'
    with open(os.path.join(dirpath, output_fname), 'w') as outfile:
        out_string = ''
        header = 'helix_num, wt_mean, wt_var, slope, variance, r2\n'
        out_string += header
        for file in files:
            readout = read_data(file, dirpath)
            out_string += readout

        outfile.write(out_string)