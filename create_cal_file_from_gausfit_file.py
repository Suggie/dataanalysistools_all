import os
import numpy as np
import pandas as pd
import tkinter
import tkinter.filedialog as filedialog

# create a file dialog where you need to input id, oligomer, mass, charge, and pressure. it takes in gausfit file
# as input and puts the drift time and wave height and wave velocity information in new file to be formatted as cal file

def read_create_calfile(create_cal_file):
    cal_file_entry = pd.read_csv(create_cal_file, sep=',')
    file = cal_file_entry['filepath'].values
    id_ = cal_file_entry['id'].values
    mass = cal_file_entry['mass'].values
    oligomer = cal_file_entry['oligomer'].values
    charge = cal_file_entry['charge'].values
    pressure = cal_file_entry['pressure'].values
    return file, id_, mass, oligomer, charge, pressure

def read_gaussfit_file(file):
    gaussdata = np.genfromtxt(file, delimiter=',', skip_header=1)
    wh = gaussdata[:, 0]
    wv = gaussdata[:, 1]
    dt = gaussdata[:, 4]
    return wh, wv, dt

def create_cal_file(id_, wh, wv, oligo, mass, charge, pressure, dt, dirpath):
    header = '# id,wv,wh,oligomer,mass,charge,pressure,dt\n'
    fname = '_'.join([id_, str(oligo), str(charge), 'cal']) + '.csv'
    with open(os.path.join(dirpath, fname), 'w') as calfile:
        calfile.write(header)
        for ind in range(len(wh)):
            line = '{}, {}, {}, {}, {}, {}, {}, {}\n'.format(id_, wv[ind], wh[ind], oligo, mass, charge, pressure, dt[ind])
            calfile.write(line)
        calfile.close()

if __name__=='__main__':
    cal_entry_file = r"C:\Users\sugyan\Documents\Processed data\090618_Kicki_Peptide_Study\103118_CCSCalib\cal_file_create_entry.csv"
    outdirloc = r"C:\Users\sugyan\Documents\Processed data\090618_Kicki_Peptide_Study\103118_CCSCalib\_calfiles"
    gaussfile, id_, mass, oligomer, charge, pressure = read_create_calfile(cal_entry_file)
    for ind, file in enumerate(gaussfile):
        wh, wv, dt = read_gaussfit_file(file)
        create_cal_file(id_[ind], wh, wv, oligomer[ind], mass[ind], charge[ind], pressure[ind], dt, outdirloc)