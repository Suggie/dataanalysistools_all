## plot ccs devation from calibrations output

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def get_file_endid(dirpath, endid='cal_output'):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files

def plot_bar_graph(y_, y_err, x_labels):
    num = len(y_)
    ind = np.arange(num)
    width = 0.7
    fig, ax = plt.subplots(figsize=(20,10))
    if y_err:
        rects = ax.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
    else:
        rects = ax.bar(ind, y_, width, color='black', alpha=1)
    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel('% CCS deviation')


def plot_bar_graph_with_two_breaks_y_axis(y_, y_err, x_labels, ylim1, ylim2, ylim3, color_list):
    """
    plot bar graph with y breaks [includes all the limits]
    :param y_: y data
    :param y_err: y err data
    :param x_labels: x labels
    :param y_breaks: y breaks
    :return: plot object
    """

    num = len(y_)
    ind = np.arange(num)
    width = 0.7

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True, figsize=(20, 10), gridspec_kw={'height_ratios': [1, 4, 1]})

    if not color_list:
        if y_err:
            rects1 = ax1.bar(ind, y_, width, color='black', alpha=1, yerr=y_err,
                             error_kw=dict(lw=1, capsize=1, capthick=1))
            rects2 = ax2.bar(ind, y_, width, color='black', alpha=1, yerr=y_err,
                             error_kw=dict(lw=1, capsize=1, capthick=1))
            rects3 = ax3.bar(ind, y_, width, color='black', alpha=1, yerr=y_err,
                             error_kw=dict(lw=1, capsize=1, capthick=1))
        else:
            rects1 = ax1.bar(ind, y_, width, color='black', alpha=1)
            rects2 = ax2.bar(ind, y_, width, color='black', alpha=1)
            rects3 = ax3.bar(ind, y_, width, color='black', alpha=1)

    if color_list:
        if y_err:
            rects1 = ax1.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err,
                             error_kw=dict(lw=1, capsize=1, capthick=1))
            rects2 = ax2.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err,
                             error_kw=dict(lw=1, capsize=1, capthick=1))
            rects3 = ax3.bar(ind, y_, width, color=color_list, alpha=1, yerr=y_err,
                             error_kw=dict(lw=1, capsize=1, capthick=1))
        else:
            rects1 = ax1.bar(ind, y_, width, color=color_list, alpha=1)
            rects2 = ax2.bar(ind, y_, width, color=color_list, alpha=1)
            rects3 = ax3.bar(ind, y_, width, color=color_list, alpha=1)

    ax1.set_ylim(ylim1)
    ax2.set_ylim(ylim2)
    ax3.set_ylim(ylim3)

    ax1.spines['bottom'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax3.spines['top'].set_visible(False)

    ax1.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)
    ax3.tick_params(axis='x', which='both', bottom=False, top=False)

    ax1.yaxis.set_ticks(ylim1)
    ax3.yaxis.set_ticks(ylim3)

    ax2.yaxis.set_ticks(np.arange(ylim2[0], ylim2[1]+1, 1))

    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel('% CCS deviation')


def plot_bar_graph_with_one_breaks_y_axis(y_, y_err, x_labels, ylim1, ylim2):
    """
    plot bar graph with y breaks [includes all the limits]
    :param y_: y data
    :param y_err: y err data
    :param x_labels: x labels
    :param y_breaks: y breaks
    :return: plot object
    """

    num = len(y_)
    ind = np.arange(num)
    width = 0.7

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(20, 10), gridspec_kw={'height_ratios': [1, 4]})


    if y_err:
        rects1 = ax1.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
        rects2 = ax2.bar(ind, y_, width, color='black', alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
    else:
        rects1 = ax1.bar(ind, y_, width, color='black', alpha=1)
        rects2 = ax2.bar(ind, y_, width, color='black', alpha=1)

    ax1.set_ylim(ylim1)
    ax2.set_ylim(ylim2)


    ax1.spines['bottom'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)

    ax1.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False)

    ax1.yaxis.set_ticks(ylim1)

    ax2.yaxis.set_ticks(np.arange(ylim2[0], ylim2[1]+1, 1))

    plt.xticks(ind, x_labels, rotation='vertical')
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel('% CCS deviation')


def add_subclass_to_caloutput_data(dataframe):
    """
    use the dataframe from the output data and add the subclass
    :param dataframe:
    :return:
    """
    ccs_database_df = pd.read_csv(r"C:\Users\sugyan\PycharmProjects\twim_ccs_cal\CCSDatabase\ccsdatabse_positive.csv")
    ccs_database_df['spec_id'] = ccs_database_df['id'] + ccs_database_df['n_oligomers'].map(str) + ccs_database_df[
        'z'].map(str)

    dataframe = dataframe.sort_values(['mass', 'charge'], ascending=[True, True])
    dataframe['spec_id'] = dataframe['#id'] + dataframe['oligomer'].map(str) + dataframe['charge'].map(str)

    dataframe_class = []
    dataframe_color = []

    for spec_id in dataframe['spec_id'].values:
        dataframe_class.append(ccs_database_df[ccs_database_df['spec_id'] == spec_id]['subclass'].values[0])
        dataframe_color.append(ccs_database_df[ccs_database_df['spec_id'] == spec_id]['subclass_color_hex_code'].values[0])

    dataframe_class = np.array(dataframe_class)
    dataframe_color = np.array(dataframe_color)

    dataframe['subclass'] = dataframe_class
    dataframe['subclass_color'] = dataframe_color

    return dataframe


def plot_ccs_dev_bar_graph(dirpath, outid, ext, endid='ccs_dev_output.csv', y_key='ccs_dev_avg', y_err_key='ccs_dev_std'):
    list_of_files = get_file_endid(dirpath, endid=endid)

    list_ = list_of_files
    # for file in list_of_files:
    #     if str(file).find('species') == -1:
    #         list_.append(file)


    subclass_list = ['aminoacid', 'organic_compound', 'lipid', 'tripeptide', 'reverse_peptide','polyala', 'denature_protein','native_protein']

    for file in list_:

        df = pd.read_csv(os.path.join(dirpath, file))
        df = df.drop_duplicates()
        df = df.sort_values(['mass', 'charge'], ascending=[True, True])
        df = add_subclass_to_caloutput_data(df)

        x_label = []
        ydata = []
        y_err_data = []
        color_list = []

        for index, subclass in enumerate(subclass_list):
            if subclass in df['subclass'].values:
                df_subclass = df[df['subclass'] == subclass]
                if y_err_key:
                    for ind, (id, oligo, ydata_, yerrdata_, color_) in enumerate(zip(df_subclass['#id'], df_subclass['oligomer'],
                                                                             df_subclass[y_key], df_subclass[y_err_key],
                                                                                     df_subclass['subclass_color'])):
                        ydata.append(ydata_)
                        y_err_data.append(yerrdata_)
                        label_string = '_'.join([id, str(oligo)])
                        x_label.append(label_string)
                        color_list.append(color_)
                else:
                    for ind, (id, oligo, ydata_, color_) in enumerate(zip(df_subclass['#id'], df_subclass['oligomer'],
                                                                             df_subclass[y_key], df_subclass['subclass_color'])):
                        ydata.append(ydata_)
                        label_string = '_'.join([id, str(oligo)])
                        x_label.append(label_string)
                        color_list.append(color_)


        out_fname = str(file).split('.')[0]

        # plot_bar_graph(ydata, y_err_data, x_label)
        plot_bar_graph_with_two_breaks_y_axis(ydata, y_err_data, x_label, (10, 25), (-5, 5), (-10, -8),
                                              color_list=color_list)
        # plot_bar_graph_with_one_breaks_y_axis(ydata, y_err_data, x_label, (10, 20), (-8, 6))
        # plt.ylim((-15, 25))
        plt.savefig(os.path.join(dirpath, out_fname + outid + ext), dpi=500)
        plt.close()

        print('heho')



def plot_ccs_dev_bar_graph_with_species(dirpath, outid, ext, endid='ccs_dev_output.csv', y_key='ccs_dev_avg', y_err_key='ccs_dev_std'):
    list_of_files = get_file_endid(dirpath, endid=endid)

    list_ = list_of_files


    # subclass_list = ['lipid','polyala', 'denature_protein','native_protein']
    subclass_list = ['aminoacid', 'organic_compound', 'lipid', 'tripeptide', 'reverse_peptide', 'polyala',
                     'denature_protein', 'native_protein']

    for file in list_:

        df = pd.read_csv(os.path.join(dirpath, file))
        df = df.drop_duplicates()
        df = df.sort_values(['mass', 'charge'], ascending=[True, True])

        x_label = []
        ydata = []
        y_err_data = []

        for index, subclass in enumerate(subclass_list):
            if subclass in df['subclass'].values:
                df_subclass = df[df['subclass'] == subclass]
                if y_err_key:
                    for ind, (id, oligo, ydata_, yerrdata_) in enumerate(zip(df_subclass['#id'], df_subclass['oligomer'],
                                                                             df_subclass[y_key], df_subclass[y_err_key])):
                        ydata.append(ydata_)
                        y_err_data.append(yerrdata_)
                        label_string = '_'.join([id, str(oligo)])
                        x_label.append(label_string)
                else:
                    for ind, (id, oligo, ydata_) in enumerate(zip(df_subclass['#id'], df_subclass['oligomer'],
                                                                             df_subclass[y_key])):
                        ydata.append(ydata_)
                        label_string = '_'.join([id, str(oligo)])
                        x_label.append(label_string)


        out_fname = str(file).split('.')[0]

        plot_bar_graph(ydata, y_err_data, x_label)
        # plt.ylim((-15, 25))
        plt.savefig(os.path.join(dirpath, out_fname + outid + ext), dpi=500)
        plt.close()

        print('heho')



if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\Processed data\102419_CalProcessing\fig_data\fig1\leaveonespeciesout"
    endid = 'caldata.csv'
    outid = 'ccs_dev_ylim'
    ext = '.pdf'
    plot_ccs_dev_bar_graph(dirpath, endid=endid, outid=outid, ext=ext)