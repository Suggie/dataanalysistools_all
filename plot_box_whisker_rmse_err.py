# plot box whisker with scatter points

import os
import itertools
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import iqr, ttest_ind, sem, t
from math import sqrt



def get_files(dirpath, startid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith(startid)]
    return files



def box_whisker_plot(data, color_labels, marker_list, outid, dirpath, xlim=None, ylim=None, y_int=1.0):
    """
    plots box whisker plot from the list of 1 d data given
    :param data:
    :return:
    """

    for ind, (data_, color_, marker_) in enumerate(zip(data, color_labels, marker_list)):
        x_rep = np.random.normal(ind+1, 0.05, size=len(data_))
        plt.scatter(x_rep, data_, marker=marker_, color=color_, alpha=0.2)

    box_prop_dict = dict(linewidth=1.0)
    whisker_prop_dict = dict(linewidth=1.0)
    cap_prop_dict = dict(linewidth=1.0)
    median_prop_dict = dict(linestyle='-', color='black', linewidth=1.0)
    mean_prop_dict = dict(linestyle='-', color='red', linewidth=1.0)
    bp = plt.boxplot(data, showfliers=False, meanline=True, showmeans=True,
                     boxprops=box_prop_dict,
                     whiskerprops=whisker_prop_dict,
                     capprops=cap_prop_dict,
                     medianprops=median_prop_dict,
                     meanprops=mean_prop_dict)
    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)
        plt.yticks(np.arange(np.floor(ylim[0]), np.ceil(ylim[1])+1, y_int))

    out_fname = outid + '_box_whisk_plot.png'
    plt.savefig(os.path.join(dirpath, out_fname), dpi=500)
    plt.close()



def ttest_save_out(list_of_data, list_of_labels, out_id, dirpath):
    outlines2 = ''
    header2 = '#caltype1,caltype2,t-stat,p-value\n'
    outlines2 += header2

    for ind, (cal_combo, data_combo) in enumerate(
            zip(itertools.combinations(list_of_labels, 2), itertools.combinations(list_of_data, 2))):
        ttest = ttest_ind(data_combo[0], data_combo[1])
        line2 = '{},{},{},{}\n'.format(cal_combo[0], cal_combo[1], ttest[0], ttest[1])
        outlines2 += line2

    with open(os.path.join(dirpath, 'ttest_' + out_id + '.csv'), 'w') as outfile:
        outfile.write(outlines2)
        outfile.close()


def get_confidence_interval(data, confidence=0.95):
    """
    Calculate the confidence interval given the data
    :param data:
    :param confidence:
    :return: mean, margin_error, ci_low, ci_high
    """
    num_data = len(data)
    mean_ = np.mean(data)
    std_err = sem(data)
    # ci_int = 1.96 * np.std(data) / sqrt(num_data)
    ci_int = std_err * t.ppf((1 + confidence) / 2, num_data - 1)
    return num_data, mean_, std_err, ci_int, mean_ - ci_int, mean_ + ci_int


def gen_ccs_dev_rmse_stats(cal_mode_string_list, ccs_dev_rmse_array):
    """
    use the ccs dev array and ccs rmse mean to calculate mean, median, std, iqr and put it in dictionary
    :param cal_mode_string_list:
    :return: dict of cal_mode, mean, median, std, iqr
    """


    mean_arr = []
    median_arr = []
    std_arr = []
    iqr_arr = []

    low_quantile_arr = []
    high_quantile_arr = []

    low_whisker_arr = []
    high_whisker_arr = []

    num_data_arr = []
    std_err_arr = []
    margin_error_arr = []
    confidence_interval_arr = []

    fifth_percentile_arr = []
    ninetyfifth_percentile_arr = []


    for arr in ccs_dev_rmse_array:

        low_quantile, high_quantile = np.quantile(arr, [.25, .75])
        iqr_ = high_quantile - low_quantile
        low_whisker = low_quantile - 1.5 * iqr_
        high_whisker = high_quantile + 1.5 * iqr_

        low_quantile_arr.append(low_quantile)
        high_quantile_arr.append(high_quantile)

        low_whisker_arr.append(low_whisker)
        high_whisker_arr.append(high_whisker)

        num_data, mean, std_err, margin_err, ci_low, ci_high = get_confidence_interval(arr, confidence=0.95)
        mean_arr.append(mean)
        num_data_arr.append(num_data)
        std_err_arr.append(std_err)
        margin_error_arr.append(margin_err)
        confidence_interval_arr.append([ci_low, ci_high])
        median_arr.append(np.median(arr))
        std_arr.append(np.std(arr))
        iqr_arr.append(iqr(arr))
        fifth_percentile_arr.append(np.percentile(arr, 5))
        ninetyfifth_percentile_arr.append(np.percentile(arr, 95))




    stat_dict = dict()
    stat_dict['cal_mode_string_list'] = cal_mode_string_list
    stat_dict['num_sample'] = num_data_arr
    stat_dict['mean_arr'] = mean_arr
    stat_dict['median_arr'] = median_arr
    stat_dict['std_arr'] = std_arr
    stat_dict['iqr_arr'] = iqr_arr
    stat_dict['low_quantile_arr'] = low_quantile_arr
    stat_dict['high_quantile_arr'] = high_quantile_arr
    stat_dict['low_whisker_arr'] = low_whisker_arr
    stat_dict['high_whisker_arr'] = high_whisker_arr
    stat_dict['std_err_arr'] = std_err_arr
    stat_dict['margin_err_arr'] = margin_error_arr
    stat_dict['confidence_interval_arr'] = confidence_interval_arr
    stat_dict['5thpercentile_arr'] = fifth_percentile_arr
    stat_dict['95thpecentile_arr'] = ninetyfifth_percentile_arr

    return stat_dict


def write_ccs_dev_rmse_stats_out(output_dict, cal_mode_string_list, out_id, dirpath):
    """
    write the ccs dev rmse stats output based on the output dictionary
    :param output_dict: output dict
    :param outid: outid string to append to fname
    :param dirpath: directory to save output
    :return: write file
    """

    output_string = ''
    header = '#caltype,num_sample,mean,median,std,q1,q3,iqr,low_whisk,high_whisk,std_err,margin_err,ci_low,ci_high,5th_percent,95th_percent\n'
    output_string += header



    for num in range(len(cal_mode_string_list)):
        line = '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(output_dict['cal_mode_string_list'][num],
                                                     output_dict['num_sample'][num],
                                                     output_dict['mean_arr'][num],
                                                     output_dict['median_arr'][num],
                                                     output_dict['std_arr'][num],
                                                              output_dict['low_quantile_arr'][num],
                                                              output_dict['high_quantile_arr'][num],
                                                     output_dict['iqr_arr'][num],
                                                              output_dict['low_whisker_arr'][num],
                                                              output_dict['high_whisker_arr'][num],
                                         output_dict['std_err_arr'][num],
                                         output_dict['margin_err_arr'][num],
                                         output_dict['confidence_interval_arr'][num][0],
                                         output_dict['confidence_interval_arr'][num][1],
                                                              output_dict['5thpercentile_arr'][num],
                                                              output_dict['95thpecentile_arr'][num])

        output_string += line

    stats_out_fname = out_id + '.csv'

    with open(os.path.join(dirpath, stats_out_fname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


def get_outliers_from_distribution(dirpath, cal_mode_string_list, df_list, df_key, outid):
    """
    get outliers outside of 25th and 75th percentile from the distribution
    :param dirpath:directory
    :param df_list: list of pandas dataframe
    :param stat_dict: stat dict
    :return:void. write files for outliers
    """


    for index, (df_, cal_mode) in enumerate(zip(df_list, cal_mode_string_list)):
        arr_ = df_[df_key]
        low_quantile, high_quantile = np.quantile(arr_, [.25, .75])
        iqr_ = high_quantile - low_quantile
        low_whisker = low_quantile - 1.5 * iqr_
        high_whisker = high_quantile + 1.5 * iqr_
        df_outlier = df_[(df_[df_key] > high_whisker) | (df_[df_key] < low_whisker)]
        outfname = df_key + '_' + cal_mode + '_' + outid + '.csv'
        df_outlier.to_csv(os.path.join(dirpath, outfname), index=False)




def plot_box_whisk_plot_and_save_output(dirpath, startid, cal_mode_string_list, endid=None, wh_wv_exception_list=None, cal_exception_list=None):
    """
    loop through each type of cal func and store data for box whisker plot. save output for mean, median, variance, iqr.
    also do t test and get p value
    :param dirpath: directory to look for files
    :param endid: file ends with key
    :param data_key: column name for pandas
    :return: data and data labels list
    """

    df_list = []

    ccs_rmse_all = []
    ccs_dev_all = []

    ccs_rmse_cal_exception = []
    ccs_dev_cal_exception = []


    for ind, cal_mode in enumerate(cal_mode_string_list):
        if endid:
            fname = cal_mode + '_' + endid + '.csv'
        else:
            fname = startid + '_' + cal_mode + '.csv'

        df = pd.read_csv(os.path.join(dirpath, fname))

        if wh_wv_exception_list:
            for ind, arr in enumerate(wh_wv_exception_list):
                df = df[(df['wh'] != arr[0]) | (df['wv'] != arr[1])]

        df_list.append(df)

        ccs_rmse_all.append(df['ccs_rmse'].values)
        ccs_dev_all.append(df['ccs_dev'].values)

        if cal_exception_list:
            if cal_mode not in cal_exception_list:
                ccs_rmse_cal_exception.append(df['ccs_rmse'].values)
                ccs_dev_cal_exception.append(df['ccs_dev'].values)



    rmse_all_dict = gen_ccs_dev_rmse_stats(cal_mode_string_list, ccs_rmse_all)
    dev_all_dict = gen_ccs_dev_rmse_stats(cal_mode_string_list, ccs_dev_all)

    get_outliers_from_distribution(dirpath, cal_mode_string_list, df_list, df_key='ccs_dev', outid='outliers')
    get_outliers_from_distribution(dirpath, cal_mode_string_list, df_list, df_key='ccs_rmse', outid='outliers')


    # write ccs dev rmse stats out

    write_ccs_dev_rmse_stats_out(rmse_all_dict, cal_mode_string_list, 'ccs_rmse_all_stats', dirpath)
    write_ccs_dev_rmse_stats_out(dev_all_dict, cal_mode_string_list, 'ccs_dev_all_stats', dirpath)


    # perform t test and save the output

    ttest_save_out(ccs_rmse_all, cal_mode_string_list, 'ccs_rmse_all', dirpath)
    ttest_save_out(ccs_dev_all, cal_mode_string_list, 'ccs_dev_all', dirpath)



    # plot box whisker plot

    good_bad_outid = 'good_bad_combined'

    color_label_good_bad = ['slateblue', 'slateblue', 'royalblue', 'royalblue', 'orangered', 'orangered', 'coral', 'coral',
                   'teal', 'teal', 'dodgerblue', 'dodgerblue']

    color_label_all = ['slateblue', 'royalblue', 'orangered', 'coral',
                                                        'teal', 'dodgerblue',]

    markers = ['X','o','X','o', 'X','o','X','o', 'X','o','X','o']
    markers_2 = ['x', 'o', 'x', 'o', 'x', 'o', 'x', 'o', 'x', 'o', 'x', 'o']
    markers_3 = ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o']

    ylim_rmse_good_bad = None#(0,10)
    ylim_dev_good_bad = None#(-20, 75)


    if cal_exception_list:
        ccs_rmse_box_whisk = ccs_rmse_cal_exception
        ccs_dev_box_whisk = ccs_dev_cal_exception
    else:
        ccs_rmse_box_whisk = ccs_rmse_all
        ccs_dev_box_whisk = ccs_dev_all


    box_whisker_plot(ccs_rmse_box_whisk, ylim=ylim_rmse_good_bad, y_int=1.0, color_labels=color_label_all,
                     marker_list=markers_3, outid='ccs_rmse_all', dirpath=dirpath)

    box_whisker_plot(ccs_dev_box_whisk, ylim=ylim_dev_good_bad, y_int=10.0, color_labels=color_label_all,
                     marker_list=markers_3, outid='ccs_dev_all', dirpath=dirpath)



if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_fixa\All_Ion\LeaveOneSpec"

    start_id = 'ccs_dev_rmse_all_out'
    end_id = 'ccs_dev_output'

    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True',
                            'relax_True_terms_6',
                            'relax_True_terms_6_exp_True',
                            'blended_True',
                            'blended_True_exp_True']

    # cal_mode_string_list = ['power_law_True',
    #                         'power_law_True_exp_True',
    #                         'blended_True',
    #                         'blended_True_exp_True']

    cal_excp_list = ['relax_True_terms_6',
                     'relax_True_terms_6_exp_True']

    data_key = 'ccs_dev'
    out_id = 'all'

    exception_list = [[40, 300], [35, 300]]#, [30, 300], [40, 400]]

    plot_box_whisk_plot_and_save_output(dirpath, start_id, cal_mode_string_list, endid=None,
                                        wh_wv_exception_list=exception_list, cal_exception_list=cal_excp_list)