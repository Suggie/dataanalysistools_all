# combine all the codes to run this once and generate all the outputs

# import get_ccs_vel_deviation_calibrants_output
from get_ccs_vel_deviation_calibrants_output import gen_calibrants_dev_output, gen_calibrants_dev_output_wh_wv
# import get_rmse_cal_output
from get_rmse_cal_output import generate_rmse_out_caltype_file_list
# import plot_rmse_caloutput_contour
from plot_rmse_caloutput_contour import plot_rmse
# import plot_ccs_dev_calibrants_output
from plot_ccs_dev_calibrants_output import plot_ccs_dev_bar_graph
# import get_ccs_vel_deviation_crossval_leaveoneionout
from get_ccs_vel_deviation_crossval_leaveoneionout import get_ccs_dev_output_unk_pred, get_ccs_dev_output_unk_pred_wh_wv
from get_ccs_vel_deviation_crossval_leaveonespeciesout import get_ccs_dev_output_unk_pred as get_ccs_dev_output_unk_pred_loso
from get_ccs_vel_deviation_crossval_leaveonespeciesout import get_ccs_dev_output_unk_pred_wh_wv as get_ccs_dev_output_unk_pred_wh_wv_loso
# import get_ccs_rmse_dev_all_output
from get_ccs_rmse_dev_all_output import get_ccs_rmse_dev_output_all
# import plot_box_whisker_rmse_err
from plot_box_whisker_rmse_err import plot_box_whisk_plot_and_save_output
from plot_rmse_ccsdev_bar_from_stats import plot_bar_graph_from_stats_file
import time
import winsound


def sound_notification(time):
    frequency = 2500
    winsound.Beep(frequency, time)

def module_all_ion_analysis(dirpath, cal_mode_string_list, wh_wv_exception_list, exception_list_contour_plot, pic_ext):
    """
    do all the analysis required
    :param dirpath: dirpath
    :param cal_mode_string_list: cal mode string list
    :exception_list_contour_plot: exception of wh and wv conditions
    :pic_ext: picture output extension(.png, .pdf)
    :return: all outputs
    """

    # generate species ccs dev output
    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        gen_calibrants_dev_output(dirpath, cal_mode_string, wh_wv_exception_list)
        gen_calibrants_dev_output_wh_wv(dirpath, cal_mode_string)

    # generate rmse output
    for ind, cal_mode_string in enumerate(cal_mode_string_list):
        generate_rmse_out_caltype_file_list(dirpath, cal_mode_string)


    # plot contour of ccs dev output for wh, wv for each species

    plot_rmse(dirpath,
              x_key='wh',
              y_key='wv',
              z_key='ccs_dev',
              endid='species_ccs_dev_output.csv',
              outfid='ccs_dev',
              ext=pic_ext,
              exception_list=exception_list_contour_plot,
              colormap='bone_r',
              color_mode='notdiverge')

    # plot contour of ccs rmse output for wh, wv for each cal function

    plot_rmse(dirpath,
              x_key='wh',
              y_key='wv',
              z_key='ccs_rmse',
              endid='rmse.csv',
              outfid='ccs_rmse',
              ext=pic_ext,
              exception_list=exception_list_contour_plot,
              colormap='bone_r',
              color_mode='notdiverge',
              scale='sqrt')

    # plot bar chart off ccs dev of all species for each cal function
    endid2 = 'ccs_dev_output.csv'
    outid2 = 'ccs_dev'
    plot_ccs_dev_bar_graph(dirpath, endid='ccs_dev_output.csv', outid='ccs_dev', ext=pic_ext)


def module_crossval_analysis(dirpath, cal_mode_string_list, ccs_db_file, exception_list_contour_plot, pic_ext,
                             cal_exception_list=None, leaveionout=True):
    """
    do all the analysis required in leave one ion out
    :param dirpath:
    :param cal_mode_string_list:
    :param exception_list_contour_plot:
    :param pic_ext:
    :return:
    """

    # generate species ccs dev output

    if leaveionout:
        for ind, cal_mode_string in enumerate(cal_mode_string_list):
            get_ccs_dev_output_unk_pred(dirpath, cal_mode_string, ccs_db_file, exception_list_contour_plot)
            get_ccs_dev_output_unk_pred_wh_wv(dirpath, cal_mode_string, ccs_db_file)

    else:
        for ind, cal_mode_string in enumerate(cal_mode_string_list):
            get_ccs_dev_output_unk_pred_loso(dirpath, cal_mode_string, ccs_db_file, exception_list_contour_plot)
            get_ccs_dev_output_unk_pred_wh_wv_loso(dirpath, cal_mode_string, ccs_db_file)

    # pause for 5
    time.sleep(2)


    # get ccs rmse dev all output file
    for cal_mode in cal_mode_string_list:
        get_ccs_rmse_dev_output_all(dirpath, cal_mode=cal_mode, endid='species_ccs_dev_output.csv')
    # pause for 5
    time.sleep(2)


    # plot contour of ccs dev output for wh, wv for each species
    plot_rmse(dirpath,
              x_key='wh',
              y_key='wv',
              z_key='ccs_dev',
              endid='species_ccs_dev_output.csv',
              outfid='ccs_dev',
              ext=pic_ext,
              exception_list=exception_list_contour_plot,
              colormap='inferno',
              color_mode='notdiverge')

    # plot contour of ccs rmse output for wh, wv for each species
    plot_rmse(dirpath,
              x_key='wh',
              y_key='wv',
              z_key='ccs_rmse',
              endid='species_ccs_dev_output.csv',
              outfid='ccs_rmse',
              ext=pic_ext,
              exception_list=exception_list_contour_plot,
              colormap='inferno',
              color_mode='notdiverge')


    # plot bar chart of ccs dev of species for each cal function
    plot_ccs_dev_bar_graph(dirpath, endid='ccs_dev_output.csv', outid='ccs_dev', ext=pic_ext)

    # #plot bar chart of ccs rmse of species for each cal function
    plot_ccs_dev_bar_graph(dirpath, endid='ccs_dev_output.csv', outid='ccs_rmse', ext=pic_ext,
                           y_key='ccs_rmse_avg', y_err_key='ccs_rmse_std')

    # gen ccs rmse / dev stats good/bad/all condition plus box whisker plot
    plot_box_whisk_plot_and_save_output(dirpath, startid='ccs_dev_rmse_all_out',
                                        cal_mode_string_list=cal_mode_string_list,
                                        wh_wv_exception_list=exception_list_contour_plot,
                                        cal_exception_list=cal_excp_list)
    # pause for 5
    time.sleep(2)

    plot_bar_graph_from_stats_file(dirpath, exception_list=cal_exception_list, image_type=pic_ext)

if __name__ == '__main__':

    ccs_db_file = r"C:\Users\sugyan\PycharmProjects\twim_ccs_calibration\CCSDatabase\ccsdatabse_positive.csv"
    exception_list = [[35, 300], [40, 300]]#, [30, 300], [40, 400]]
    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True',
                            'relax_True_terms_6',
                            'relax_True_terms_6_exp_True',
                            'blended_True',
                            'blended_True_exp_True']

    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True']

    # cal_mode_string_list = ['power_law_True',
    #                         'power_law_True_exp_True',
    #                         'blended_True',
    #                         'blended_True_exp_True']

    # cal_mode_string_list = ['relax_True_terms_6',
    #                         'relax_True_terms_6_exp_True']

    pic_extension = '.pdf'

    cal_excp_list = ['relax_True_terms_6', 'relax_True_terms_6_exp_True']

    # dirpath_all_ion = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_nofixa\Peptides\All"
    # dirpath_one_ion = r"C:\Users\sugyan\Documents\Processed data\062719_CalProcessing\_nofixa\Peptides\LeaveOneIon"
    dirpath_one_species = r"C:\Users\sugyan\Documents\Processed data\092219_CalProcessing\powerlaw\smallmol\leaveonespec"

    # print('MODULE ALL ION')
    # module_all_ion_analysis(dirpath=dirpath_all_ion, cal_mode_string_list=cal_mode_string_list,
    #                         wh_wv_exception_list=exception_list,
    #                         exception_list_contour_plot=exception_list, pic_ext=pic_extension)
    #
    # print('MODULE ONE ION OUT')
    # module_crossval_analysis(dirpath=dirpath_one_ion, cal_mode_string_list=cal_mode_string_list,
    #                          ccs_db_file=ccs_db_file,
    #                          exception_list_contour_plot=exception_list, pic_ext=pic_extension,
    #                          cal_exception_list=cal_excp_list, leaveionout=True)

    #
    print('MODULE ONE SPECIES OUT')
    module_crossval_analysis(dirpath=dirpath_one_species, cal_mode_string_list=cal_mode_string_list,
                             ccs_db_file=ccs_db_file,
                             exception_list_contour_plot=exception_list, pic_ext=pic_extension,
                             cal_exception_list=cal_excp_list, leaveionout=False)

    sound_notification(5)