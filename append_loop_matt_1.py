import pandas as pd
import os

def list_files(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('.csv')]
    return files

def create_dataframe_field_threshold(dataframe, field='intensity', value=15):
    df = dataframe[dataframe[field] > value]
    return df

def create_pandas_dataframe(dirpath, file):
    file_chars = str(file).split('.')
    ret_time = float(file_chars[0])
    df = pd.read_csv(os.path.join(dirpath, file), sep=',', header=None)
    df.columns = ['MassCharge', 'intensity']
    df['Retention Time'] = ret_time
    return df

def combine_dataframe_save(list_of_dataframe, dirpath):
    final_df = pd.concat(list_of_dataframe)
    write = pd.ExcelWriter(os.path.join(dirpath, 'combined_ret_int_cutoff.xlsx'))
    final_df.to_excel(write, 'Sheet1')
    write.save()

if __name__=='__main__':
    dirpath = r"C:\Users\sugyan\Documents\CIUSuite\matt_append"
    files = list_files(dirpath)
    new_df_list = []
    for file in files:
        original_df = create_pandas_dataframe(dirpath, file)
        df_intensity_cutoff = create_dataframe_field_threshold(original_df, 'intensity', 15)
        new_df_list.append(df_intensity_cutoff)
    combine_dataframe_save(new_df_list, dirpath)