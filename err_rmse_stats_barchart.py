import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def get_stat_file(dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith('stats.csv')]
    files = [x for x in files if x.startswith('300WV_400WV')]
    return files

def load_file(fpath):
    data = np.genfromtxt(fpath, delimiter=',', skip_header=1, dtype='str')
    caltype = data[:, 0]
    sort_index = np.argsort(caltype)
    data_sort = data[sort_index]
    return data_sort



def plot_bar_chart(ydata, yerr, xlabel, fname, dirpath):
    num = len(ydata)
    ind = np.arange(num)
    width = 0.7
    fig, ax = plt.subplots()
    rects = ax.bar(ind, ydata, width, color='grey', yerr=yerr, error_kw=dict(lw=1, capsize=1, capthick=1))
    ax.set_ylabel('% '+ str(fname).split('_')[0])
    plt.xticks(ind, tuple(xlabel), rotation='vertical')
    plt.subplots_adjust(bottom=0.3)
    plt.tick_params(labelsize=5.5)

    plt.savefig(os.path.join(dirpath, fname + '_.png'), dpi=500)
    plt.close()

if __name__=='__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\NativeProteins\All_ions"

    stat_files = get_stat_file(dirpath)

    xlabel_ = np.array(['powerfit_exp_False', 'powerfit_exp_True', 'relaxation_False_term_4', 'relaxation_False_term_6', 'relaxation_True_term_4', 'relaxation_True_term_6', 'relaxationexp_True_term_4', 'relaxationexp_True_term_6'], dtype='str')

    for ind, file in enumerate(stat_files):
        file_path = os.path.join(dirpath, file)
        data = load_file(file_path)
        ydata = data[:, 1]
        xlab = data[:, 0]
        plot_bar_chart(np.array(data[:, 1], dtype='float'), np.array(data[:, 2], dtype='float'), xlabel_, str(file), dirpath)
        print('heho')
