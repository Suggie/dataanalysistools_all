import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from midpoint_normalize_class import MidpointNormalize
from plot_ccs_dev_bar_graph_publication import gen_color_dict
from collections import OrderedDict


def gen_dataframe_and_color_dict(dirpath, file, exception_list=None):
    """
    generate dataframe after exception list if any and create the color dict
    :param filepath: fpath
    :param exceptionlist: exception list in wh and wv conditions
    :return: df and color dict
    """

    df = pd.read_csv(os.path.join(dirpath, file))
    df = df.drop_duplicates()

    if exception_list:
        for ind, except_list in enumerate(exception_list[1:]):
            df = df[(df[exception_list[0][0]] != except_list[0]) | (df[exception_list[0][1]] != except_list[1])]

    color_dict = gen_color_dict(df)

    return df, color_dict


def plot_ccs_dev_vs_rmse_scatter(calmode_string_list, startid, dirpath, exception_list, label_key, ext):
    """
    plot scatter of ccs rmse
    :param calmode_string_list: cal mode string list
    :param startid: start id
    :param dirpath:directory
    :param ext: extension of picture file
    :return:void. Save picture
    """

    for ind, cal_mode in enumerate(calmode_string_list):
        fname = startid + '_' + cal_mode + '.csv'
        df, color_dict = gen_dataframe_and_color_dict(dirpath, fname, exception_list)

        fig = plt.figure()
        ax = plt.subplot(111)

        if label_key == 'wh_wv':

            for index, (ccs_rmse, ccs_dev, color, label) in enumerate(zip(df['ccs_rmse'], df['ccs_dev'],
                                                                                      color_dict['color_wh_wv'],
                                                                                      color_dict['wh_wv_label_list'])):
                ax.scatter(ccs_rmse, ccs_dev, color=color, label=label)

        if label_key == 'ion':

            for index, (ccs_rmse, ccs_dev, color, label) in enumerate(zip(df['ccs_rmse'], df['ccs_dev'],
                                                                                      color_dict['color_species'],
                                                                                      color_dict['label_list'])):
                ax.scatter(ccs_rmse, ccs_dev, color=color, label=label)


        if label_key == 'mol_type':

            for index, (ccs_rmse, ccs_dev, color, label) in enumerate(zip(df['ccs_rmse'], df['ccs_dev'],
                                                                                      color_dict['color_group'],
                                                                                      color_dict['moltype'])):
                ax.scatter(ccs_rmse, ccs_dev, color=color, label=label)





        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        ax.legend(by_label.values(), by_label.keys(), loc='center left', bbox_to_anchor = (1, 0.5), ncol=2)
        plt.xlabel('ccs_rmse')
        plt.ylabel('ccs_dev')
        outfname = startid + '_' + cal_mode + '_rmse_vs_dev_' + label_key + ext
        plt.savefig(os.path.join(dirpath, outfname), bbox_inches='tight')
        plt.close()
        print('heho')


def plot_ccs_rmse_dev_hist(calmode_string_list, startid, dirpath, exception_list, ext):
    for ind, cal_mode in enumerate(calmode_string_list):
        fname = startid + '_' + cal_mode + '.csv'
        df, color_dict = gen_dataframe_and_color_dict(dirpath, fname, exception_list)
        plt.hist(df['ccs_rmse'].values, bins='auto', alpha=0.7, color='black')
        plt.xlabel('CCS_RMSE')
        outfname = startid + '_' + cal_mode + '_rmse_hist'+ ext
        plt.savefig(os.path.join(dirpath, outfname))
        plt.close()

        plt.hist(df['ccs_dev'].values, bins='auto', alpha=0.7, color='black')
        plt.xlabel('CCS_DEV')
        outfname = startid + '_' + cal_mode + '_dev_hist' + ext
        plt.savefig(os.path.join(dirpath, outfname))
        plt.close()





def plot_ccs_dev_vs_rmse_vs_whwv_contour(calmode_string_list, startid, dirpath, ext):
    """
    plot contour of ccs dev as a function of ccs rmse and wh wv
    :param df: dataframe
    :param calmode: calmode
    :return: plot
    """

    for ind, cal_mode in enumerate(calmode_string_list):
        fname = startid + '_' + cal_mode + '.csv'
        df, color_dict = gen_dataframe_and_color_dict(dirpath, fname, exception_list)

        whoverwv = df['wh'].values/df['wv'].values
        ccsrmse = df['ccs_rmse'].values
        ccsdev = df['ccs_dev'].values

        colormap = 'seismic_r'

        vmax = np.max(ccsdev)
        vmin = np.min(ccsdev)
        norm = MidpointNormalize(vmin=vmin, vmax=vmax, midpoint=0)
        curr_cmap = plt.cm.get_cmap(colormap)
        curr_cmap.set_bad(color='grey', alpha=1)

        plt.tricontourf(whoverwv, ccsrmse, ccsdev, cmap=curr_cmap, clim=(vmin, vmax), norm=norm)
        plt.xlabel('WH/WV')
        plt.ylabel('CCS_RMSE')
        plt.colorbar()
        outfname = startid + '_' + cal_mode + '_wv_wv_rmse_dev_contour' + ext
        plt.savefig(os.path.join(dirpath, outfname), dpi=500)
        plt.close()


def get_best_rmse_dev_combo_wh_wv(calmode_string_list, startid, dirpath):
    """
    write ouptut file for best wave height and wave velocity condition for low rmse only, low dev only, and low rmse and low dev
    :param calmode_string_list:
    :param startid:
    :param dirpath:
    :param ext:
    :return:
    """

    for ind, cal_mode in enumerate(calmode_string_list):
        fname = startid + '_' + cal_mode + '.csv'
        df, color_dict = gen_dataframe_and_color_dict(dirpath, fname, exception_list)

        df['ccs_dev_abs'] = np.abs(df['ccs_dev'].values)

        df_sort = df.sort_values(['ccs_rmse', 'ccs_dev_abs'], ascending=[True, True])
        outfname = start_id + '_' + cal_mode + '_sort_rmse_dev.csv'
        df_sort.to_csv(os.path.join(dirpath, outfname))

        print('heho')






if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\Denature_Proteins\_blur_LeaveOneSpecies_CrossVal"

    start_id = 'ccs_dev_rmse_all_out'

    exception_list = [['wh', 'wv'], [35, 300], [40, 300], [30, 300], [40, 400]]

    cal_mode_string_list = ['power_law_True',
                            'power_law_True_exp_True',
                            'relax_True_terms_4',
                            'relax_True_terms_4_exp_True',
                            'blended_True',
                            'blended_True_exp_True']

    get_best_rmse_dev_combo_wh_wv(cal_mode_string_list,
                                  start_id,
                                  dirpath)

    plot_ccs_dev_vs_rmse_vs_whwv_contour(cal_mode_string_list, start_id, dirpath, ext='.png')

    plot_ccs_dev_vs_rmse_scatter(cal_mode_string_list,
                                 start_id,
                                 dirpath,
                                 exception_list,
                                 label_key = 'ion',
                                 ext='.png')
    plot_ccs_dev_vs_rmse_scatter(cal_mode_string_list,
                                 start_id,
                                 dirpath,
                                 exception_list,
                                 label_key='wh_wv',
                                 ext='.png')
    plot_ccs_dev_vs_rmse_scatter(cal_mode_string_list,
                                 start_id,
                                 dirpath,
                                 exception_list,
                                 label_key='mol_type',
                                 ext='.png')
    plot_ccs_rmse_dev_hist(cal_mode_string_list,
                           start_id,
                           dirpath,
                           exception_list,
                           ext='.png')

    # plot_ccs_dev_vs_rmse_wh_wv(cal_mode_string_list, start_id, dirpath, '.png')