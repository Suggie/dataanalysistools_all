import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def get_files(dirpath, endid):
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files

def plot_bar_graph(y_, y_err, x_labels, y_label, ylim=None):
    num = len(y_)
    ind = np.arange(num)
    width = 0.3
    hatch_type = ''
    fig, ax = plt.subplots()
    rects = ax.bar(ind, y_, width, color='dodgerblue', edgecolor='black',
                   hatch=hatch_type, alpha=1, yerr=y_err, error_kw=dict(lw=1, capsize=1, capthick=1))
    plt.xticks(ind, x_labels, rotation=45)
    if ylim:
        plt.ylim(ylim)
    plt.subplots_adjust(bottom=0.2)
    plt.tick_params(labelsize=10)
    plt.ylabel(y_label)


def get_average_sec_structs(list_of_files, dirpath):

    helix_avg = []
    helix_std = []
    beta_avg = []
    beta_std = []
    loop_avg = []
    loop_std = []
    helix_310_avg = []
    helix_310_std = []
    alpha_helix_avg = []
    alpha_helix_std = []
    pi_helix_avg = []
    pi_helix_std = []
    beta_buldge_avg = []
    beta_buldge_std = []
    beta_bridge_avg = []
    beta_bridge_std = []
    turns_avg = []
    turns_std = []
    curve_avg = []
    curve_std = []
    otherloops_avg = []
    otherloops_std = []

    cluster_number = []


    for ind, file in enumerate(list_of_files):

        fchars = str(file).split('_')
        clust_num = int(fchars[1].split('man')[1])
        cluster_number.append(clust_num)

        df = pd.read_csv(os.path.join(dirpath, file), sep=',')
        helix_avg.append(np.mean(df['frac_helix'].values))
        helix_std.append(np.std(df['frac_helix'].values))
        beta_avg.append(np.mean(df['frac_beta_strand'].values))
        beta_std.append(np.std(df['frac_beta_strand'].values))
        loop_avg.append(np.mean(df['frac_loop'].values))
        loop_std.append(np.std(df['frac_loop'].values))
        helix_310_avg.append(np.mean(df['frac_310_helix'].values))
        helix_310_std.append(np.std(df['frac_310_helix'].values))
        alpha_helix_avg.append(np.mean(df['frac_alpha_helix'].values))
        alpha_helix_std.append(np.std(df['frac_alpha_helix'].values))
        pi_helix_avg.append(np.mean(df['frac_pi_helix'].values))
        pi_helix_std.append(np.std(df['frac_pi_helix'].values))
        beta_buldge_avg.append(np.mean(df['frac_beta_buldge'].values))
        beta_buldge_std.append(np.std(df['frac_beta_buldge'].values))
        beta_bridge_avg.append(np.mean(df['frac_beta_bridge'].values))
        beta_bridge_std.append(np.std(df['frac_beta_bridge'].values))
        turns_avg.append(np.mean(df['frac_turns'].values))
        turns_std.append(np.std(df['frac_turns'].values))
        curve_avg.append(np.mean(df['frac_curve'].values))
        curve_std.append(np.std(df['frac_curve'].values))
        otherloops_avg.append(np.mean(df['frac_other_loop'].values))
        otherloops_std.append(np.std(df['frac_other_loop'].values))

    header = 'cluster,helix_avg,helix_std,beta_strand_avg,beta_strand_std,loop_avg,loop_std,310helix_avg,310helix_std,alphahelix_avg,alphahelix_std,pihelix_avg,pihelix_std,betabuldge_avg,betabuldge_std,betabridge_avg,betabridge_std,turns_avg,turns_std,curve_avg,curve_std,otherloop_avg,otherloop_std\n'
    data_string = ''

    for num in range(len(cluster_number)):

        line = '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(cluster_number[num],
                                                                                               helix_avg[num],
                                                                                               helix_std[num],
                                                                                               beta_avg[num],
                                                                                               beta_std[num],
                                                                                               loop_avg[num],
                                                                                               loop_std[num],
                                                                                               helix_310_avg[num],
                                                                                               helix_310_std[num],
                                                                                               alpha_helix_avg[num],
                                                                                               alpha_helix_std[num],
                                                                                               pi_helix_avg[num],
                                                                                               pi_helix_std[num],
                                                                                               beta_buldge_avg[num],
                                                                                               beta_buldge_std[num],
                                                                                               beta_bridge_avg[num],
                                                                                               beta_bridge_std[num],
                                                                                               turns_avg[num],
                                                                                               turns_std[num],
                                                                                               curve_avg[num],
                                                                                               curve_std[num],
                                                                                               otherloops_avg[num],
                                                                                               otherloops_std[num])
        data_string += line


    output_string = header + data_string

    outname = os.path.join(dirpath, 'clusters_sec_struct_summary.csv')

    with open(outname, 'w') as outfile:
        outfile.write(output_string)
        outfile.close()




if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\MembraneMDfiles\Serf_tmp_model\Replica_Exchange_Analysis_nomod\rmsdmat"

    files = get_files(dirpath, endid='dssp_analysis_struct.csv')
    get_average_sec_structs(files, dirpath)
