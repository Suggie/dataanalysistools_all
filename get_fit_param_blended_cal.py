
import os
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt



def get_cal_out_files(dirpath, endid):
    """
    get cal out files in the directory
    :param dirpath: directory path
    :param endid: end id
    :return: cal output files from calibration
    """
    files = os.listdir(dirpath)
    files = [x for x in files if x.endswith(endid)]
    return files


def write_fit_params(cal_output_fname_arr, wh_arr, wv_arr, fit_params_arr, dirpath, outid):
    """
    write the fit params to a file
    :param cal_output_fname_arr:
    :param wh_arr:
    :param wv_arr:
    :param fit_params_arr:
    :param dirpath: directory to save the file
    :param outid: outid
    :return: void. Writes the file
    """
    param_len = len(fit_params_arr[0])
    param_header = ''
    if param_len == 3:
        param_header += 'a,g,exp'
    if param_len == 2:
        param_header += 'a,g'

    header = 'cal_output_fname,wh,wv,'+param_header+'\n'

    data_string = ''

    for num in range(len(cal_output_fname_arr)):

        fit_param_string = ','.join([str(x) for x in fit_params_arr[num]])

        line = '{},{},{},{}\n'.format(cal_output_fname_arr[num], wh_arr[num], wv_arr[num], fit_param_string)
        data_string += line

    output_string = header + data_string

    outname = outid+ '_fit_params.csv'

    with open(os.path.join(dirpath, outname), 'w') as outfile:
        outfile.write(output_string)
        outfile.close()


def plot_wh_wv_fitparams(fit_params_arr, wh_arr, wv_arr, dirpath, outid):


    outname = outid+ '_fit_params.pdf'

    param_label = ['a','g','exp']

    with PdfPages(os.path.join(dirpath, outname)) as pdf:

        for ind, (fit_param, param_) in enumerate(zip(fit_params_arr, param_label)):

            plt.scatter(wh_arr, fit_param, marker='o', color='black')
            plt.xlabel('WH')
            plt.ylabel(param_)
            pdf.savefig()
            plt.close()

            plt.scatter(wv_arr, fit_param, marker='o', color='black')
            plt.xlabel('WV')
            plt.ylabel(param_)
            pdf.savefig()
            plt.close()

            plt.scatter(wh_arr/wv_arr, fit_param, marker='o', color='black')
            plt.xlabel('WH/WV')
            plt.ylabel(param_)
            pdf.savefig()
            plt.close()




def get_fit_params(dirpath, endid):
    """
    write the fit params in a file
    :param dirpath:
    :param list_calout_fpaths:
    :return:
    """

    list_calout_files = get_cal_out_files(dirpath, endid)

    fit_params_arr = []
    wh_arr = []
    wv_arr = []
    cal_output_fname_arr = []

    for ind, file in enumerate(list_calout_files):

        cal_output_fname_arr.append(str(file))

        fpath = os.path.join(dirpath, file)

        with open(fpath, 'r') as calout_file:
            caloutfile = calout_file.read().splitlines()
            for line in caloutfile:
                if line.startswith('#Waveht'):
                    wave_ht = float(line.split(',')[1])
                    wh_arr.append(wave_ht)
                if line.startswith('#Wavevel'):
                    wave_vel = float(line.split(',')[1])
                    wv_arr.append(wave_vel)
                if line.startswith('#popt'):
                    if not line.split(',')[1].startswith('param'):
                        fit_params_chars = line.split(',')[1:]
                        fit_params = [float(x) for x in fit_params_chars]
                        fit_params_arr.append(fit_params)

    outid = endid.split('.csv')[0]

    write_fit_params(cal_output_fname_arr, wh_arr, wv_arr, fit_params_arr, dirpath, outid)

    fit_params_arr_t = np.array(fit_params_arr).T
    plot_wh_wv_fitparams(fit_params_arr_t, np.array(wh_arr), np.array(wv_arr), dirpath, outid)


if __name__ == '__main__':

    dirpath = r"C:\Users\sugyan\Documents\Processed data\021519_CalProcessing\PolyAla\LeaveOneSpecies_CrossVal"
    endid = 'blended_True_exp_True_cal_output.csv'

    get_fit_params(dirpath, endid)