import os
import pandas as pd
import multiprocessing as mp
from math import pi
from midpoint_normalize_class import MidpointNormalize
import calculate_mobility
from calculate_mobility import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from scipy.interpolate import griddata


def get_files(startid, endid, dirpath):
    files = os.listdir(dirpath)
    files = [x for x in files if x.startswith(startid)]
    files = [x for x in files if x.endswith(endid)]
    return files


def makenewdir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
    return dirpath


def calculate_gamma(wavenum, mobility, waveamp, wavevel):
    y = (wavenum * mobility * waveamp) / wavevel
    return y


def calculate_alpha(wavevel, mobility, mass_kg, charge_state, wavelength):
    y = (2 * pi * wavevel * mobility * mass_kg) / (charge_state * 1.6e-19 * wavelength)
    return y


def make_ccs_err_2d_matrix(alpha_, gamma_, ccs_err):
    alpha_ = np.array(alpha_)
    gamma_ = np.array(gamma_)
    x_grid = np.linspace(np.min(alpha_), np.max(alpha_), len(alpha_))
    y_grid = np.linspace(np.min(gamma_), np.max(gamma_), len(gamma_))
    xx, yy = np.meshgrid(x_grid, y_grid)
    zgrid = griddata((alpha_, gamma_), ccs_err, (xx, yy), method='nearest')
    return xx, yy, zgrid


def plot_tricontourf(x, y, z, xlabel, ylabel, dirpath):
    vmin = np.min(z)
    vmax = np.max(z)
    vdiff_abs = int(vmax - vmin)
    norm = MidpointNormalize(vmin=vmin, vmax=vmax, midpoint=0)
    plt.tricontourf(x, y, z, vdiff_abs, cmap='RdBu_r', norm=norm)
    ax = plt.gca()
    ax.set_facecolor('xkcd:light grey')
    plt.colorbar()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(os.path.join(dirpath, 'CCSerror_' + xlabel + '_' + ylabel + '_.png'), dpi=500)
    plt.close()

def create_df_species(df_, species_label, chargestate=None):
    df = df_[df_['id'] == species_label]
    if chargestate != None:
        df = df[df['charge'] == chargestate]
    return df

def plot_dt_vs_gamma(df_species, temp, press_bar, mass_gas, wavenum, color):
    mass_da = df_species['mass'].values
    charge = df_species['charge'].values
    ccs_nm2 = df_species['ccs'].values
    drift_time = df_species['drift_time'].values
    waveht = df_species['wh'].values
    wavevel = df_species['wv'].values
    waveamp = (16.67/40) * waveht
    mob_ = calculate_mobility.calc_mobility(mass_da, charge, ccs_nm2, temp, press_bar, mass_gas)
    gamma_ = calculate_gamma(wavenum, mob_, waveamp, wavevel)
    plt.scatter(gamma_, drift_time, color=color, alpha=1, label=df_species['id'].values[0])
    plt.xlabel('gamma')
    plt.ylabel('drift_time')
    plt.legend()

def plot_dt_vs_alpha(df_species, temp, press_bar, mass_gas, wavelength, color):
    mass_da = df_species['mass'].values
    mass_kg = mass_da_to_kg(mass_da)
    charge = df_species['charge'].values
    ccs_nm2 = df_species['ccs'].values
    drift_time = df_species['drift_time'].values
    waveht = df_species['wh'].values
    wavevel = df_species['wv'].values
    waveamp = (16.67/40) * waveht
    mob_ = calculate_mobility.calc_mobility(mass_da, charge, ccs_nm2, temp, press_bar, mass_gas)
    alpha_ = calculate_alpha(wavevel, mob_, mass_kg, charge, wavelength)
    plt.scatter(alpha_, drift_time, color=color, alpha=1, label=df_species['id'].values[0])
    plt.xlabel('alpha')
    plt.ylabel('drift_time')
    plt.legend()





dirpath = r"C:\Users\sugyan\Documents\Processed data\072018_CalData\_calfles\New_cal_scheme\All_ions\Cross_Val_each_ion"

mass_gas_da = 28.0
press_bar = 3.5 / 1000
temp_cel = 298

wavelength = 0.012
wavenum = 524

start_id_list = ['ccs_error_powerfit_powerfitexp_False',
                 'ccs_error_powerfit_powerfitexp_True',
                 'ccs_error_relaxation_True_terms_4',
                 'ccs_error_relaxation_True_terms_6',
                 'ccs_error_relaxation_False_terms_4',
                 'ccs_error_relaxation_False_terms_6',
                 'ccs_error_relaxationexp_True_terms_4',
                 'ccs_error_relaxationexp_True_terms_6']

endid = '.csv'

for startid in start_id_list[:2]:

    print(startid + ' ...')
    outdirpath = os.path.join(dirpath, startid)
    outdir = makenewdir(outdirpath)

    df_list = []

    files = get_files(startid, endid, dirpath)
    for file in files:
        fname = str(file)
        df = pd.read_csv(os.path.join(dirpath, fname), sep=',')
        df = df[(df['wh'] != 40) | (df['wv'] != 300)]
        df = df[(df['wh'] != 35) | (df['wv'] != 300)]
        df = df[(df['wh'] != 40) | (df['wv'] != 400)]
        df_list.append(df)

    df_comb = pd.concat(df_list)
    # df_avd = df_comb[df_comb['id'] == 'avidin']
    # print('heho')
    df_avd = create_df_species(df_comb, 'avidin', chargestate=None)
    df_bsa = create_df_species(df_comb, 'bsa', chargestate=None)
    df_ttr = create_df_species(df_comb, 'transthyretin', chargestate=None)
    plot_dt_vs_alpha(df_avd, temp_cel, press_bar, mass_gas_da, wavelength, 'r')
    plot_dt_vs_alpha(df_bsa, temp_cel, press_bar, mass_gas_da, wavelength, 'black')
    plot_dt_vs_alpha(df_ttr, temp_cel, press_bar, mass_gas_da, wavelength, 'green')
    # plot_dt_vs_gamma(df_avd, temp_cel, press_bar, mass_gas_da, wavenum, 'r')
    # plot_dt_vs_gamma(df_bsa, temp_cel, press_bar, mass_gas_da, wavenum, 'black')
    # plot_dt_vs_gamma(df_ttr, temp_cel, press_bar, mass_gas_da, wavenum, 'green')
    plt.show()
    plt.close()