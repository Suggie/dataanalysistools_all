from math import pi, sqrt
import numpy as np

# need mass, charge, CCS, temperature, and pressure [put these as variables]

def mass_da_to_kg(mass):
    return mass*1.66054e-27

def pressure_bar_to_pascals(pressure):
    return pressure * 1e5

def temp_cel_to_kel(temp):
    return 273.15 + temp

def ccs_nm2_to_m2(ccs):
    return ccs * 1e-18

def calculate_number_density(pressure_pascal, temperature_kelvin):
    gas_constant = 8.314
    num_density = (pressure_pascal * 6.022e23) / (gas_constant * temperature_kelvin)
    return num_density

def calc_mobility(mass_da, charge_state, ccs_nm2, temp_k, pressure_bar, mass_gas_da):
    pressure_pasc = pressure_bar_to_pascals(pressure_bar)
    num_den = calculate_number_density(pressure_pasc, temp_k)
    mass_anal_kg = mass_da_to_kg(mass_da)
    mass_gas_kg = mass_da_to_kg(mass_gas_da)
    ccs_m2 = ccs_nm2_to_m2(ccs_nm2)
    y = (3 / 16) * np.sqrt(( 2 * pi * (mass_anal_kg + mass_gas_kg)) / (mass_anal_kg * mass_gas_kg * 1.38e-23 * temp_k)) * ((charge_state * 1.6e-19) / (num_den * ccs_m2))
    return y

# def calc_reduced_mobility(mobility, temperature, pressure):


pressure = 4/1000
mass = 12000
charge = 6
temp = 298
mob = calc_mobility(mass, charge, 14.9, temp, pressure, 28)
print(mob)